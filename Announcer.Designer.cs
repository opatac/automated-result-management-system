namespace ARMS
{
    partial class Announcer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Announcer));
            this.groupBoxMessages = new System.Windows.Forms.GroupBox();
            this.richTextBoxExtended1 = new ARMS.RichTextBoxExtended();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.comboBoxFaculty = new System.Windows.Forms.ComboBox();
            this.comboBoxGroup = new System.Windows.Forms.ComboBox();
            this.buttonAnnounce = new System.Windows.Forms.Button();
            this.comboBoxDepartment = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBoxMessages.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxMessages
            // 
            this.groupBoxMessages.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxMessages.Controls.Add(this.richTextBoxExtended1);
            this.groupBoxMessages.Location = new System.Drawing.Point(12, 104);
            this.groupBoxMessages.Name = "groupBoxMessages";
            this.groupBoxMessages.Size = new System.Drawing.Size(713, 308);
            this.groupBoxMessages.TabIndex = 4;
            this.groupBoxMessages.TabStop = false;
            this.groupBoxMessages.Text = "Enter an Announcmenet to make in the box provided below";
            // 
            // richTextBoxExtended1
            // 
            this.richTextBoxExtended1.AcceptsTab = false;
            this.richTextBoxExtended1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxExtended1.AutoWordSelection = true;
            this.richTextBoxExtended1.DetectURLs = true;
            this.richTextBoxExtended1.Location = new System.Drawing.Point(8, 23);
            this.richTextBoxExtended1.Name = "richTextBoxExtended1";
            this.richTextBoxExtended1.ReadOnly = false;
            // 
            // 
            // 
            this.richTextBoxExtended1.RichTextBox.AutoWordSelection = true;
            this.richTextBoxExtended1.RichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxExtended1.RichTextBox.Location = new System.Drawing.Point(0, 26);
            this.richTextBoxExtended1.RichTextBox.MaxLength = 2000;
            this.richTextBoxExtended1.RichTextBox.Name = "rtb1";
            this.richTextBoxExtended1.RichTextBox.Size = new System.Drawing.Size(697, 253);
            this.richTextBoxExtended1.RichTextBox.TabIndex = 1;
            this.richTextBoxExtended1.ShowBold = true;
            this.richTextBoxExtended1.ShowCenterJustify = true;
            this.richTextBoxExtended1.ShowColors = true;
            this.richTextBoxExtended1.ShowCopy = true;
            this.richTextBoxExtended1.ShowCut = true;
            this.richTextBoxExtended1.ShowFont = true;
            this.richTextBoxExtended1.ShowFontSize = true;
            this.richTextBoxExtended1.ShowItalic = true;
            this.richTextBoxExtended1.ShowLeftJustify = true;
            this.richTextBoxExtended1.ShowOpen = true;
            this.richTextBoxExtended1.ShowPaste = true;
            this.richTextBoxExtended1.ShowRedo = true;
            this.richTextBoxExtended1.ShowRightJustify = true;
            this.richTextBoxExtended1.ShowStamp = true;
            this.richTextBoxExtended1.ShowStrikeout = true;
            this.richTextBoxExtended1.ShowToolBarText = false;
            this.richTextBoxExtended1.ShowUnderline = true;
            this.richTextBoxExtended1.ShowUndo = true;
            this.richTextBoxExtended1.Size = new System.Drawing.Size(697, 279);
            this.richTextBoxExtended1.StampAction = ARMS.StampActions.EditedBy;
            this.richTextBoxExtended1.StampColor = System.Drawing.Color.Blue;
            this.richTextBoxExtended1.TabIndex = 0;
            // 
            // 
            // 
            this.richTextBoxExtended1.Toolbar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
            this.richTextBoxExtended1.Toolbar.ButtonSize = new System.Drawing.Size(16, 16);
            this.richTextBoxExtended1.Toolbar.Divider = false;
            this.richTextBoxExtended1.Toolbar.DropDownArrows = true;
            this.richTextBoxExtended1.Toolbar.Location = new System.Drawing.Point(0, 0);
            this.richTextBoxExtended1.Toolbar.Name = "tb1";
            this.richTextBoxExtended1.Toolbar.ShowToolTips = true;
            this.richTextBoxExtended1.Toolbar.Size = new System.Drawing.Size(697, 26);
            this.richTextBoxExtended1.Toolbar.TabIndex = 0;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "announcements.png");
            this.imageList1.Images.SetKeyName(1, "eannounce.png");
            this.imageList1.Images.SetKeyName(2, "APPLY.PNG");
            // 
            // comboBoxFaculty
            // 
            this.comboBoxFaculty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFaculty.FormattingEnabled = true;
            this.comboBoxFaculty.Location = new System.Drawing.Point(20, 52);
            this.comboBoxFaculty.Name = "comboBoxFaculty";
            this.comboBoxFaculty.Size = new System.Drawing.Size(207, 26);
            this.comboBoxFaculty.TabIndex = 14;
            this.comboBoxFaculty.SelectedIndexChanged += new System.EventHandler(this.comboBoxFaculty_SelectedIndexChanged);
            // 
            // comboBoxGroup
            // 
            this.comboBoxGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxGroup.FormattingEnabled = true;
            this.comboBoxGroup.Items.AddRange(new object[] {
            "All",
            "Lecturer",
            "HOD",
            "Exam Officer",
            "Dean",
            "Department Admin"});
            this.comboBoxGroup.Location = new System.Drawing.Point(502, 52);
            this.comboBoxGroup.Name = "comboBoxGroup";
            this.comboBoxGroup.Size = new System.Drawing.Size(203, 26);
            this.comboBoxGroup.TabIndex = 14;
            // 
            // buttonAnnounce
            // 
            this.buttonAnnounce.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAnnounce.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAnnounce.ImageKey = "announcements.png";
            this.buttonAnnounce.ImageList = this.imageList1;
            this.buttonAnnounce.Location = new System.Drawing.Point(603, 418);
            this.buttonAnnounce.Name = "buttonAnnounce";
            this.buttonAnnounce.Padding = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.buttonAnnounce.Size = new System.Drawing.Size(122, 40);
            this.buttonAnnounce.TabIndex = 11;
            this.buttonAnnounce.Text = "&Announce";
            this.buttonAnnounce.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAnnounce.UseVisualStyleBackColor = true;
            this.buttonAnnounce.Click += new System.EventHandler(this.buttonAnnounce_Click);
            // 
            // comboBoxDepartment
            // 
            this.comboBoxDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDepartment.FormattingEnabled = true;
            this.comboBoxDepartment.Location = new System.Drawing.Point(243, 52);
            this.comboBoxDepartment.Name = "comboBoxDepartment";
            this.comboBoxDepartment.Size = new System.Drawing.Size(226, 26);
            this.comboBoxDepartment.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(278, 18);
            this.label1.TabIndex = 15;
            this.label1.Text = "Enter announcement target details below:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(500, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 18);
            this.label2.TabIndex = 15;
            this.label2.Text = "Message Recipients:";
            // 
            // Announcer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(737, 465);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxGroup);
            this.Controls.Add(this.comboBoxDepartment);
            this.Controls.Add(this.comboBoxFaculty);
            this.Controls.Add(this.buttonAnnounce);
            this.Controls.Add(this.groupBoxMessages);
            this.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Announcer";
            this.Text = "Announcer";
            this.groupBoxMessages.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxMessages;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button buttonAnnounce;
        private System.Windows.Forms.ComboBox comboBoxFaculty;
        private System.Windows.Forms.ComboBox comboBoxGroup;
        private RichTextBoxExtended richTextBoxExtended1;
        private System.Windows.Forms.ComboBox comboBoxDepartment;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}
