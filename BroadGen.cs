using DocumentFormat.OpenXml.Packaging;
using Ap = DocumentFormat.OpenXml.ExtendedProperties;
using Vt = DocumentFormat.OpenXml.VariantTypes;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using X14 = DocumentFormat.OpenXml.Office2010.Excel;
using A = DocumentFormat.OpenXml.Drawing;
using Xdr = DocumentFormat.OpenXml.Drawing.Spreadsheet;

namespace ARMS
{
    public class BroadGen
    {
        // Creates a SpreadsheetDocument.
        public void CreatePackage(string filePath, string department, string faculty, string academicYear, string semester, string level)
        {
            this.department = department;
            this.faculty = faculty;
            this.academicYear = academicYear;
            this.semester = semester;
            this.level = level;
            using (SpreadsheetDocument package = SpreadsheetDocument.Create(filePath, SpreadsheetDocumentType.Workbook))
            {
                CreateParts(package);
            }
        }
        private string department;
        private string faculty;
        private string academicYear;
        private string semester;
        private string level;

        // Adds child parts and generates content of the specified part.
        private void CreateParts(SpreadsheetDocument document)
        {
            ExtendedFilePropertiesPart extendedFilePropertiesPart1 = document.AddNewPart<ExtendedFilePropertiesPart>("rId3");
            GenerateExtendedFilePropertiesPart1Content(extendedFilePropertiesPart1);

            WorkbookPart workbookPart1 = document.AddWorkbookPart();
            GenerateWorkbookPart1Content(workbookPart1);

            WorkbookStylesPart workbookStylesPart1 = workbookPart1.AddNewPart<WorkbookStylesPart>("rId3");
            GenerateWorkbookStylesPart1Content(workbookStylesPart1);

            ThemePart themePart1 = workbookPart1.AddNewPart<ThemePart>("rId2");
            GenerateThemePart1Content(themePart1);

            WorksheetPart worksheetPart1 = workbookPart1.AddNewPart<WorksheetPart>("rId1");
            GenerateWorksheetPart1Content(worksheetPart1);

            DrawingsPart drawingsPart1 = worksheetPart1.AddNewPart<DrawingsPart>("rId2");
            GenerateDrawingsPart1Content(drawingsPart1);

            SpreadsheetPrinterSettingsPart spreadsheetPrinterSettingsPart1 = worksheetPart1.AddNewPart<SpreadsheetPrinterSettingsPart>("rId1");
            GenerateSpreadsheetPrinterSettingsPart1Content(spreadsheetPrinterSettingsPart1);

            CalculationChainPart calculationChainPart1 = workbookPart1.AddNewPart<CalculationChainPart>("rId5");
            GenerateCalculationChainPart1Content(calculationChainPart1);

            SharedStringTablePart sharedStringTablePart1 = workbookPart1.AddNewPart<SharedStringTablePart>("rId4");
            GenerateSharedStringTablePart1Content(sharedStringTablePart1);

            SetPackageProperties(document);
        }

        // Generates content of extendedFilePropertiesPart1.
        private void GenerateExtendedFilePropertiesPart1Content(ExtendedFilePropertiesPart extendedFilePropertiesPart1)
        {
            Ap.Properties properties1 = new Ap.Properties();
            properties1.AddNamespaceDeclaration("vt", "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes");
            Ap.Application application1 = new Ap.Application();
            application1.Text = "Microsoft Excel";
            Ap.DocumentSecurity documentSecurity1 = new Ap.DocumentSecurity();
            documentSecurity1.Text = "0";
            Ap.ScaleCrop scaleCrop1 = new Ap.ScaleCrop();
            scaleCrop1.Text = "false";

            Ap.HeadingPairs headingPairs1 = new Ap.HeadingPairs();

            Vt.VTVector vTVector1 = new Vt.VTVector() { BaseType = Vt.VectorBaseValues.Variant, Size = (UInt32Value)2U };

            Vt.Variant variant1 = new Vt.Variant();
            Vt.VTLPSTR vTLPSTR1 = new Vt.VTLPSTR();
            vTLPSTR1.Text = "Worksheets";

            variant1.Append(vTLPSTR1);

            Vt.Variant variant2 = new Vt.Variant();
            Vt.VTInt32 vTInt321 = new Vt.VTInt32();
            vTInt321.Text = "1";

            variant2.Append(vTInt321);

            vTVector1.Append(variant1);
            vTVector1.Append(variant2);

            headingPairs1.Append(vTVector1);

            Ap.TitlesOfParts titlesOfParts1 = new Ap.TitlesOfParts();

            Vt.VTVector vTVector2 = new Vt.VTVector() { BaseType = Vt.VectorBaseValues.Lpstr, Size = (UInt32Value)1U };
            Vt.VTLPSTR vTLPSTR2 = new Vt.VTLPSTR();
            vTLPSTR2.Text = "Sheet1";

            vTVector2.Append(vTLPSTR2);

            titlesOfParts1.Append(vTVector2);
            Ap.Company company1 = new Ap.Company();
            company1.Text = "";
            Ap.LinksUpToDate linksUpToDate1 = new Ap.LinksUpToDate();
            linksUpToDate1.Text = "false";
            Ap.SharedDocument sharedDocument1 = new Ap.SharedDocument();
            sharedDocument1.Text = "false";
            Ap.HyperlinksChanged hyperlinksChanged1 = new Ap.HyperlinksChanged();
            hyperlinksChanged1.Text = "false";
            Ap.ApplicationVersion applicationVersion1 = new Ap.ApplicationVersion();
            applicationVersion1.Text = "15.0300";

            properties1.Append(application1);
            properties1.Append(documentSecurity1);
            properties1.Append(scaleCrop1);
            properties1.Append(headingPairs1);
            properties1.Append(titlesOfParts1);
            properties1.Append(company1);
            properties1.Append(linksUpToDate1);
            properties1.Append(sharedDocument1);
            properties1.Append(hyperlinksChanged1);
            properties1.Append(applicationVersion1);

            extendedFilePropertiesPart1.Properties = properties1;
        }

        // Generates content of workbookPart1.
        private void GenerateWorkbookPart1Content(WorkbookPart workbookPart1)
        {
            Workbook workbook1 = new Workbook() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "x15" } };
            workbook1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            workbook1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            workbook1.AddNamespaceDeclaration("x15", "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main");
            FileVersion fileVersion1 = new FileVersion() { ApplicationName = "xl", LastEdited = "6", LowestEdited = "6", BuildVersion = "14420" };
            WorkbookProperties workbookProperties1 = new WorkbookProperties();

            AlternateContent alternateContent1 = new AlternateContent();
            alternateContent1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");

            AlternateContentChoice alternateContentChoice1 = new AlternateContentChoice() { Requires = "x15" };

            OpenXmlUnknownElement openXmlUnknownElement1 = OpenXmlUnknownElement.CreateOpenXmlUnknownElement("<x15ac:absPath xmlns:x15ac=\"http://schemas.microsoft.com/office/spreadsheetml/2010/11/ac\" url=\"C:\\Robosys\\CodeCamp\\eCampus\\templates\\\" />");

            alternateContentChoice1.Append(openXmlUnknownElement1);

            alternateContent1.Append(alternateContentChoice1);

            BookViews bookViews1 = new BookViews();
            WorkbookView workbookView1 = new WorkbookView() { XWindow = 0, YWindow = 0, WindowWidth = (UInt32Value)12885U, WindowHeight = (UInt32Value)4965U };

            bookViews1.Append(workbookView1);

            Sheets sheets1 = new Sheets();
            Sheet sheet1 = new Sheet() { Name = "Sheet1", SheetId = (UInt32Value)1U, Id = "rId1" };

            sheets1.Append(sheet1);
            CalculationProperties calculationProperties1 = new CalculationProperties() { CalculationId = (UInt32Value)152511U };

            WorkbookExtensionList workbookExtensionList1 = new WorkbookExtensionList();

            WorkbookExtension workbookExtension1 = new WorkbookExtension() { Uri = "{140A7094-0E35-4892-8432-C4D2E57EDEB5}" };
            workbookExtension1.AddNamespaceDeclaration("x15", "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main");

            OpenXmlUnknownElement openXmlUnknownElement2 = OpenXmlUnknownElement.CreateOpenXmlUnknownElement("<x15:workbookPr chartTrackingRefBase=\"1\" xmlns:x15=\"http://schemas.microsoft.com/office/spreadsheetml/2010/11/main\" />");

            workbookExtension1.Append(openXmlUnknownElement2);

            workbookExtensionList1.Append(workbookExtension1);

            workbook1.Append(fileVersion1);
            workbook1.Append(workbookProperties1);
            workbook1.Append(alternateContent1);
            workbook1.Append(bookViews1);
            workbook1.Append(sheets1);
            workbook1.Append(calculationProperties1);
            workbook1.Append(workbookExtensionList1);

            workbookPart1.Workbook = workbook1;
        }

        // Generates content of workbookStylesPart1.
        private void GenerateWorkbookStylesPart1Content(WorkbookStylesPart workbookStylesPart1)
        {
            Stylesheet stylesheet1 = new Stylesheet() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "x14ac" } };
            stylesheet1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            stylesheet1.AddNamespaceDeclaration("x14ac", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac");

            NumberingFormats numberingFormats1 = new NumberingFormats() { Count = (UInt32Value)2U };
            NumberingFormat numberingFormat1 = new NumberingFormat() { NumberFormatId = (UInt32Value)164U, FormatCode = "0;\\-0;;@" };
            NumberingFormat numberingFormat2 = new NumberingFormat() { NumberFormatId = (UInt32Value)165U, FormatCode = "0.000" };

            numberingFormats1.Append(numberingFormat1);
            numberingFormats1.Append(numberingFormat2);

            Fonts fonts1 = new Fonts() { Count = (UInt32Value)19U, KnownFonts = true };

            Font font1 = new Font();
            FontSize fontSize1 = new FontSize() { Val = 10D };
            Color color1 = new Color() { Theme = (UInt32Value)1U };
            FontName fontName1 = new FontName() { Val = "Arial" };
            FontFamilyNumbering fontFamilyNumbering1 = new FontFamilyNumbering() { Val = 2 };
            FontScheme fontScheme1 = new FontScheme() { Val = FontSchemeValues.Minor };

            font1.Append(fontSize1);
            font1.Append(color1);
            font1.Append(fontName1);
            font1.Append(fontFamilyNumbering1);
            font1.Append(fontScheme1);

            Font font2 = new Font();
            FontSize fontSize2 = new FontSize() { Val = 14D };
            FontName fontName2 = new FontName() { Val = "Times New Roman" };
            FontFamilyNumbering fontFamilyNumbering2 = new FontFamilyNumbering() { Val = 1 };

            font2.Append(fontSize2);
            font2.Append(fontName2);
            font2.Append(fontFamilyNumbering2);

            Font font3 = new Font();
            FontSize fontSize3 = new FontSize() { Val = 12D };
            Color color2 = new Color() { Indexed = (UInt32Value)62U };
            FontName fontName3 = new FontName() { Val = "Times New Roman" };
            FontFamilyNumbering fontFamilyNumbering3 = new FontFamilyNumbering() { Val = 1 };

            font3.Append(fontSize3);
            font3.Append(color2);
            font3.Append(fontName3);
            font3.Append(fontFamilyNumbering3);

            Font font4 = new Font();
            FontSize fontSize4 = new FontSize() { Val = 10D };
            FontName fontName4 = new FontName() { Val = "Times New Roman" };
            FontFamilyNumbering fontFamilyNumbering4 = new FontFamilyNumbering() { Val = 1 };

            font4.Append(fontSize4);
            font4.Append(fontName4);
            font4.Append(fontFamilyNumbering4);

            Font font5 = new Font();
            FontSize fontSize5 = new FontSize() { Val = 12D };
            FontName fontName5 = new FontName() { Val = "Times New Roman" };
            FontFamilyNumbering fontFamilyNumbering5 = new FontFamilyNumbering() { Val = 1 };

            font5.Append(fontSize5);
            font5.Append(fontName5);
            font5.Append(fontFamilyNumbering5);

            Font font6 = new Font();
            FontSize fontSize6 = new FontSize() { Val = 10D };
            Color color3 = new Color() { Indexed = (UInt32Value)62U };
            FontName fontName6 = new FontName() { Val = "Times New Roman" };
            FontFamilyNumbering fontFamilyNumbering6 = new FontFamilyNumbering() { Val = 1 };

            font6.Append(fontSize6);
            font6.Append(color3);
            font6.Append(fontName6);
            font6.Append(fontFamilyNumbering6);

            Font font7 = new Font();
            FontSize fontSize7 = new FontSize() { Val = 14D };
            Color color4 = new Color() { Indexed = (UInt32Value)30U };
            FontName fontName7 = new FontName() { Val = "Times New Roman" };
            FontFamilyNumbering fontFamilyNumbering7 = new FontFamilyNumbering() { Val = 1 };

            font7.Append(fontSize7);
            font7.Append(color4);
            font7.Append(fontName7);
            font7.Append(fontFamilyNumbering7);

            Font font8 = new Font();
            Bold bold1 = new Bold();
            FontSize fontSize8 = new FontSize() { Val = 14D };
            FontName fontName8 = new FontName() { Val = "Times New Roman" };
            FontFamilyNumbering fontFamilyNumbering8 = new FontFamilyNumbering() { Val = 1 };

            font8.Append(bold1);
            font8.Append(fontSize8);
            font8.Append(fontName8);
            font8.Append(fontFamilyNumbering8);

            Font font9 = new Font();
            Bold bold2 = new Bold();
            FontSize fontSize9 = new FontSize() { Val = 12D };
            Color color5 = new Color() { Indexed = (UInt32Value)62U };
            FontName fontName9 = new FontName() { Val = "Times New Roman" };
            FontFamilyNumbering fontFamilyNumbering9 = new FontFamilyNumbering() { Val = 1 };

            font9.Append(bold2);
            font9.Append(fontSize9);
            font9.Append(color5);
            font9.Append(fontName9);
            font9.Append(fontFamilyNumbering9);

            Font font10 = new Font();
            Bold bold3 = new Bold();
            FontSize fontSize10 = new FontSize() { Val = 10D };
            Color color6 = new Color() { Indexed = (UInt32Value)62U };
            FontName fontName10 = new FontName() { Val = "Times New Roman" };
            FontFamilyNumbering fontFamilyNumbering10 = new FontFamilyNumbering() { Val = 1 };

            font10.Append(bold3);
            font10.Append(fontSize10);
            font10.Append(color6);
            font10.Append(fontName10);
            font10.Append(fontFamilyNumbering10);

            Font font11 = new Font();
            Bold bold4 = new Bold();
            FontSize fontSize11 = new FontSize() { Val = 10D };
            FontName fontName11 = new FontName() { Val = "Times New Roman" };
            FontFamilyNumbering fontFamilyNumbering11 = new FontFamilyNumbering() { Val = 1 };

            font11.Append(bold4);
            font11.Append(fontSize11);
            font11.Append(fontName11);
            font11.Append(fontFamilyNumbering11);

            Font font12 = new Font();
            Bold bold5 = new Bold();
            FontSize fontSize12 = new FontSize() { Val = 14D };
            Color color7 = new Color() { Indexed = (UInt32Value)30U };
            FontName fontName12 = new FontName() { Val = "Times New Roman" };
            FontFamilyNumbering fontFamilyNumbering12 = new FontFamilyNumbering() { Val = 1 };

            font12.Append(bold5);
            font12.Append(fontSize12);
            font12.Append(color7);
            font12.Append(fontName12);
            font12.Append(fontFamilyNumbering12);

            Font font13 = new Font();
            Bold bold6 = new Bold();
            FontSize fontSize13 = new FontSize() { Val = 8D };
            Color color8 = new Color() { Indexed = (UInt32Value)62U };
            FontName fontName13 = new FontName() { Val = "Arial" };
            FontFamilyNumbering fontFamilyNumbering13 = new FontFamilyNumbering() { Val = 2 };

            font13.Append(bold6);
            font13.Append(fontSize13);
            font13.Append(color8);
            font13.Append(fontName13);
            font13.Append(fontFamilyNumbering13);

            Font font14 = new Font();
            Bold bold7 = new Bold();
            FontSize fontSize14 = new FontSize() { Val = 8D };
            FontName fontName14 = new FontName() { Val = "Arial" };
            FontFamilyNumbering fontFamilyNumbering14 = new FontFamilyNumbering() { Val = 2 };

            font14.Append(bold7);
            font14.Append(fontSize14);
            font14.Append(fontName14);
            font14.Append(fontFamilyNumbering14);

            Font font15 = new Font();
            Bold bold8 = new Bold();
            FontSize fontSize15 = new FontSize() { Val = 8D };
            Color color9 = new Color() { Indexed = (UInt32Value)30U };
            FontName fontName15 = new FontName() { Val = "Arial" };
            FontFamilyNumbering fontFamilyNumbering15 = new FontFamilyNumbering() { Val = 2 };

            font15.Append(bold8);
            font15.Append(fontSize15);
            font15.Append(color9);
            font15.Append(fontName15);
            font15.Append(fontFamilyNumbering15);

            Font font16 = new Font();
            Bold bold9 = new Bold();
            FontSize fontSize16 = new FontSize() { Val = 8D };
            Color color10 = new Color() { Rgb = "FF7030A0" };
            FontName fontName16 = new FontName() { Val = "Arial" };
            FontFamilyNumbering fontFamilyNumbering16 = new FontFamilyNumbering() { Val = 2 };

            font16.Append(bold9);
            font16.Append(fontSize16);
            font16.Append(color10);
            font16.Append(fontName16);
            font16.Append(fontFamilyNumbering16);

            Font font17 = new Font();
            FontSize fontSize17 = new FontSize() { Val = 8D };
            FontName fontName17 = new FontName() { Val = "Arial" };
            FontFamilyNumbering fontFamilyNumbering17 = new FontFamilyNumbering() { Val = 2 };

            font17.Append(fontSize17);
            font17.Append(fontName17);
            font17.Append(fontFamilyNumbering17);

            Font font18 = new Font();
            FontSize fontSize18 = new FontSize() { Val = 8D };
            Color color11 = new Color() { Theme = (UInt32Value)1U };
            FontName fontName18 = new FontName() { Val = "Arial" };
            FontFamilyNumbering fontFamilyNumbering18 = new FontFamilyNumbering() { Val = 2 };

            font18.Append(fontSize18);
            font18.Append(color11);
            font18.Append(fontName18);
            font18.Append(fontFamilyNumbering18);

            Font font19 = new Font();
            FontSize fontSize19 = new FontSize() { Val = 8D };
            Color color12 = new Color() { Theme = (UInt32Value)1U };
            FontName fontName19 = new FontName() { Val = "Arial" };
            FontFamilyNumbering fontFamilyNumbering19 = new FontFamilyNumbering() { Val = 2 };
            FontScheme fontScheme2 = new FontScheme() { Val = FontSchemeValues.Minor };

            font19.Append(fontSize19);
            font19.Append(color12);
            font19.Append(fontName19);
            font19.Append(fontFamilyNumbering19);
            font19.Append(fontScheme2);

            fonts1.Append(font1);
            fonts1.Append(font2);
            fonts1.Append(font3);
            fonts1.Append(font4);
            fonts1.Append(font5);
            fonts1.Append(font6);
            fonts1.Append(font7);
            fonts1.Append(font8);
            fonts1.Append(font9);
            fonts1.Append(font10);
            fonts1.Append(font11);
            fonts1.Append(font12);
            fonts1.Append(font13);
            fonts1.Append(font14);
            fonts1.Append(font15);
            fonts1.Append(font16);
            fonts1.Append(font17);
            fonts1.Append(font18);
            fonts1.Append(font19);

            Fills fills1 = new Fills() { Count = (UInt32Value)2U };

            Fill fill1 = new Fill();
            PatternFill patternFill1 = new PatternFill() { PatternType = PatternValues.None };

            fill1.Append(patternFill1);

            Fill fill2 = new Fill();
            PatternFill patternFill2 = new PatternFill() { PatternType = PatternValues.Gray125 };

            fill2.Append(patternFill2);

            fills1.Append(fill1);
            fills1.Append(fill2);

            Borders borders1 = new Borders() { Count = (UInt32Value)1U };

            Border border1 = new Border();
            LeftBorder leftBorder1 = new LeftBorder();
            RightBorder rightBorder1 = new RightBorder();
            TopBorder topBorder1 = new TopBorder();
            BottomBorder bottomBorder1 = new BottomBorder();
            DiagonalBorder diagonalBorder1 = new DiagonalBorder();

            border1.Append(leftBorder1);
            border1.Append(rightBorder1);
            border1.Append(topBorder1);
            border1.Append(bottomBorder1);
            border1.Append(diagonalBorder1);

            borders1.Append(border1);

            CellStyleFormats cellStyleFormats1 = new CellStyleFormats() { Count = (UInt32Value)1U };
            CellFormat cellFormat1 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U };

            cellStyleFormats1.Append(cellFormat1);

            CellFormats cellFormats1 = new CellFormats() { Count = (UInt32Value)35U };
            CellFormat cellFormat2 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)0U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U };

            CellFormat cellFormat3 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)1U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Protection protection1 = new Protection() { Locked = false };

            cellFormat3.Append(protection1);

            CellFormat cellFormat4 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)2U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment1 = new Alignment() { Horizontal = HorizontalAlignmentValues.Right };

            cellFormat4.Append(alignment1);

            CellFormat cellFormat5 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)3U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Protection protection2 = new Protection() { Locked = false };

            cellFormat5.Append(protection2);
            CellFormat cellFormat6 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)3U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true };

            CellFormat cellFormat7 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)4U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true };
            Alignment alignment2 = new Alignment() { Horizontal = HorizontalAlignmentValues.Left, WrapText = true };

            cellFormat7.Append(alignment2);
            CellFormat cellFormat8 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)1U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true };

            CellFormat cellFormat9 = new CellFormat() { NumberFormatId = (UInt32Value)164U, FontId = (UInt32Value)6U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyNumberFormat = true, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment3 = new Alignment() { Horizontal = HorizontalAlignmentValues.Left, WrapText = true };
            Protection protection3 = new Protection() { Locked = false, Hidden = true };

            cellFormat9.Append(alignment3);
            cellFormat9.Append(protection3);

            CellFormat cellFormat10 = new CellFormat() { NumberFormatId = (UInt32Value)165U, FontId = (UInt32Value)6U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyNumberFormat = true, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment4 = new Alignment() { Horizontal = HorizontalAlignmentValues.Left, WrapText = true };
            Protection protection4 = new Protection() { Locked = false, Hidden = true };

            cellFormat10.Append(alignment4);
            cellFormat10.Append(protection4);

            CellFormat cellFormat11 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)7U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Protection protection5 = new Protection() { Locked = false };

            cellFormat11.Append(protection5);

            CellFormat cellFormat12 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)8U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment5 = new Alignment() { Horizontal = HorizontalAlignmentValues.Right };
            Protection protection6 = new Protection() { Locked = false };

            cellFormat12.Append(alignment5);
            cellFormat12.Append(protection6);
            CellFormat cellFormat13 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)10U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true };

            CellFormat cellFormat14 = new CellFormat() { NumberFormatId = (UInt32Value)164U, FontId = (UInt32Value)11U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyNumberFormat = true, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment6 = new Alignment() { Horizontal = HorizontalAlignmentValues.Left, WrapText = true };
            Protection protection7 = new Protection() { Locked = false, Hidden = true };

            cellFormat14.Append(alignment6);
            cellFormat14.Append(protection7);

            CellFormat cellFormat15 = new CellFormat() { NumberFormatId = (UInt32Value)165U, FontId = (UInt32Value)11U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyNumberFormat = true, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment7 = new Alignment() { Horizontal = HorizontalAlignmentValues.Left, WrapText = true };
            Protection protection8 = new Protection() { Locked = false, Hidden = true };

            cellFormat15.Append(alignment7);
            cellFormat15.Append(protection8);

            CellFormat cellFormat16 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)12U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment8 = new Alignment() { Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Center };

            cellFormat16.Append(alignment8);

            CellFormat cellFormat17 = new CellFormat() { NumberFormatId = (UInt32Value)164U, FontId = (UInt32Value)14U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyNumberFormat = true, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment9 = new Alignment() { Horizontal = HorizontalAlignmentValues.Left, Vertical = VerticalAlignmentValues.Top, TextRotation = (UInt32Value)180U, WrapText = true };
            Protection protection9 = new Protection() { Locked = false, Hidden = true };

            cellFormat17.Append(alignment9);
            cellFormat17.Append(protection9);

            CellFormat cellFormat18 = new CellFormat() { NumberFormatId = (UInt32Value)165U, FontId = (UInt32Value)14U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyNumberFormat = true, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment10 = new Alignment() { Horizontal = HorizontalAlignmentValues.Left, Vertical = VerticalAlignmentValues.Top, TextRotation = (UInt32Value)180U, WrapText = true };
            Protection protection10 = new Protection() { Locked = false, Hidden = true };

            cellFormat18.Append(alignment10);
            cellFormat18.Append(protection10);

            CellFormat cellFormat19 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)13U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment11 = new Alignment() { Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Top, TextRotation = (UInt32Value)180U, WrapText = true };
            Protection protection11 = new Protection() { Locked = false };

            cellFormat19.Append(alignment11);
            cellFormat19.Append(protection11);

            CellFormat cellFormat20 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)13U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment12 = new Alignment() { Horizontal = HorizontalAlignmentValues.Left, Vertical = VerticalAlignmentValues.Top, TextRotation = (UInt32Value)180U, WrapText = true };
            Protection protection12 = new Protection() { Locked = false };

            cellFormat20.Append(alignment12);
            cellFormat20.Append(protection12);

            CellFormat cellFormat21 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)15U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment13 = new Alignment() { Horizontal = HorizontalAlignmentValues.Left, Vertical = VerticalAlignmentValues.Top, TextRotation = (UInt32Value)180U, WrapText = true };
            Protection protection13 = new Protection() { Locked = false };

            cellFormat21.Append(alignment13);
            cellFormat21.Append(protection13);

            CellFormat cellFormat22 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)9U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment14 = new Alignment() { Horizontal = HorizontalAlignmentValues.Right, WrapText = true };

            cellFormat22.Append(alignment14);

            CellFormat cellFormat23 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)16U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment15 = new Alignment() { Horizontal = HorizontalAlignmentValues.Center };
            Protection protection14 = new Protection() { Locked = false };

            cellFormat23.Append(alignment15);
            cellFormat23.Append(protection14);

            CellFormat cellFormat24 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)12U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment16 = new Alignment() { Horizontal = HorizontalAlignmentValues.Center, WrapText = true };
            Protection protection15 = new Protection() { Hidden = true };

            cellFormat24.Append(alignment16);
            cellFormat24.Append(protection15);

            CellFormat cellFormat25 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)12U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment17 = new Alignment() { WrapText = true };
            Protection protection16 = new Protection() { Hidden = true };

            cellFormat25.Append(alignment17);
            cellFormat25.Append(protection16);
            CellFormat cellFormat26 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)17U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true };

            CellFormat cellFormat27 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)12U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment18 = new Alignment() { Horizontal = HorizontalAlignmentValues.Center, WrapText = true };
            Protection protection17 = new Protection() { Hidden = true };

            cellFormat27.Append(alignment18);
            cellFormat27.Append(protection17);

            CellFormat cellFormat28 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)12U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment19 = new Alignment() { Horizontal = HorizontalAlignmentValues.Right };

            cellFormat28.Append(alignment19);

            CellFormat cellFormat29 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)12U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment20 = new Alignment() { Horizontal = HorizontalAlignmentValues.Right, WrapText = true };

            cellFormat29.Append(alignment20);

            CellFormat cellFormat30 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)8U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment21 = new Alignment() { Horizontal = HorizontalAlignmentValues.Center };

            cellFormat30.Append(alignment21);

            CellFormat cellFormat31 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)3U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment22 = new Alignment() { Horizontal = HorizontalAlignmentValues.Center, WrapText = true };
            Protection protection18 = new Protection() { Locked = false };

            cellFormat31.Append(alignment22);
            cellFormat31.Append(protection18);

            CellFormat cellFormat32 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)2U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment23 = new Alignment() { Horizontal = HorizontalAlignmentValues.Left, WrapText = true };

            cellFormat32.Append(alignment23);

            CellFormat cellFormat33 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)3U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment24 = new Alignment() { Horizontal = HorizontalAlignmentValues.Left, WrapText = true };
            Protection protection19 = new Protection() { Locked = false };

            cellFormat33.Append(alignment24);
            cellFormat33.Append(protection19);

            CellFormat cellFormat34 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)2U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment25 = new Alignment() { Horizontal = HorizontalAlignmentValues.Center, WrapText = true };

            cellFormat34.Append(alignment25);

            CellFormat cellFormat35 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)5U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true, ApplyBorder = true, ApplyAlignment = true, ApplyProtection = true };
            Alignment alignment26 = new Alignment() { Horizontal = HorizontalAlignmentValues.Left, WrapText = true };

            cellFormat35.Append(alignment26);
            CellFormat cellFormat36 = new CellFormat() { NumberFormatId = (UInt32Value)0U, FontId = (UInt32Value)18U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, FormatId = (UInt32Value)0U, ApplyFont = true };

            cellFormats1.Append(cellFormat2);
            cellFormats1.Append(cellFormat3);
            cellFormats1.Append(cellFormat4);
            cellFormats1.Append(cellFormat5);
            cellFormats1.Append(cellFormat6);
            cellFormats1.Append(cellFormat7);
            cellFormats1.Append(cellFormat8);
            cellFormats1.Append(cellFormat9);
            cellFormats1.Append(cellFormat10);
            cellFormats1.Append(cellFormat11);
            cellFormats1.Append(cellFormat12);
            cellFormats1.Append(cellFormat13);
            cellFormats1.Append(cellFormat14);
            cellFormats1.Append(cellFormat15);
            cellFormats1.Append(cellFormat16);
            cellFormats1.Append(cellFormat17);
            cellFormats1.Append(cellFormat18);
            cellFormats1.Append(cellFormat19);
            cellFormats1.Append(cellFormat20);
            cellFormats1.Append(cellFormat21);
            cellFormats1.Append(cellFormat22);
            cellFormats1.Append(cellFormat23);
            cellFormats1.Append(cellFormat24);
            cellFormats1.Append(cellFormat25);
            cellFormats1.Append(cellFormat26);
            cellFormats1.Append(cellFormat27);
            cellFormats1.Append(cellFormat28);
            cellFormats1.Append(cellFormat29);
            cellFormats1.Append(cellFormat30);
            cellFormats1.Append(cellFormat31);
            cellFormats1.Append(cellFormat32);
            cellFormats1.Append(cellFormat33);
            cellFormats1.Append(cellFormat34);
            cellFormats1.Append(cellFormat35);
            cellFormats1.Append(cellFormat36);

            CellStyles cellStyles1 = new CellStyles() { Count = (UInt32Value)1U };
            CellStyle cellStyle1 = new CellStyle() { Name = "Normal", FormatId = (UInt32Value)0U, BuiltinId = (UInt32Value)0U };

            cellStyles1.Append(cellStyle1);

            DifferentialFormats differentialFormats1 = new DifferentialFormats() { Count = (UInt32Value)1U };

            DifferentialFormat differentialFormat1 = new DifferentialFormat();

            Font font20 = new Font();
            Condense condense1 = new Condense() { Val = false };
            Extend extend1 = new Extend() { Val = false };
            Color color13 = new Color() { Rgb = "FF9C6500" };

            font20.Append(condense1);
            font20.Append(extend1);
            font20.Append(color13);

            Fill fill3 = new Fill();

            PatternFill patternFill3 = new PatternFill();
            BackgroundColor backgroundColor1 = new BackgroundColor() { Rgb = "FFFFEB9C" };

            patternFill3.Append(backgroundColor1);

            fill3.Append(patternFill3);

            differentialFormat1.Append(font20);
            differentialFormat1.Append(fill3);

            differentialFormats1.Append(differentialFormat1);
            TableStyles tableStyles1 = new TableStyles() { Count = (UInt32Value)0U, DefaultTableStyle = "TableStyleMedium2", DefaultPivotStyle = "PivotStyleLight16" };

            Colors colors1 = new Colors();

            IndexedColors indexedColors1 = new IndexedColors();
            RgbColor rgbColor1 = new RgbColor() { Rgb = "00000000" };
            RgbColor rgbColor2 = new RgbColor() { Rgb = "00FFFFFF" };
            RgbColor rgbColor3 = new RgbColor() { Rgb = "00FF0000" };
            RgbColor rgbColor4 = new RgbColor() { Rgb = "0000FF00" };
            RgbColor rgbColor5 = new RgbColor() { Rgb = "000000FF" };
            RgbColor rgbColor6 = new RgbColor() { Rgb = "00FFFF00" };
            RgbColor rgbColor7 = new RgbColor() { Rgb = "00FF00FF" };
            RgbColor rgbColor8 = new RgbColor() { Rgb = "0000FFFF" };
            RgbColor rgbColor9 = new RgbColor() { Rgb = "00000000" };
            RgbColor rgbColor10 = new RgbColor() { Rgb = "00FFFFFF" };
            RgbColor rgbColor11 = new RgbColor() { Rgb = "00FF0000" };
            RgbColor rgbColor12 = new RgbColor() { Rgb = "0000FF00" };
            RgbColor rgbColor13 = new RgbColor() { Rgb = "000000FF" };
            RgbColor rgbColor14 = new RgbColor() { Rgb = "00FFFF00" };
            RgbColor rgbColor15 = new RgbColor() { Rgb = "00FF00FF" };
            RgbColor rgbColor16 = new RgbColor() { Rgb = "0000FFFF" };
            RgbColor rgbColor17 = new RgbColor() { Rgb = "00800000" };
            RgbColor rgbColor18 = new RgbColor() { Rgb = "00008000" };
            RgbColor rgbColor19 = new RgbColor() { Rgb = "00000080" };
            RgbColor rgbColor20 = new RgbColor() { Rgb = "00808000" };
            RgbColor rgbColor21 = new RgbColor() { Rgb = "00800080" };
            RgbColor rgbColor22 = new RgbColor() { Rgb = "00008080" };
            RgbColor rgbColor23 = new RgbColor() { Rgb = "00C0C0C0" };
            RgbColor rgbColor24 = new RgbColor() { Rgb = "00808080" };
            RgbColor rgbColor25 = new RgbColor() { Rgb = "009999FF" };
            RgbColor rgbColor26 = new RgbColor() { Rgb = "00993366" };
            RgbColor rgbColor27 = new RgbColor() { Rgb = "00FFFFCC" };
            RgbColor rgbColor28 = new RgbColor() { Rgb = "00CCFFFF" };
            RgbColor rgbColor29 = new RgbColor() { Rgb = "00660066" };
            RgbColor rgbColor30 = new RgbColor() { Rgb = "00FF8080" };
            RgbColor rgbColor31 = new RgbColor() { Rgb = "000066CC" };
            RgbColor rgbColor32 = new RgbColor() { Rgb = "00CCCCFF" };
            RgbColor rgbColor33 = new RgbColor() { Rgb = "00000080" };
            RgbColor rgbColor34 = new RgbColor() { Rgb = "00FF00FF" };
            RgbColor rgbColor35 = new RgbColor() { Rgb = "00FFFF00" };
            RgbColor rgbColor36 = new RgbColor() { Rgb = "0000FFFF" };
            RgbColor rgbColor37 = new RgbColor() { Rgb = "00800080" };
            RgbColor rgbColor38 = new RgbColor() { Rgb = "00800000" };
            RgbColor rgbColor39 = new RgbColor() { Rgb = "00008080" };
            RgbColor rgbColor40 = new RgbColor() { Rgb = "000000FF" };
            RgbColor rgbColor41 = new RgbColor() { Rgb = "0000CCFF" };
            RgbColor rgbColor42 = new RgbColor() { Rgb = "00CCFFFF" };
            RgbColor rgbColor43 = new RgbColor() { Rgb = "00CCFFCC" };
            RgbColor rgbColor44 = new RgbColor() { Rgb = "00FFFF99" };
            RgbColor rgbColor45 = new RgbColor() { Rgb = "0099CCFF" };
            RgbColor rgbColor46 = new RgbColor() { Rgb = "00FF99CC" };
            RgbColor rgbColor47 = new RgbColor() { Rgb = "00CC99FF" };
            RgbColor rgbColor48 = new RgbColor() { Rgb = "00FFCC99" };
            RgbColor rgbColor49 = new RgbColor() { Rgb = "003366FF" };
            RgbColor rgbColor50 = new RgbColor() { Rgb = "0033CCCC" };
            RgbColor rgbColor51 = new RgbColor() { Rgb = "0099CC00" };
            RgbColor rgbColor52 = new RgbColor() { Rgb = "00FFCC00" };
            RgbColor rgbColor53 = new RgbColor() { Rgb = "00FF9900" };
            RgbColor rgbColor54 = new RgbColor() { Rgb = "00FF6600" };
            RgbColor rgbColor55 = new RgbColor() { Rgb = "00666699" };
            RgbColor rgbColor56 = new RgbColor() { Rgb = "00969696" };
            RgbColor rgbColor57 = new RgbColor() { Rgb = "00003366" };
            RgbColor rgbColor58 = new RgbColor() { Rgb = "00339966" };
            RgbColor rgbColor59 = new RgbColor() { Rgb = "00003300" };
            RgbColor rgbColor60 = new RgbColor() { Rgb = "00333300" };
            RgbColor rgbColor61 = new RgbColor() { Rgb = "00993300" };
            RgbColor rgbColor62 = new RgbColor() { Rgb = "00993366" };
            RgbColor rgbColor63 = new RgbColor() { Rgb = "00333399" };
            RgbColor rgbColor64 = new RgbColor() { Rgb = "00333333" };

            indexedColors1.Append(rgbColor1);
            indexedColors1.Append(rgbColor2);
            indexedColors1.Append(rgbColor3);
            indexedColors1.Append(rgbColor4);
            indexedColors1.Append(rgbColor5);
            indexedColors1.Append(rgbColor6);
            indexedColors1.Append(rgbColor7);
            indexedColors1.Append(rgbColor8);
            indexedColors1.Append(rgbColor9);
            indexedColors1.Append(rgbColor10);
            indexedColors1.Append(rgbColor11);
            indexedColors1.Append(rgbColor12);
            indexedColors1.Append(rgbColor13);
            indexedColors1.Append(rgbColor14);
            indexedColors1.Append(rgbColor15);
            indexedColors1.Append(rgbColor16);
            indexedColors1.Append(rgbColor17);
            indexedColors1.Append(rgbColor18);
            indexedColors1.Append(rgbColor19);
            indexedColors1.Append(rgbColor20);
            indexedColors1.Append(rgbColor21);
            indexedColors1.Append(rgbColor22);
            indexedColors1.Append(rgbColor23);
            indexedColors1.Append(rgbColor24);
            indexedColors1.Append(rgbColor25);
            indexedColors1.Append(rgbColor26);
            indexedColors1.Append(rgbColor27);
            indexedColors1.Append(rgbColor28);
            indexedColors1.Append(rgbColor29);
            indexedColors1.Append(rgbColor30);
            indexedColors1.Append(rgbColor31);
            indexedColors1.Append(rgbColor32);
            indexedColors1.Append(rgbColor33);
            indexedColors1.Append(rgbColor34);
            indexedColors1.Append(rgbColor35);
            indexedColors1.Append(rgbColor36);
            indexedColors1.Append(rgbColor37);
            indexedColors1.Append(rgbColor38);
            indexedColors1.Append(rgbColor39);
            indexedColors1.Append(rgbColor40);
            indexedColors1.Append(rgbColor41);
            indexedColors1.Append(rgbColor42);
            indexedColors1.Append(rgbColor43);
            indexedColors1.Append(rgbColor44);
            indexedColors1.Append(rgbColor45);
            indexedColors1.Append(rgbColor46);
            indexedColors1.Append(rgbColor47);
            indexedColors1.Append(rgbColor48);
            indexedColors1.Append(rgbColor49);
            indexedColors1.Append(rgbColor50);
            indexedColors1.Append(rgbColor51);
            indexedColors1.Append(rgbColor52);
            indexedColors1.Append(rgbColor53);
            indexedColors1.Append(rgbColor54);
            indexedColors1.Append(rgbColor55);
            indexedColors1.Append(rgbColor56);
            indexedColors1.Append(rgbColor57);
            indexedColors1.Append(rgbColor58);
            indexedColors1.Append(rgbColor59);
            indexedColors1.Append(rgbColor60);
            indexedColors1.Append(rgbColor61);
            indexedColors1.Append(rgbColor62);
            indexedColors1.Append(rgbColor63);
            indexedColors1.Append(rgbColor64);

            colors1.Append(indexedColors1);

            StylesheetExtensionList stylesheetExtensionList1 = new StylesheetExtensionList();

            StylesheetExtension stylesheetExtension1 = new StylesheetExtension() { Uri = "{EB79DEF2-80B8-43e5-95BD-54CBDDF9020C}" };
            stylesheetExtension1.AddNamespaceDeclaration("x14", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main");
            X14.SlicerStyles slicerStyles1 = new X14.SlicerStyles() { DefaultSlicerStyle = "SlicerStyleLight1" };

            stylesheetExtension1.Append(slicerStyles1);

            StylesheetExtension stylesheetExtension2 = new StylesheetExtension() { Uri = "{9260A510-F301-46a8-8635-F512D64BE5F5}" };
            stylesheetExtension2.AddNamespaceDeclaration("x15", "http://schemas.microsoft.com/office/spreadsheetml/2010/11/main");

            OpenXmlUnknownElement openXmlUnknownElement3 = OpenXmlUnknownElement.CreateOpenXmlUnknownElement("<x15:timelineStyles defaultTimelineStyle=\"TimeSlicerStyleLight1\" xmlns:x15=\"http://schemas.microsoft.com/office/spreadsheetml/2010/11/main\" />");

            stylesheetExtension2.Append(openXmlUnknownElement3);

            stylesheetExtensionList1.Append(stylesheetExtension1);
            stylesheetExtensionList1.Append(stylesheetExtension2);

            stylesheet1.Append(numberingFormats1);
            stylesheet1.Append(fonts1);
            stylesheet1.Append(fills1);
            stylesheet1.Append(borders1);
            stylesheet1.Append(cellStyleFormats1);
            stylesheet1.Append(cellFormats1);
            stylesheet1.Append(cellStyles1);
            stylesheet1.Append(differentialFormats1);
            stylesheet1.Append(tableStyles1);
            stylesheet1.Append(colors1);
            stylesheet1.Append(stylesheetExtensionList1);

            workbookStylesPart1.Stylesheet = stylesheet1;
        }

        // Generates content of themePart1.
        private void GenerateThemePart1Content(ThemePart themePart1)
        {
            A.Theme theme1 = new A.Theme() { Name = "Office Theme" };
            theme1.AddNamespaceDeclaration("a", "http://schemas.openxmlformats.org/drawingml/2006/main");

            A.ThemeElements themeElements1 = new A.ThemeElements();

            A.ColorScheme colorScheme1 = new A.ColorScheme() { Name = "Office" };

            A.Dark1Color dark1Color1 = new A.Dark1Color();
            A.SystemColor systemColor1 = new A.SystemColor() { Val = A.SystemColorValues.WindowText, LastColor = "000000" };

            dark1Color1.Append(systemColor1);

            A.Light1Color light1Color1 = new A.Light1Color();
            A.SystemColor systemColor2 = new A.SystemColor() { Val = A.SystemColorValues.Window, LastColor = "FCFCFC" };

            light1Color1.Append(systemColor2);

            A.Dark2Color dark2Color1 = new A.Dark2Color();
            A.RgbColorModelHex rgbColorModelHex1 = new A.RgbColorModelHex() { Val = "44546A" };

            dark2Color1.Append(rgbColorModelHex1);

            A.Light2Color light2Color1 = new A.Light2Color();
            A.RgbColorModelHex rgbColorModelHex2 = new A.RgbColorModelHex() { Val = "E7E6E6" };

            light2Color1.Append(rgbColorModelHex2);

            A.Accent1Color accent1Color1 = new A.Accent1Color();
            A.RgbColorModelHex rgbColorModelHex3 = new A.RgbColorModelHex() { Val = "5B9BD5" };

            accent1Color1.Append(rgbColorModelHex3);

            A.Accent2Color accent2Color1 = new A.Accent2Color();
            A.RgbColorModelHex rgbColorModelHex4 = new A.RgbColorModelHex() { Val = "ED7D31" };

            accent2Color1.Append(rgbColorModelHex4);

            A.Accent3Color accent3Color1 = new A.Accent3Color();
            A.RgbColorModelHex rgbColorModelHex5 = new A.RgbColorModelHex() { Val = "A5A5A5" };

            accent3Color1.Append(rgbColorModelHex5);

            A.Accent4Color accent4Color1 = new A.Accent4Color();
            A.RgbColorModelHex rgbColorModelHex6 = new A.RgbColorModelHex() { Val = "FFC000" };

            accent4Color1.Append(rgbColorModelHex6);

            A.Accent5Color accent5Color1 = new A.Accent5Color();
            A.RgbColorModelHex rgbColorModelHex7 = new A.RgbColorModelHex() { Val = "4472C4" };

            accent5Color1.Append(rgbColorModelHex7);

            A.Accent6Color accent6Color1 = new A.Accent6Color();
            A.RgbColorModelHex rgbColorModelHex8 = new A.RgbColorModelHex() { Val = "70AD47" };

            accent6Color1.Append(rgbColorModelHex8);

            A.Hyperlink hyperlink1 = new A.Hyperlink();
            A.RgbColorModelHex rgbColorModelHex9 = new A.RgbColorModelHex() { Val = "0563C1" };

            hyperlink1.Append(rgbColorModelHex9);

            A.FollowedHyperlinkColor followedHyperlinkColor1 = new A.FollowedHyperlinkColor();
            A.RgbColorModelHex rgbColorModelHex10 = new A.RgbColorModelHex() { Val = "954F72" };

            followedHyperlinkColor1.Append(rgbColorModelHex10);

            colorScheme1.Append(dark1Color1);
            colorScheme1.Append(light1Color1);
            colorScheme1.Append(dark2Color1);
            colorScheme1.Append(light2Color1);
            colorScheme1.Append(accent1Color1);
            colorScheme1.Append(accent2Color1);
            colorScheme1.Append(accent3Color1);
            colorScheme1.Append(accent4Color1);
            colorScheme1.Append(accent5Color1);
            colorScheme1.Append(accent6Color1);
            colorScheme1.Append(hyperlink1);
            colorScheme1.Append(followedHyperlinkColor1);

            A.FontScheme fontScheme3 = new A.FontScheme() { Name = "Arial" };

            A.MajorFont majorFont1 = new A.MajorFont();
            A.LatinFont latinFont1 = new A.LatinFont() { Typeface = "Arial", Panose = "020B0604020202020204" };
            A.EastAsianFont eastAsianFont1 = new A.EastAsianFont() { Typeface = "" };
            A.ComplexScriptFont complexScriptFont1 = new A.ComplexScriptFont() { Typeface = "" };
            A.SupplementalFont supplementalFont1 = new A.SupplementalFont() { Script = "Jpan", Typeface = "ＭＳ Ｐゴシック" };
            A.SupplementalFont supplementalFont2 = new A.SupplementalFont() { Script = "Hang", Typeface = "굴림" };
            A.SupplementalFont supplementalFont3 = new A.SupplementalFont() { Script = "Hans", Typeface = "黑体" };
            A.SupplementalFont supplementalFont4 = new A.SupplementalFont() { Script = "Hant", Typeface = "微軟正黑體" };
            A.SupplementalFont supplementalFont5 = new A.SupplementalFont() { Script = "Arab", Typeface = "Arial" };
            A.SupplementalFont supplementalFont6 = new A.SupplementalFont() { Script = "Hebr", Typeface = "Arial" };
            A.SupplementalFont supplementalFont7 = new A.SupplementalFont() { Script = "Thai", Typeface = "Cordia New" };
            A.SupplementalFont supplementalFont8 = new A.SupplementalFont() { Script = "Ethi", Typeface = "Nyala" };
            A.SupplementalFont supplementalFont9 = new A.SupplementalFont() { Script = "Beng", Typeface = "Vrinda" };
            A.SupplementalFont supplementalFont10 = new A.SupplementalFont() { Script = "Gujr", Typeface = "Shruti" };
            A.SupplementalFont supplementalFont11 = new A.SupplementalFont() { Script = "Khmr", Typeface = "DaunPenh" };
            A.SupplementalFont supplementalFont12 = new A.SupplementalFont() { Script = "Knda", Typeface = "Tunga" };
            A.SupplementalFont supplementalFont13 = new A.SupplementalFont() { Script = "Guru", Typeface = "Raavi" };
            A.SupplementalFont supplementalFont14 = new A.SupplementalFont() { Script = "Cans", Typeface = "Euphemia" };
            A.SupplementalFont supplementalFont15 = new A.SupplementalFont() { Script = "Cher", Typeface = "Plantagenet Cherokee" };
            A.SupplementalFont supplementalFont16 = new A.SupplementalFont() { Script = "Yiii", Typeface = "Microsoft Yi Baiti" };
            A.SupplementalFont supplementalFont17 = new A.SupplementalFont() { Script = "Tibt", Typeface = "Microsoft Himalaya" };
            A.SupplementalFont supplementalFont18 = new A.SupplementalFont() { Script = "Thaa", Typeface = "MV Boli" };
            A.SupplementalFont supplementalFont19 = new A.SupplementalFont() { Script = "Deva", Typeface = "Mangal" };
            A.SupplementalFont supplementalFont20 = new A.SupplementalFont() { Script = "Telu", Typeface = "Gautami" };
            A.SupplementalFont supplementalFont21 = new A.SupplementalFont() { Script = "Taml", Typeface = "Latha" };
            A.SupplementalFont supplementalFont22 = new A.SupplementalFont() { Script = "Syrc", Typeface = "Estrangelo Edessa" };
            A.SupplementalFont supplementalFont23 = new A.SupplementalFont() { Script = "Orya", Typeface = "Kalinga" };
            A.SupplementalFont supplementalFont24 = new A.SupplementalFont() { Script = "Mlym", Typeface = "Kartika" };
            A.SupplementalFont supplementalFont25 = new A.SupplementalFont() { Script = "Laoo", Typeface = "DokChampa" };
            A.SupplementalFont supplementalFont26 = new A.SupplementalFont() { Script = "Sinh", Typeface = "Iskoola Pota" };
            A.SupplementalFont supplementalFont27 = new A.SupplementalFont() { Script = "Mong", Typeface = "Mongolian Baiti" };
            A.SupplementalFont supplementalFont28 = new A.SupplementalFont() { Script = "Viet", Typeface = "Arial" };
            A.SupplementalFont supplementalFont29 = new A.SupplementalFont() { Script = "Uigh", Typeface = "Microsoft Uighur" };
            A.SupplementalFont supplementalFont30 = new A.SupplementalFont() { Script = "Geor", Typeface = "Sylfaen" };

            majorFont1.Append(latinFont1);
            majorFont1.Append(eastAsianFont1);
            majorFont1.Append(complexScriptFont1);
            majorFont1.Append(supplementalFont1);
            majorFont1.Append(supplementalFont2);
            majorFont1.Append(supplementalFont3);
            majorFont1.Append(supplementalFont4);
            majorFont1.Append(supplementalFont5);
            majorFont1.Append(supplementalFont6);
            majorFont1.Append(supplementalFont7);
            majorFont1.Append(supplementalFont8);
            majorFont1.Append(supplementalFont9);
            majorFont1.Append(supplementalFont10);
            majorFont1.Append(supplementalFont11);
            majorFont1.Append(supplementalFont12);
            majorFont1.Append(supplementalFont13);
            majorFont1.Append(supplementalFont14);
            majorFont1.Append(supplementalFont15);
            majorFont1.Append(supplementalFont16);
            majorFont1.Append(supplementalFont17);
            majorFont1.Append(supplementalFont18);
            majorFont1.Append(supplementalFont19);
            majorFont1.Append(supplementalFont20);
            majorFont1.Append(supplementalFont21);
            majorFont1.Append(supplementalFont22);
            majorFont1.Append(supplementalFont23);
            majorFont1.Append(supplementalFont24);
            majorFont1.Append(supplementalFont25);
            majorFont1.Append(supplementalFont26);
            majorFont1.Append(supplementalFont27);
            majorFont1.Append(supplementalFont28);
            majorFont1.Append(supplementalFont29);
            majorFont1.Append(supplementalFont30);

            A.MinorFont minorFont1 = new A.MinorFont();
            A.LatinFont latinFont2 = new A.LatinFont() { Typeface = "Arial", Panose = "020B0604020202020204" };
            A.EastAsianFont eastAsianFont2 = new A.EastAsianFont() { Typeface = "" };
            A.ComplexScriptFont complexScriptFont2 = new A.ComplexScriptFont() { Typeface = "" };
            A.SupplementalFont supplementalFont31 = new A.SupplementalFont() { Script = "Jpan", Typeface = "ＭＳ Ｐゴシック" };
            A.SupplementalFont supplementalFont32 = new A.SupplementalFont() { Script = "Hang", Typeface = "굴림" };
            A.SupplementalFont supplementalFont33 = new A.SupplementalFont() { Script = "Hans", Typeface = "黑体" };
            A.SupplementalFont supplementalFont34 = new A.SupplementalFont() { Script = "Hant", Typeface = "微軟正黑體" };
            A.SupplementalFont supplementalFont35 = new A.SupplementalFont() { Script = "Arab", Typeface = "Arial" };
            A.SupplementalFont supplementalFont36 = new A.SupplementalFont() { Script = "Hebr", Typeface = "Arial" };
            A.SupplementalFont supplementalFont37 = new A.SupplementalFont() { Script = "Thai", Typeface = "Cordia New" };
            A.SupplementalFont supplementalFont38 = new A.SupplementalFont() { Script = "Ethi", Typeface = "Nyala" };
            A.SupplementalFont supplementalFont39 = new A.SupplementalFont() { Script = "Beng", Typeface = "Vrinda" };
            A.SupplementalFont supplementalFont40 = new A.SupplementalFont() { Script = "Gujr", Typeface = "Shruti" };
            A.SupplementalFont supplementalFont41 = new A.SupplementalFont() { Script = "Khmr", Typeface = "DaunPenh" };
            A.SupplementalFont supplementalFont42 = new A.SupplementalFont() { Script = "Knda", Typeface = "Tunga" };
            A.SupplementalFont supplementalFont43 = new A.SupplementalFont() { Script = "Guru", Typeface = "Raavi" };
            A.SupplementalFont supplementalFont44 = new A.SupplementalFont() { Script = "Cans", Typeface = "Euphemia" };
            A.SupplementalFont supplementalFont45 = new A.SupplementalFont() { Script = "Cher", Typeface = "Plantagenet Cherokee" };
            A.SupplementalFont supplementalFont46 = new A.SupplementalFont() { Script = "Yiii", Typeface = "Microsoft Yi Baiti" };
            A.SupplementalFont supplementalFont47 = new A.SupplementalFont() { Script = "Tibt", Typeface = "Microsoft Himalaya" };
            A.SupplementalFont supplementalFont48 = new A.SupplementalFont() { Script = "Thaa", Typeface = "MV Boli" };
            A.SupplementalFont supplementalFont49 = new A.SupplementalFont() { Script = "Deva", Typeface = "Mangal" };
            A.SupplementalFont supplementalFont50 = new A.SupplementalFont() { Script = "Telu", Typeface = "Gautami" };
            A.SupplementalFont supplementalFont51 = new A.SupplementalFont() { Script = "Taml", Typeface = "Latha" };
            A.SupplementalFont supplementalFont52 = new A.SupplementalFont() { Script = "Syrc", Typeface = "Estrangelo Edessa" };
            A.SupplementalFont supplementalFont53 = new A.SupplementalFont() { Script = "Orya", Typeface = "Kalinga" };
            A.SupplementalFont supplementalFont54 = new A.SupplementalFont() { Script = "Mlym", Typeface = "Kartika" };
            A.SupplementalFont supplementalFont55 = new A.SupplementalFont() { Script = "Laoo", Typeface = "DokChampa" };
            A.SupplementalFont supplementalFont56 = new A.SupplementalFont() { Script = "Sinh", Typeface = "Iskoola Pota" };
            A.SupplementalFont supplementalFont57 = new A.SupplementalFont() { Script = "Mong", Typeface = "Mongolian Baiti" };
            A.SupplementalFont supplementalFont58 = new A.SupplementalFont() { Script = "Viet", Typeface = "Arial" };
            A.SupplementalFont supplementalFont59 = new A.SupplementalFont() { Script = "Uigh", Typeface = "Microsoft Uighur" };
            A.SupplementalFont supplementalFont60 = new A.SupplementalFont() { Script = "Geor", Typeface = "Sylfaen" };

            minorFont1.Append(latinFont2);
            minorFont1.Append(eastAsianFont2);
            minorFont1.Append(complexScriptFont2);
            minorFont1.Append(supplementalFont31);
            minorFont1.Append(supplementalFont32);
            minorFont1.Append(supplementalFont33);
            minorFont1.Append(supplementalFont34);
            minorFont1.Append(supplementalFont35);
            minorFont1.Append(supplementalFont36);
            minorFont1.Append(supplementalFont37);
            minorFont1.Append(supplementalFont38);
            minorFont1.Append(supplementalFont39);
            minorFont1.Append(supplementalFont40);
            minorFont1.Append(supplementalFont41);
            minorFont1.Append(supplementalFont42);
            minorFont1.Append(supplementalFont43);
            minorFont1.Append(supplementalFont44);
            minorFont1.Append(supplementalFont45);
            minorFont1.Append(supplementalFont46);
            minorFont1.Append(supplementalFont47);
            minorFont1.Append(supplementalFont48);
            minorFont1.Append(supplementalFont49);
            minorFont1.Append(supplementalFont50);
            minorFont1.Append(supplementalFont51);
            minorFont1.Append(supplementalFont52);
            minorFont1.Append(supplementalFont53);
            minorFont1.Append(supplementalFont54);
            minorFont1.Append(supplementalFont55);
            minorFont1.Append(supplementalFont56);
            minorFont1.Append(supplementalFont57);
            minorFont1.Append(supplementalFont58);
            minorFont1.Append(supplementalFont59);
            minorFont1.Append(supplementalFont60);

            fontScheme3.Append(majorFont1);
            fontScheme3.Append(minorFont1);

            A.FormatScheme formatScheme1 = new A.FormatScheme() { Name = "Office" };

            A.FillStyleList fillStyleList1 = new A.FillStyleList();

            A.SolidFill solidFill1 = new A.SolidFill();
            A.SchemeColor schemeColor1 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };

            solidFill1.Append(schemeColor1);

            A.GradientFill gradientFill1 = new A.GradientFill() { RotateWithShape = true };

            A.GradientStopList gradientStopList1 = new A.GradientStopList();

            A.GradientStop gradientStop1 = new A.GradientStop() { Position = 0 };

            A.SchemeColor schemeColor2 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.LuminanceModulation luminanceModulation1 = new A.LuminanceModulation() { Val = 110000 };
            A.SaturationModulation saturationModulation1 = new A.SaturationModulation() { Val = 105000 };
            A.Tint tint1 = new A.Tint() { Val = 67000 };

            schemeColor2.Append(luminanceModulation1);
            schemeColor2.Append(saturationModulation1);
            schemeColor2.Append(tint1);

            gradientStop1.Append(schemeColor2);

            A.GradientStop gradientStop2 = new A.GradientStop() { Position = 50000 };

            A.SchemeColor schemeColor3 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.LuminanceModulation luminanceModulation2 = new A.LuminanceModulation() { Val = 105000 };
            A.SaturationModulation saturationModulation2 = new A.SaturationModulation() { Val = 103000 };
            A.Tint tint2 = new A.Tint() { Val = 73000 };

            schemeColor3.Append(luminanceModulation2);
            schemeColor3.Append(saturationModulation2);
            schemeColor3.Append(tint2);

            gradientStop2.Append(schemeColor3);

            A.GradientStop gradientStop3 = new A.GradientStop() { Position = 100000 };

            A.SchemeColor schemeColor4 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.LuminanceModulation luminanceModulation3 = new A.LuminanceModulation() { Val = 105000 };
            A.SaturationModulation saturationModulation3 = new A.SaturationModulation() { Val = 109000 };
            A.Tint tint3 = new A.Tint() { Val = 81000 };

            schemeColor4.Append(luminanceModulation3);
            schemeColor4.Append(saturationModulation3);
            schemeColor4.Append(tint3);

            gradientStop3.Append(schemeColor4);

            gradientStopList1.Append(gradientStop1);
            gradientStopList1.Append(gradientStop2);
            gradientStopList1.Append(gradientStop3);
            A.LinearGradientFill linearGradientFill1 = new A.LinearGradientFill() { Angle = 5400000, Scaled = false };

            gradientFill1.Append(gradientStopList1);
            gradientFill1.Append(linearGradientFill1);

            A.GradientFill gradientFill2 = new A.GradientFill() { RotateWithShape = true };

            A.GradientStopList gradientStopList2 = new A.GradientStopList();

            A.GradientStop gradientStop4 = new A.GradientStop() { Position = 0 };

            A.SchemeColor schemeColor5 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.SaturationModulation saturationModulation4 = new A.SaturationModulation() { Val = 103000 };
            A.LuminanceModulation luminanceModulation4 = new A.LuminanceModulation() { Val = 102000 };
            A.Tint tint4 = new A.Tint() { Val = 94000 };

            schemeColor5.Append(saturationModulation4);
            schemeColor5.Append(luminanceModulation4);
            schemeColor5.Append(tint4);

            gradientStop4.Append(schemeColor5);

            A.GradientStop gradientStop5 = new A.GradientStop() { Position = 50000 };

            A.SchemeColor schemeColor6 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.SaturationModulation saturationModulation5 = new A.SaturationModulation() { Val = 110000 };
            A.LuminanceModulation luminanceModulation5 = new A.LuminanceModulation() { Val = 100000 };
            A.Shade shade1 = new A.Shade() { Val = 100000 };

            schemeColor6.Append(saturationModulation5);
            schemeColor6.Append(luminanceModulation5);
            schemeColor6.Append(shade1);

            gradientStop5.Append(schemeColor6);

            A.GradientStop gradientStop6 = new A.GradientStop() { Position = 100000 };

            A.SchemeColor schemeColor7 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.LuminanceModulation luminanceModulation6 = new A.LuminanceModulation() { Val = 99000 };
            A.SaturationModulation saturationModulation6 = new A.SaturationModulation() { Val = 120000 };
            A.Shade shade2 = new A.Shade() { Val = 78000 };

            schemeColor7.Append(luminanceModulation6);
            schemeColor7.Append(saturationModulation6);
            schemeColor7.Append(shade2);

            gradientStop6.Append(schemeColor7);

            gradientStopList2.Append(gradientStop4);
            gradientStopList2.Append(gradientStop5);
            gradientStopList2.Append(gradientStop6);
            A.LinearGradientFill linearGradientFill2 = new A.LinearGradientFill() { Angle = 5400000, Scaled = false };

            gradientFill2.Append(gradientStopList2);
            gradientFill2.Append(linearGradientFill2);

            fillStyleList1.Append(solidFill1);
            fillStyleList1.Append(gradientFill1);
            fillStyleList1.Append(gradientFill2);

            A.LineStyleList lineStyleList1 = new A.LineStyleList();

            A.Outline outline1 = new A.Outline() { Width = 6350, CapType = A.LineCapValues.Flat, CompoundLineType = A.CompoundLineValues.Single, Alignment = A.PenAlignmentValues.Center };

            A.SolidFill solidFill2 = new A.SolidFill();
            A.SchemeColor schemeColor8 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };

            solidFill2.Append(schemeColor8);
            A.PresetDash presetDash1 = new A.PresetDash() { Val = A.PresetLineDashValues.Solid };
            A.Miter miter1 = new A.Miter() { Limit = 800000 };

            outline1.Append(solidFill2);
            outline1.Append(presetDash1);
            outline1.Append(miter1);

            A.Outline outline2 = new A.Outline() { Width = 12700, CapType = A.LineCapValues.Flat, CompoundLineType = A.CompoundLineValues.Single, Alignment = A.PenAlignmentValues.Center };

            A.SolidFill solidFill3 = new A.SolidFill();
            A.SchemeColor schemeColor9 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };

            solidFill3.Append(schemeColor9);
            A.PresetDash presetDash2 = new A.PresetDash() { Val = A.PresetLineDashValues.Solid };
            A.Miter miter2 = new A.Miter() { Limit = 800000 };

            outline2.Append(solidFill3);
            outline2.Append(presetDash2);
            outline2.Append(miter2);

            A.Outline outline3 = new A.Outline() { Width = 19050, CapType = A.LineCapValues.Flat, CompoundLineType = A.CompoundLineValues.Single, Alignment = A.PenAlignmentValues.Center };

            A.SolidFill solidFill4 = new A.SolidFill();
            A.SchemeColor schemeColor10 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };

            solidFill4.Append(schemeColor10);
            A.PresetDash presetDash3 = new A.PresetDash() { Val = A.PresetLineDashValues.Solid };
            A.Miter miter3 = new A.Miter() { Limit = 800000 };

            outline3.Append(solidFill4);
            outline3.Append(presetDash3);
            outline3.Append(miter3);

            lineStyleList1.Append(outline1);
            lineStyleList1.Append(outline2);
            lineStyleList1.Append(outline3);

            A.EffectStyleList effectStyleList1 = new A.EffectStyleList();

            A.EffectStyle effectStyle1 = new A.EffectStyle();
            A.EffectList effectList1 = new A.EffectList();

            effectStyle1.Append(effectList1);

            A.EffectStyle effectStyle2 = new A.EffectStyle();
            A.EffectList effectList2 = new A.EffectList();

            effectStyle2.Append(effectList2);

            A.EffectStyle effectStyle3 = new A.EffectStyle();

            A.EffectList effectList3 = new A.EffectList();

            A.OuterShadow outerShadow1 = new A.OuterShadow() { BlurRadius = 57150L, Distance = 19050L, Direction = 5400000, Alignment = A.RectangleAlignmentValues.Center, RotateWithShape = false };

            A.RgbColorModelHex rgbColorModelHex11 = new A.RgbColorModelHex() { Val = "000000" };
            A.Alpha alpha1 = new A.Alpha() { Val = 63000 };

            rgbColorModelHex11.Append(alpha1);

            outerShadow1.Append(rgbColorModelHex11);

            effectList3.Append(outerShadow1);

            effectStyle3.Append(effectList3);

            effectStyleList1.Append(effectStyle1);
            effectStyleList1.Append(effectStyle2);
            effectStyleList1.Append(effectStyle3);

            A.BackgroundFillStyleList backgroundFillStyleList1 = new A.BackgroundFillStyleList();

            A.SolidFill solidFill5 = new A.SolidFill();
            A.SchemeColor schemeColor11 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };

            solidFill5.Append(schemeColor11);

            A.SolidFill solidFill6 = new A.SolidFill();

            A.SchemeColor schemeColor12 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.Tint tint5 = new A.Tint() { Val = 95000 };
            A.SaturationModulation saturationModulation7 = new A.SaturationModulation() { Val = 170000 };

            schemeColor12.Append(tint5);
            schemeColor12.Append(saturationModulation7);

            solidFill6.Append(schemeColor12);

            A.GradientFill gradientFill3 = new A.GradientFill() { RotateWithShape = true };

            A.GradientStopList gradientStopList3 = new A.GradientStopList();

            A.GradientStop gradientStop7 = new A.GradientStop() { Position = 0 };

            A.SchemeColor schemeColor13 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.Tint tint6 = new A.Tint() { Val = 93000 };
            A.SaturationModulation saturationModulation8 = new A.SaturationModulation() { Val = 150000 };
            A.Shade shade3 = new A.Shade() { Val = 98000 };
            A.LuminanceModulation luminanceModulation7 = new A.LuminanceModulation() { Val = 102000 };

            schemeColor13.Append(tint6);
            schemeColor13.Append(saturationModulation8);
            schemeColor13.Append(shade3);
            schemeColor13.Append(luminanceModulation7);

            gradientStop7.Append(schemeColor13);

            A.GradientStop gradientStop8 = new A.GradientStop() { Position = 50000 };

            A.SchemeColor schemeColor14 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.Tint tint7 = new A.Tint() { Val = 98000 };
            A.SaturationModulation saturationModulation9 = new A.SaturationModulation() { Val = 130000 };
            A.Shade shade4 = new A.Shade() { Val = 90000 };
            A.LuminanceModulation luminanceModulation8 = new A.LuminanceModulation() { Val = 103000 };

            schemeColor14.Append(tint7);
            schemeColor14.Append(saturationModulation9);
            schemeColor14.Append(shade4);
            schemeColor14.Append(luminanceModulation8);

            gradientStop8.Append(schemeColor14);

            A.GradientStop gradientStop9 = new A.GradientStop() { Position = 100000 };

            A.SchemeColor schemeColor15 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.Shade shade5 = new A.Shade() { Val = 63000 };
            A.SaturationModulation saturationModulation10 = new A.SaturationModulation() { Val = 120000 };

            schemeColor15.Append(shade5);
            schemeColor15.Append(saturationModulation10);

            gradientStop9.Append(schemeColor15);

            gradientStopList3.Append(gradientStop7);
            gradientStopList3.Append(gradientStop8);
            gradientStopList3.Append(gradientStop9);
            A.LinearGradientFill linearGradientFill3 = new A.LinearGradientFill() { Angle = 5400000, Scaled = false };

            gradientFill3.Append(gradientStopList3);
            gradientFill3.Append(linearGradientFill3);

            backgroundFillStyleList1.Append(solidFill5);
            backgroundFillStyleList1.Append(solidFill6);
            backgroundFillStyleList1.Append(gradientFill3);

            formatScheme1.Append(fillStyleList1);
            formatScheme1.Append(lineStyleList1);
            formatScheme1.Append(effectStyleList1);
            formatScheme1.Append(backgroundFillStyleList1);

            themeElements1.Append(colorScheme1);
            themeElements1.Append(fontScheme3);
            themeElements1.Append(formatScheme1);
            A.ObjectDefaults objectDefaults1 = new A.ObjectDefaults();
            A.ExtraColorSchemeList extraColorSchemeList1 = new A.ExtraColorSchemeList();

            A.ExtensionList extensionList1 = new A.ExtensionList();

            A.Extension extension1 = new A.Extension() { Uri = "{05A4C25C-085E-4340-85A3-A5531E510DB2}" };

            OpenXmlUnknownElement openXmlUnknownElement4 = OpenXmlUnknownElement.CreateOpenXmlUnknownElement("<thm15:themeFamily xmlns:thm15=\"http://schemas.microsoft.com/office/thememl/2012/main\" name=\"Office Theme\" id=\"{62F939B6-93AF-4DB8-9C6B-D6C7DFDC589F}\" vid=\"{4A3C46E8-61CC-4603-A589-7422A47A8E4A}\" />");

            extension1.Append(openXmlUnknownElement4);

            extensionList1.Append(extension1);

            theme1.Append(themeElements1);
            theme1.Append(objectDefaults1);
            theme1.Append(extraColorSchemeList1);
            theme1.Append(extensionList1);

            themePart1.Theme = theme1;
        }

        // Generates content of worksheetPart1.
        private void GenerateWorksheetPart1Content(WorksheetPart worksheetPart1)
        {
            Worksheet worksheet1 = new Worksheet() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "x14ac" } };
            worksheet1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            worksheet1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            worksheet1.AddNamespaceDeclaration("x14ac", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac");

            SheetProperties sheetProperties1 = new SheetProperties();
            PageSetupProperties pageSetupProperties1 = new PageSetupProperties() { FitToPage = true };

            sheetProperties1.Append(pageSetupProperties1);
            SheetDimension sheetDimension1 = new SheetDimension() { Reference = "A1:AR5" };

            SheetViews sheetViews1 = new SheetViews();

            SheetView sheetView1 = new SheetView() { TabSelected = true, ZoomScaleNormal = (UInt32Value)100U, WorkbookViewId = (UInt32Value)0U };
            Selection selection1 = new Selection() { ActiveCell = "A5", SequenceOfReferences = new ListValue<StringValue>() { InnerText = "A5" } };

            sheetView1.Append(selection1);

            sheetViews1.Append(sheetView1);
            SheetFormatProperties sheetFormatProperties1 = new SheetFormatProperties() { DefaultRowHeight = 14.25D, DyDescent = 0.2D };

            Columns columns1 = new Columns();
            Column column1 = new Column() { Min = (UInt32Value)1U, Max = (UInt32Value)1U, Width = 4D, CustomWidth = true };
            Column column2 = new Column() { Min = (UInt32Value)2U, Max = (UInt32Value)2U, Width = 24.25D, CustomWidth = true };
            Column column3 = new Column() { Min = (UInt32Value)3U, Max = (UInt32Value)3U, Width = 12.25D, CustomWidth = true };
            Column column4 = new Column() { Min = (UInt32Value)4U, Max = (UInt32Value)38U, Width = 3.25D, CustomWidth = true };
            Column column5 = new Column() { Min = (UInt32Value)39U, Max = (UInt32Value)40U, Width = 2.625D, CustomWidth = true };
            Column column6 = new Column() { Min = (UInt32Value)41U, Max = (UInt32Value)41U, Width = 3.25D, CustomWidth = true };
            Column column7 = new Column() { Min = (UInt32Value)42U, Max = (UInt32Value)43U, Width = 3.625D, CustomWidth = true };
            Column column8 = new Column() { Min = (UInt32Value)44U, Max = (UInt32Value)44U, Width = 5.625D, CustomWidth = true };

            columns1.Append(column1);
            columns1.Append(column2);
            columns1.Append(column3);
            columns1.Append(column4);
            columns1.Append(column5);
            columns1.Append(column6);
            columns1.Append(column7);
            columns1.Append(column8);

            SheetData sheetData1 = new SheetData();

            Row row1 = new Row() { RowIndex = (UInt32Value)1U, Spans = new ListValue<StringValue>() { InnerText = "1:44" }, Height = 15.75D, DyDescent = 0.25D };

            Cell cell1 = new Cell() { CellReference = "A1", StyleIndex = (UInt32Value)28U, DataType = CellValues.SharedString };
            CellValue cellValue1 = new CellValue();
            cellValue1.Text = "0";

            cell1.Append(cellValue1);
            Cell cell2 = new Cell() { CellReference = "B1", StyleIndex = (UInt32Value)28U };
            Cell cell3 = new Cell() { CellReference = "C1", StyleIndex = (UInt32Value)28U };
            Cell cell4 = new Cell() { CellReference = "D1", StyleIndex = (UInt32Value)28U };
            Cell cell5 = new Cell() { CellReference = "E1", StyleIndex = (UInt32Value)28U };
            Cell cell6 = new Cell() { CellReference = "F1", StyleIndex = (UInt32Value)28U };
            Cell cell7 = new Cell() { CellReference = "G1", StyleIndex = (UInt32Value)28U };
            Cell cell8 = new Cell() { CellReference = "H1", StyleIndex = (UInt32Value)28U };
            Cell cell9 = new Cell() { CellReference = "I1", StyleIndex = (UInt32Value)28U };
            Cell cell10 = new Cell() { CellReference = "J1", StyleIndex = (UInt32Value)28U };
            Cell cell11 = new Cell() { CellReference = "K1", StyleIndex = (UInt32Value)28U };
            Cell cell12 = new Cell() { CellReference = "L1", StyleIndex = (UInt32Value)28U };
            Cell cell13 = new Cell() { CellReference = "M1", StyleIndex = (UInt32Value)28U };
            Cell cell14 = new Cell() { CellReference = "N1", StyleIndex = (UInt32Value)28U };
            Cell cell15 = new Cell() { CellReference = "O1", StyleIndex = (UInt32Value)28U };
            Cell cell16 = new Cell() { CellReference = "P1", StyleIndex = (UInt32Value)28U };
            Cell cell17 = new Cell() { CellReference = "Q1", StyleIndex = (UInt32Value)28U };
            Cell cell18 = new Cell() { CellReference = "R1", StyleIndex = (UInt32Value)28U };
            Cell cell19 = new Cell() { CellReference = "S1", StyleIndex = (UInt32Value)28U };
            Cell cell20 = new Cell() { CellReference = "T1", StyleIndex = (UInt32Value)28U };
            Cell cell21 = new Cell() { CellReference = "U1", StyleIndex = (UInt32Value)28U };
            Cell cell22 = new Cell() { CellReference = "V1", StyleIndex = (UInt32Value)28U };
            Cell cell23 = new Cell() { CellReference = "W1", StyleIndex = (UInt32Value)28U };
            Cell cell24 = new Cell() { CellReference = "X1", StyleIndex = (UInt32Value)28U };
            Cell cell25 = new Cell() { CellReference = "Y1", StyleIndex = (UInt32Value)28U };
            Cell cell26 = new Cell() { CellReference = "Z1", StyleIndex = (UInt32Value)28U };
            Cell cell27 = new Cell() { CellReference = "AA1", StyleIndex = (UInt32Value)28U };
            Cell cell28 = new Cell() { CellReference = "AB1", StyleIndex = (UInt32Value)28U };
            Cell cell29 = new Cell() { CellReference = "AC1", StyleIndex = (UInt32Value)28U };
            Cell cell30 = new Cell() { CellReference = "AD1", StyleIndex = (UInt32Value)28U };
            Cell cell31 = new Cell() { CellReference = "AE1", StyleIndex = (UInt32Value)28U };
            Cell cell32 = new Cell() { CellReference = "AF1", StyleIndex = (UInt32Value)28U };
            Cell cell33 = new Cell() { CellReference = "AG1", StyleIndex = (UInt32Value)28U };
            Cell cell34 = new Cell() { CellReference = "AH1", StyleIndex = (UInt32Value)28U };
            Cell cell35 = new Cell() { CellReference = "AI1", StyleIndex = (UInt32Value)28U };
            Cell cell36 = new Cell() { CellReference = "AJ1", StyleIndex = (UInt32Value)28U };
            Cell cell37 = new Cell() { CellReference = "AK1", StyleIndex = (UInt32Value)28U };
            Cell cell38 = new Cell() { CellReference = "AL1", StyleIndex = (UInt32Value)28U };
            Cell cell39 = new Cell() { CellReference = "AM1", StyleIndex = (UInt32Value)28U };
            Cell cell40 = new Cell() { CellReference = "AN1", StyleIndex = (UInt32Value)28U };
            Cell cell41 = new Cell() { CellReference = "AO1", StyleIndex = (UInt32Value)28U };
            Cell cell42 = new Cell() { CellReference = "AP1", StyleIndex = (UInt32Value)28U };
            Cell cell43 = new Cell() { CellReference = "AQ1", StyleIndex = (UInt32Value)28U };
            Cell cell44 = new Cell() { CellReference = "AR1", StyleIndex = (UInt32Value)28U };

            row1.Append(cell1);
            row1.Append(cell2);
            row1.Append(cell3);
            row1.Append(cell4);
            row1.Append(cell5);
            row1.Append(cell6);
            row1.Append(cell7);
            row1.Append(cell8);
            row1.Append(cell9);
            row1.Append(cell10);
            row1.Append(cell11);
            row1.Append(cell12);
            row1.Append(cell13);
            row1.Append(cell14);
            row1.Append(cell15);
            row1.Append(cell16);
            row1.Append(cell17);
            row1.Append(cell18);
            row1.Append(cell19);
            row1.Append(cell20);
            row1.Append(cell21);
            row1.Append(cell22);
            row1.Append(cell23);
            row1.Append(cell24);
            row1.Append(cell25);
            row1.Append(cell26);
            row1.Append(cell27);
            row1.Append(cell28);
            row1.Append(cell29);
            row1.Append(cell30);
            row1.Append(cell31);
            row1.Append(cell32);
            row1.Append(cell33);
            row1.Append(cell34);
            row1.Append(cell35);
            row1.Append(cell36);
            row1.Append(cell37);
            row1.Append(cell38);
            row1.Append(cell39);
            row1.Append(cell40);
            row1.Append(cell41);
            row1.Append(cell42);
            row1.Append(cell43);
            row1.Append(cell44);

            Row row2 = new Row() { RowIndex = (UInt32Value)2U, Spans = new ListValue<StringValue>() { InnerText = "1:44" }, Height = 18.75D, DyDescent = 0.3D };
            Cell cell45 = new Cell() { CellReference = "A2", StyleIndex = (UInt32Value)1U };

            Cell cell46 = new Cell() { CellReference = "B2", StyleIndex = (UInt32Value)2U, DataType = CellValues.SharedString };
            CellValue cellValue2 = new CellValue();
            cellValue2.Text = "1";

            cell46.Append(cellValue2);

            Cell cell47 = new Cell() { CellReference = "C2", StyleIndex = (UInt32Value)29U, DataType = CellValues.SharedString };
            CellValue cellValue3 = new CellValue();
            cellValue3.Text = "2";

            cell47.Append(cellValue3);
            Cell cell48 = new Cell() { CellReference = "D2", StyleIndex = (UInt32Value)29U };
            Cell cell49 = new Cell() { CellReference = "E2", StyleIndex = (UInt32Value)29U };
            Cell cell50 = new Cell() { CellReference = "F2", StyleIndex = (UInt32Value)29U };
            Cell cell51 = new Cell() { CellReference = "G2", StyleIndex = (UInt32Value)29U };
            Cell cell52 = new Cell() { CellReference = "H2", StyleIndex = (UInt32Value)21U };

            Cell cell53 = new Cell() { CellReference = "I2", StyleIndex = (UInt32Value)30U, DataType = CellValues.SharedString };
            CellValue cellValue4 = new CellValue();
            cellValue4.Text = "3";

            cell53.Append(cellValue4);
            Cell cell54 = new Cell() { CellReference = "J2", StyleIndex = (UInt32Value)30U };
            Cell cell55 = new Cell() { CellReference = "K2", StyleIndex = (UInt32Value)30U };

            Cell cell56 = new Cell() { CellReference = "L2", StyleIndex = (UInt32Value)31U, DataType = CellValues.SharedString };
            CellValue cellValue5 = new CellValue();
            cellValue5.Text = "4";

            cell56.Append(cellValue5);
            Cell cell57 = new Cell() { CellReference = "M2", StyleIndex = (UInt32Value)31U };
            Cell cell58 = new Cell() { CellReference = "N2", StyleIndex = (UInt32Value)31U };
            Cell cell59 = new Cell() { CellReference = "O2", StyleIndex = (UInt32Value)31U };
            Cell cell60 = new Cell() { CellReference = "P2", StyleIndex = (UInt32Value)31U };
            Cell cell61 = new Cell() { CellReference = "Q2", StyleIndex = (UInt32Value)31U };
            Cell cell62 = new Cell() { CellReference = "R2", StyleIndex = (UInt32Value)31U };
            Cell cell63 = new Cell() { CellReference = "S2", StyleIndex = (UInt32Value)3U };

            Cell cell64 = new Cell() { CellReference = "T2", StyleIndex = (UInt32Value)30U, DataType = CellValues.SharedString };
            CellValue cellValue6 = new CellValue();
            cellValue6.Text = "5";

            cell64.Append(cellValue6);
            Cell cell65 = new Cell() { CellReference = "U2", StyleIndex = (UInt32Value)30U };
            Cell cell66 = new Cell() { CellReference = "V2", StyleIndex = (UInt32Value)30U };

            Cell cell67 = new Cell() { CellReference = "W2", StyleIndex = (UInt32Value)31U, DataType = CellValues.SharedString };
            CellValue cellValue7 = new CellValue();
            cellValue7.Text = "6";

            cell67.Append(cellValue7);
            Cell cell68 = new Cell() { CellReference = "X2", StyleIndex = (UInt32Value)31U };
            Cell cell69 = new Cell() { CellReference = "Y2", StyleIndex = (UInt32Value)31U };
            Cell cell70 = new Cell() { CellReference = "Z2", StyleIndex = (UInt32Value)3U };

            Cell cell71 = new Cell() { CellReference = "AA2", StyleIndex = (UInt32Value)32U, DataType = CellValues.SharedString };
            CellValue cellValue8 = new CellValue();
            cellValue8.Text = "7";

            cell71.Append(cellValue8);
            Cell cell72 = new Cell() { CellReference = "AB2", StyleIndex = (UInt32Value)32U };
            Cell cell73 = new Cell() { CellReference = "AC2", StyleIndex = (UInt32Value)32U };
            Cell cell74 = new Cell() { CellReference = "AD2", StyleIndex = (UInt32Value)32U };

            Cell cell75 = new Cell() { CellReference = "AE2", StyleIndex = (UInt32Value)31U, DataType = CellValues.SharedString };
            CellValue cellValue9 = new CellValue();
            cellValue9.Text = "23";

            cell75.Append(cellValue9);
            Cell cell76 = new Cell() { CellReference = "AF2", StyleIndex = (UInt32Value)31U };
            Cell cell77 = new Cell() { CellReference = "AG2", StyleIndex = (UInt32Value)31U };
            Cell cell78 = new Cell() { CellReference = "AH2", StyleIndex = (UInt32Value)4U };

            Cell cell79 = new Cell() { CellReference = "AI2", StyleIndex = (UInt32Value)33U, DataType = CellValues.SharedString };
            CellValue cellValue10 = new CellValue();
            cellValue10.Text = "8";

            cell79.Append(cellValue10);
            Cell cell80 = new Cell() { CellReference = "AJ2", StyleIndex = (UInt32Value)33U };

            Cell cell81 = new Cell() { CellReference = "AK2", StyleIndex = (UInt32Value)31U };
            CellValue cellValue11 = new CellValue();
            cellValue11.Text = level;

            cell81.Append(cellValue11);
            Cell cell82 = new Cell() { CellReference = "AL2", StyleIndex = (UInt32Value)31U };
            Cell cell83 = new Cell() { CellReference = "AM2", StyleIndex = (UInt32Value)5U };
            Cell cell84 = new Cell() { CellReference = "AN2", StyleIndex = (UInt32Value)5U };
            Cell cell85 = new Cell() { CellReference = "AO2", StyleIndex = (UInt32Value)6U };
            Cell cell86 = new Cell() { CellReference = "AP2", StyleIndex = (UInt32Value)7U };
            Cell cell87 = new Cell() { CellReference = "AQ2", StyleIndex = (UInt32Value)7U };
            Cell cell88 = new Cell() { CellReference = "AR2", StyleIndex = (UInt32Value)8U };

            row2.Append(cell45);
            row2.Append(cell46);
            row2.Append(cell47);
            row2.Append(cell48);
            row2.Append(cell49);
            row2.Append(cell50);
            row2.Append(cell51);
            row2.Append(cell52);
            row2.Append(cell53);
            row2.Append(cell54);
            row2.Append(cell55);
            row2.Append(cell56);
            row2.Append(cell57);
            row2.Append(cell58);
            row2.Append(cell59);
            row2.Append(cell60);
            row2.Append(cell61);
            row2.Append(cell62);
            row2.Append(cell63);
            row2.Append(cell64);
            row2.Append(cell65);
            row2.Append(cell66);
            row2.Append(cell67);
            row2.Append(cell68);
            row2.Append(cell69);
            row2.Append(cell70);
            row2.Append(cell71);
            row2.Append(cell72);
            row2.Append(cell73);
            row2.Append(cell74);
            row2.Append(cell75);
            row2.Append(cell76);
            row2.Append(cell77);
            row2.Append(cell78);
            row2.Append(cell79);
            row2.Append(cell80);
            row2.Append(cell81);
            row2.Append(cell82);
            row2.Append(cell83);
            row2.Append(cell84);
            row2.Append(cell85);
            row2.Append(cell86);
            row2.Append(cell87);
            row2.Append(cell88);

            Row row3 = new Row() { RowIndex = (UInt32Value)3U, Spans = new ListValue<StringValue>() { InnerText = "1:44" }, Height = 18.75D, CustomHeight = true, DyDescent = 0.3D };
            Cell cell89 = new Cell() { CellReference = "A3", StyleIndex = (UInt32Value)9U };

            Cell cell90 = new Cell() { CellReference = "B3", StyleIndex = (UInt32Value)10U, DataType = CellValues.SharedString };
            CellValue cellValue12 = new CellValue();
            cellValue12.Text = "9";

            cell90.Append(cellValue12);

            Cell cell91 = new Cell() { CellReference = "C3", StyleIndex = (UInt32Value)20U, DataType = CellValues.SharedString };
            CellValue cellValue13 = new CellValue();
            cellValue13.Text = "10";

            cell91.Append(cellValue13);

            Cell cell92 = new Cell() { CellReference = "D3", StyleIndex = (UInt32Value)25U };
            CellFormula cellFormula1 = new CellFormula();
            cellFormula1.Text = "COUNTIF(D5:AL189,\"<40\")";
            cellFormula1.CalculateCell = true;
            CellValue cellValue14 = new CellValue();
            cellValue14.Text = "0";

            cell92.Append(cellFormula1);
            cell92.Append(cellValue14);
            Cell cell93 = new Cell() { CellReference = "E3", StyleIndex = (UInt32Value)25U };

            Cell cell94 = new Cell() { CellReference = "F3", StyleIndex = (UInt32Value)26U, DataType = CellValues.SharedString };
            CellValue cellValue15 = new CellValue();
            cellValue15.Text = "11";

            cell94.Append(cellValue15);
            Cell cell95 = new Cell() { CellReference = "G3", StyleIndex = (UInt32Value)26U };
            Cell cell96 = new Cell() { CellReference = "H3", StyleIndex = (UInt32Value)26U };
            Cell cell97 = new Cell() { CellReference = "I3", StyleIndex = (UInt32Value)26U };

            Cell cell98 = new Cell() { CellReference = "J3", StyleIndex = (UInt32Value)25U };
            CellFormula cellFormula2 = new CellFormula();
            cellFormula2.Text = "COUNTIFS(D5:AL189,\">39\",D5:AL189,\"<45\")";
            cellFormula2.CalculateCell = true;
            CellValue cellValue16 = new CellValue();
            cellValue16.Text = "0";

            cell98.Append(cellFormula2);
            cell98.Append(cellValue16);
            Cell cell99 = new Cell() { CellReference = "K3", StyleIndex = (UInt32Value)25U };
            Cell cell100 = new Cell() { CellReference = "L3", StyleIndex = (UInt32Value)22U };

            Cell cell101 = new Cell() { CellReference = "M3", StyleIndex = (UInt32Value)26U, DataType = CellValues.SharedString };
            CellValue cellValue17 = new CellValue();
            cellValue17.Text = "12";

            cell101.Append(cellValue17);
            Cell cell102 = new Cell() { CellReference = "N3", StyleIndex = (UInt32Value)26U };
            Cell cell103 = new Cell() { CellReference = "O3", StyleIndex = (UInt32Value)26U };

            Cell cell104 = new Cell() { CellReference = "P3", StyleIndex = (UInt32Value)25U };
            CellFormula cellFormula3 = new CellFormula();
            cellFormula3.Text = "COUNTIFS(D5:AL189,\">44\",D5:AL189,\"<50\")";
            cellFormula3.CalculateCell = true;
            CellValue cellValue18 = new CellValue();
            cellValue18.Text = "0";

            cell104.Append(cellFormula3);
            cell104.Append(cellValue18);
            Cell cell105 = new Cell() { CellReference = "Q3", StyleIndex = (UInt32Value)25U };
            Cell cell106 = new Cell() { CellReference = "R3", StyleIndex = (UInt32Value)23U };
            Cell cell107 = new Cell() { CellReference = "S3", StyleIndex = (UInt32Value)22U };

            Cell cell108 = new Cell() { CellReference = "T3", StyleIndex = (UInt32Value)26U, DataType = CellValues.SharedString };
            CellValue cellValue19 = new CellValue();
            cellValue19.Text = "13";

            cell108.Append(cellValue19);
            Cell cell109 = new Cell() { CellReference = "U3", StyleIndex = (UInt32Value)26U };
            Cell cell110 = new Cell() { CellReference = "V3", StyleIndex = (UInt32Value)26U };
            Cell cell111 = new Cell() { CellReference = "W3", StyleIndex = (UInt32Value)26U };

            Cell cell112 = new Cell() { CellReference = "X3", StyleIndex = (UInt32Value)25U };
            CellFormula cellFormula4 = new CellFormula();
            cellFormula4.Text = "COUNTIFS(D5:AL189,\">49\",D5:AL189,\"<60\")";
            cellFormula4.CalculateCell = true;
            CellValue cellValue20 = new CellValue();
            cellValue20.Text = "0";

            cell112.Append(cellFormula4);
            cell112.Append(cellValue20);
            Cell cell113 = new Cell() { CellReference = "Y3", StyleIndex = (UInt32Value)25U };

            Cell cell114 = new Cell() { CellReference = "Z3", StyleIndex = (UInt32Value)26U, DataType = CellValues.SharedString };
            CellValue cellValue21 = new CellValue();
            cellValue21.Text = "14";

            cell114.Append(cellValue21);
            Cell cell115 = new Cell() { CellReference = "AA3", StyleIndex = (UInt32Value)26U };
            Cell cell116 = new Cell() { CellReference = "AB3", StyleIndex = (UInt32Value)26U };
            Cell cell117 = new Cell() { CellReference = "AC3", StyleIndex = (UInt32Value)26U };

            Cell cell118 = new Cell() { CellReference = "AD3", StyleIndex = (UInt32Value)25U };
            CellFormula cellFormula5 = new CellFormula();
            cellFormula5.Text = "COUNTIFS(D5:AL189,\">59\",D5:AL189,\"<70\")";
            cellFormula5.CalculateCell = true;
            CellValue cellValue22 = new CellValue();
            cellValue22.Text = "0";

            cell118.Append(cellFormula5);
            cell118.Append(cellValue22);
            Cell cell119 = new Cell() { CellReference = "AE3", StyleIndex = (UInt32Value)25U };
            Cell cell120 = new Cell() { CellReference = "AF3", StyleIndex = (UInt32Value)24U };

            Cell cell121 = new Cell() { CellReference = "AG3", StyleIndex = (UInt32Value)27U, DataType = CellValues.SharedString };
            CellValue cellValue23 = new CellValue();
            cellValue23.Text = "15";

            cell121.Append(cellValue23);
            Cell cell122 = new Cell() { CellReference = "AH3", StyleIndex = (UInt32Value)27U };
            Cell cell123 = new Cell() { CellReference = "AI3", StyleIndex = (UInt32Value)27U };
            Cell cell124 = new Cell() { CellReference = "AJ3", StyleIndex = (UInt32Value)27U };

            Cell cell125 = new Cell() { CellReference = "AK3", StyleIndex = (UInt32Value)25U };
            CellFormula cellFormula6 = new CellFormula();
            cellFormula6.Text = "COUNTIF(D5:AL189,\">69\")";
            cellFormula6.CalculateCell = true;
            CellValue cellValue24 = new CellValue();
            cellValue24.Text = "0";

            cell125.Append(cellFormula6);
            cell125.Append(cellValue24);
            Cell cell126 = new Cell() { CellReference = "AL3", StyleIndex = (UInt32Value)25U };
            Cell cell127 = new Cell() { CellReference = "AO3", StyleIndex = (UInt32Value)11U };
            Cell cell128 = new Cell() { CellReference = "AP3", StyleIndex = (UInt32Value)12U };
            Cell cell129 = new Cell() { CellReference = "AQ3", StyleIndex = (UInt32Value)12U };
            Cell cell130 = new Cell() { CellReference = "AR3", StyleIndex = (UInt32Value)13U };

            row3.Append(cell89);
            row3.Append(cell90);
            row3.Append(cell91);
            row3.Append(cell92);
            row3.Append(cell93);
            row3.Append(cell94);
            row3.Append(cell95);
            row3.Append(cell96);
            row3.Append(cell97);
            row3.Append(cell98);
            row3.Append(cell99);
            row3.Append(cell100);
            row3.Append(cell101);
            row3.Append(cell102);
            row3.Append(cell103);
            row3.Append(cell104);
            row3.Append(cell105);
            row3.Append(cell106);
            row3.Append(cell107);
            row3.Append(cell108);
            row3.Append(cell109);
            row3.Append(cell110);
            row3.Append(cell111);
            row3.Append(cell112);
            row3.Append(cell113);
            row3.Append(cell114);
            row3.Append(cell115);
            row3.Append(cell116);
            row3.Append(cell117);
            row3.Append(cell118);
            row3.Append(cell119);
            row3.Append(cell120);
            row3.Append(cell121);
            row3.Append(cell122);
            row3.Append(cell123);
            row3.Append(cell124);
            row3.Append(cell125);
            row3.Append(cell126);
            row3.Append(cell127);
            row3.Append(cell128);
            row3.Append(cell129);
            row3.Append(cell130);

            Row row4 = new Row() { RowIndex = (UInt32Value)4U, Spans = new ListValue<StringValue>() { InnerText = "1:44" }, Height = 54D, CustomHeight = true, DyDescent = 0.2D };

            Cell cell131 = new Cell() { CellReference = "A4", StyleIndex = (UInt32Value)14U, DataType = CellValues.SharedString };
            CellValue cellValue25 = new CellValue();
            cellValue25.Text = "16";

            cell131.Append(cellValue25);

            Cell cell132 = new Cell() { CellReference = "B4", StyleIndex = (UInt32Value)14U, DataType = CellValues.SharedString };
            CellValue cellValue26 = new CellValue();
            cellValue26.Text = "17";

            cell132.Append(cellValue26);

            Cell cell133 = new Cell() { CellReference = "C4", StyleIndex = (UInt32Value)14U, DataType = CellValues.SharedString };
            CellValue cellValue27 = new CellValue();
            cellValue27.Text = "18";

            cell133.Append(cellValue27);
            Cell cell134 = new Cell() { CellReference = "D4", StyleIndex = (UInt32Value)17U };
            Cell cell135 = new Cell() { CellReference = "E4", StyleIndex = (UInt32Value)18U };
            Cell cell136 = new Cell() { CellReference = "F4", StyleIndex = (UInt32Value)18U };
            Cell cell137 = new Cell() { CellReference = "G4", StyleIndex = (UInt32Value)18U };
            Cell cell138 = new Cell() { CellReference = "H4", StyleIndex = (UInt32Value)18U };
            Cell cell139 = new Cell() { CellReference = "I4", StyleIndex = (UInt32Value)18U };
            Cell cell140 = new Cell() { CellReference = "J4", StyleIndex = (UInt32Value)18U };
            Cell cell141 = new Cell() { CellReference = "K4", StyleIndex = (UInt32Value)18U };
            Cell cell142 = new Cell() { CellReference = "L4", StyleIndex = (UInt32Value)18U };
            Cell cell143 = new Cell() { CellReference = "M4", StyleIndex = (UInt32Value)18U };
            Cell cell144 = new Cell() { CellReference = "N4", StyleIndex = (UInt32Value)18U };
            Cell cell145 = new Cell() { CellReference = "O4", StyleIndex = (UInt32Value)18U };
            Cell cell146 = new Cell() { CellReference = "P4", StyleIndex = (UInt32Value)18U };
            Cell cell147 = new Cell() { CellReference = "Q4", StyleIndex = (UInt32Value)18U };
            Cell cell148 = new Cell() { CellReference = "R4", StyleIndex = (UInt32Value)18U };
            Cell cell149 = new Cell() { CellReference = "S4", StyleIndex = (UInt32Value)18U };
            Cell cell150 = new Cell() { CellReference = "T4", StyleIndex = (UInt32Value)18U };
            Cell cell151 = new Cell() { CellReference = "U4", StyleIndex = (UInt32Value)18U };
            Cell cell152 = new Cell() { CellReference = "V4", StyleIndex = (UInt32Value)18U };
            Cell cell153 = new Cell() { CellReference = "W4", StyleIndex = (UInt32Value)18U };
            Cell cell154 = new Cell() { CellReference = "X4", StyleIndex = (UInt32Value)18U };
            Cell cell155 = new Cell() { CellReference = "Y4", StyleIndex = (UInt32Value)18U };
            Cell cell156 = new Cell() { CellReference = "Z4", StyleIndex = (UInt32Value)18U };
            Cell cell157 = new Cell() { CellReference = "AA4", StyleIndex = (UInt32Value)18U };
            Cell cell158 = new Cell() { CellReference = "AB4", StyleIndex = (UInt32Value)18U };
            Cell cell159 = new Cell() { CellReference = "AC4", StyleIndex = (UInt32Value)18U };
            Cell cell160 = new Cell() { CellReference = "AD4", StyleIndex = (UInt32Value)18U };
            Cell cell161 = new Cell() { CellReference = "AE4", StyleIndex = (UInt32Value)18U };
            Cell cell162 = new Cell() { CellReference = "AF4", StyleIndex = (UInt32Value)18U };
            Cell cell163 = new Cell() { CellReference = "AG4", StyleIndex = (UInt32Value)18U };
            Cell cell164 = new Cell() { CellReference = "AH4", StyleIndex = (UInt32Value)18U };
            Cell cell165 = new Cell() { CellReference = "AI4", StyleIndex = (UInt32Value)18U };
            Cell cell166 = new Cell() { CellReference = "AJ4", StyleIndex = (UInt32Value)18U };
            Cell cell167 = new Cell() { CellReference = "AK4", StyleIndex = (UInt32Value)18U };
            Cell cell168 = new Cell() { CellReference = "AL4", StyleIndex = (UInt32Value)18U };
            Cell cell169 = new Cell() { CellReference = "AM4", StyleIndex = (UInt32Value)18U };
            Cell cell170 = new Cell() { CellReference = "AN4", StyleIndex = (UInt32Value)18U };

            Cell cell171 = new Cell() { CellReference = "AO4", StyleIndex = (UInt32Value)19U, DataType = CellValues.SharedString };
            CellValue cellValue28 = new CellValue();
            cellValue28.Text = "19";

            cell171.Append(cellValue28);

            Cell cell172 = new Cell() { CellReference = "AP4", StyleIndex = (UInt32Value)15U, DataType = CellValues.SharedString };
            CellValue cellValue29 = new CellValue();
            cellValue29.Text = "20";

            cell172.Append(cellValue29);

            Cell cell173 = new Cell() { CellReference = "AQ4", StyleIndex = (UInt32Value)15U, DataType = CellValues.SharedString };
            CellValue cellValue30 = new CellValue();
            cellValue30.Text = "21";

            cell173.Append(cellValue30);

            Cell cell174 = new Cell() { CellReference = "AR4", StyleIndex = (UInt32Value)16U, DataType = CellValues.SharedString };
            CellValue cellValue31 = new CellValue();
            cellValue31.Text = "22";

            cell174.Append(cellValue31);

            row4.Append(cell131);
            row4.Append(cell132);
            row4.Append(cell133);
            row4.Append(cell134);
            row4.Append(cell135);
            row4.Append(cell136);
            row4.Append(cell137);
            row4.Append(cell138);
            row4.Append(cell139);
            row4.Append(cell140);
            row4.Append(cell141);
            row4.Append(cell142);
            row4.Append(cell143);
            row4.Append(cell144);
            row4.Append(cell145);
            row4.Append(cell146);
            row4.Append(cell147);
            row4.Append(cell148);
            row4.Append(cell149);
            row4.Append(cell150);
            row4.Append(cell151);
            row4.Append(cell152);
            row4.Append(cell153);
            row4.Append(cell154);
            row4.Append(cell155);
            row4.Append(cell156);
            row4.Append(cell157);
            row4.Append(cell158);
            row4.Append(cell159);
            row4.Append(cell160);
            row4.Append(cell161);
            row4.Append(cell162);
            row4.Append(cell163);
            row4.Append(cell164);
            row4.Append(cell165);
            row4.Append(cell166);
            row4.Append(cell167);
            row4.Append(cell168);
            row4.Append(cell169);
            row4.Append(cell170);
            row4.Append(cell171);
            row4.Append(cell172);
            row4.Append(cell173);
            row4.Append(cell174);

            Row row5 = new Row() { RowIndex = (UInt32Value)5U, Spans = new ListValue<StringValue>() { InnerText = "1:44" }, DyDescent = 0.2D };
            Cell cell175 = new Cell() { CellReference = "A5", StyleIndex = (UInt32Value)34U };

            row5.Append(cell175);

            sheetData1.Append(row1);
            sheetData1.Append(row2);
            sheetData1.Append(row3);
            sheetData1.Append(row4);
            sheetData1.Append(row5);

            MergeCells mergeCells1 = new MergeCells() { Count = (UInt32Value)21U };
            MergeCell mergeCell1 = new MergeCell() { Reference = "AG3:AJ3" };
            MergeCell mergeCell2 = new MergeCell() { Reference = "AK3:AL3" };
            MergeCell mergeCell3 = new MergeCell() { Reference = "AD3:AE3" };
            MergeCell mergeCell4 = new MergeCell() { Reference = "A1:AR1" };
            MergeCell mergeCell5 = new MergeCell() { Reference = "C2:G2" };
            MergeCell mergeCell6 = new MergeCell() { Reference = "I2:K2" };
            MergeCell mergeCell7 = new MergeCell() { Reference = "L2:R2" };
            MergeCell mergeCell8 = new MergeCell() { Reference = "T2:V2" };
            MergeCell mergeCell9 = new MergeCell() { Reference = "W2:Y2" };
            MergeCell mergeCell10 = new MergeCell() { Reference = "AA2:AD2" };
            MergeCell mergeCell11 = new MergeCell() { Reference = "AE2:AG2" };
            MergeCell mergeCell12 = new MergeCell() { Reference = "AI2:AJ2" };
            MergeCell mergeCell13 = new MergeCell() { Reference = "AK2:AL2" };
            MergeCell mergeCell14 = new MergeCell() { Reference = "D3:E3" };
            MergeCell mergeCell15 = new MergeCell() { Reference = "F3:I3" };
            MergeCell mergeCell16 = new MergeCell() { Reference = "J3:K3" };
            MergeCell mergeCell17 = new MergeCell() { Reference = "P3:Q3" };
            MergeCell mergeCell18 = new MergeCell() { Reference = "M3:O3" };
            MergeCell mergeCell19 = new MergeCell() { Reference = "X3:Y3" };
            MergeCell mergeCell20 = new MergeCell() { Reference = "T3:W3" };
            MergeCell mergeCell21 = new MergeCell() { Reference = "Z3:AC3" };

            mergeCells1.Append(mergeCell1);
            mergeCells1.Append(mergeCell2);
            mergeCells1.Append(mergeCell3);
            mergeCells1.Append(mergeCell4);
            mergeCells1.Append(mergeCell5);
            mergeCells1.Append(mergeCell6);
            mergeCells1.Append(mergeCell7);
            mergeCells1.Append(mergeCell8);
            mergeCells1.Append(mergeCell9);
            mergeCells1.Append(mergeCell10);
            mergeCells1.Append(mergeCell11);
            mergeCells1.Append(mergeCell12);
            mergeCells1.Append(mergeCell13);
            mergeCells1.Append(mergeCell14);
            mergeCells1.Append(mergeCell15);
            mergeCells1.Append(mergeCell16);
            mergeCells1.Append(mergeCell17);
            mergeCells1.Append(mergeCell18);
            mergeCells1.Append(mergeCell19);
            mergeCells1.Append(mergeCell20);
            mergeCells1.Append(mergeCell21);

            ConditionalFormatting conditionalFormatting1 = new ConditionalFormatting() { SequenceOfReferences = new ListValue<StringValue>() { InnerText = "C2:C4" } };
            ConditionalFormattingRule conditionalFormattingRule1 = new ConditionalFormattingRule() { Type = ConditionalFormatValues.DuplicateValues, FormatId = (UInt32Value)0U, Priority = 8, StopIfTrue = true };

            conditionalFormatting1.Append(conditionalFormattingRule1);
            PrintOptions printOptions1 = new PrintOptions() { GridLines = true };
            PageMargins pageMargins1 = new PageMargins() { Left = 0.25D, Right = 0.25D, Top = 0.25D, Bottom = 0.5D, Header = 0.3D, Footer = 0.2D };
            PageSetup pageSetup1 = new PageSetup() { Scale = (UInt32Value)81U, FitToHeight = (UInt32Value)0U, Orientation = OrientationValues.Landscape, HorizontalDpi = (UInt32Value)300U, VerticalDpi = (UInt32Value)300U, Id = "rId1" };

            HeaderFooter headerFooter1 = new HeaderFooter();
            OddFooter oddFooter1 = new OddFooter();
            oddFooter1.Text = "&CH.O.D Sign/Date____________________________ Exam Officer Sign./Date__________________________Dean Sign/Date____________________________";

            headerFooter1.Append(oddFooter1);
            Drawing drawing1 = new Drawing() { Id = "rId2" };

            worksheet1.Append(sheetProperties1);
            worksheet1.Append(sheetDimension1);
            worksheet1.Append(sheetViews1);
            worksheet1.Append(sheetFormatProperties1);
            worksheet1.Append(columns1);
            worksheet1.Append(sheetData1);
            worksheet1.Append(mergeCells1);
            worksheet1.Append(conditionalFormatting1);
            worksheet1.Append(printOptions1);
            worksheet1.Append(pageMargins1);
            worksheet1.Append(pageSetup1);
            worksheet1.Append(headerFooter1);
            worksheet1.Append(drawing1);

            worksheetPart1.Worksheet = worksheet1;
        }

        // Generates content of drawingsPart1.
        private void GenerateDrawingsPart1Content(DrawingsPart drawingsPart1)
        {
            Xdr.WorksheetDrawing worksheetDrawing1 = new Xdr.WorksheetDrawing();
            worksheetDrawing1.AddNamespaceDeclaration("xdr", "http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing");
            worksheetDrawing1.AddNamespaceDeclaration("a", "http://schemas.openxmlformats.org/drawingml/2006/main");

            Xdr.TwoCellAnchor twoCellAnchor1 = new Xdr.TwoCellAnchor();

            Xdr.FromMarker fromMarker1 = new Xdr.FromMarker();
            Xdr.ColumnId columnId1 = new Xdr.ColumnId();
            columnId1.Text = "0";
            Xdr.ColumnOffset columnOffset1 = new Xdr.ColumnOffset();
            columnOffset1.Text = "0";
            Xdr.RowId rowId1 = new Xdr.RowId();
            rowId1.Text = "3";
            Xdr.RowOffset rowOffset1 = new Xdr.RowOffset();
            rowOffset1.Text = "0";

            fromMarker1.Append(columnId1);
            fromMarker1.Append(columnOffset1);
            fromMarker1.Append(rowId1);
            fromMarker1.Append(rowOffset1);

            Xdr.ToMarker toMarker1 = new Xdr.ToMarker();
            Xdr.ColumnId columnId2 = new Xdr.ColumnId();
            columnId2.Text = "43";
            Xdr.ColumnOffset columnOffset2 = new Xdr.ColumnOffset();
            columnOffset2.Text = "342900";
            Xdr.RowId rowId2 = new Xdr.RowId();
            rowId2.Text = "3";
            Xdr.RowOffset rowOffset2 = new Xdr.RowOffset();
            rowOffset2.Text = "9525";

            toMarker1.Append(columnId2);
            toMarker1.Append(columnOffset2);
            toMarker1.Append(rowId2);
            toMarker1.Append(rowOffset2);

            Xdr.ConnectionShape connectionShape1 = new Xdr.ConnectionShape() { Macro = "" };

            Xdr.NonVisualConnectionShapeProperties nonVisualConnectionShapeProperties1 = new Xdr.NonVisualConnectionShapeProperties();
            Xdr.NonVisualDrawingProperties nonVisualDrawingProperties1 = new Xdr.NonVisualDrawingProperties() { Id = (UInt32Value)4U, Name = "Straight Connector 3" };
            Xdr.NonVisualConnectorShapeDrawingProperties nonVisualConnectorShapeDrawingProperties1 = new Xdr.NonVisualConnectorShapeDrawingProperties();

            nonVisualConnectionShapeProperties1.Append(nonVisualDrawingProperties1);
            nonVisualConnectionShapeProperties1.Append(nonVisualConnectorShapeDrawingProperties1);

            Xdr.ShapeProperties shapeProperties1 = new Xdr.ShapeProperties();

            A.Transform2D transform2D1 = new A.Transform2D() { VerticalFlip = true };
            A.Offset offset1 = new A.Offset() { X = 0L, Y = 866775L };
            A.Extents extents1 = new A.Extents() { Cx = 10972800L, Cy = 9525L };

            transform2D1.Append(offset1);
            transform2D1.Append(extents1);

            A.PresetGeometry presetGeometry1 = new A.PresetGeometry() { Preset = A.ShapeTypeValues.Line };
            A.AdjustValueList adjustValueList1 = new A.AdjustValueList();

            presetGeometry1.Append(adjustValueList1);

            A.Outline outline4 = new A.Outline() { Width = 12700 };

            A.SolidFill solidFill7 = new A.SolidFill();
            A.SchemeColor schemeColor16 = new A.SchemeColor() { Val = A.SchemeColorValues.Text1 };

            solidFill7.Append(schemeColor16);

            outline4.Append(solidFill7);

            shapeProperties1.Append(transform2D1);
            shapeProperties1.Append(presetGeometry1);
            shapeProperties1.Append(outline4);

            Xdr.ShapeStyle shapeStyle1 = new Xdr.ShapeStyle();

            A.LineReference lineReference1 = new A.LineReference() { Index = (UInt32Value)1U };
            A.SchemeColor schemeColor17 = new A.SchemeColor() { Val = A.SchemeColorValues.Accent1 };

            lineReference1.Append(schemeColor17);

            A.FillReference fillReference1 = new A.FillReference() { Index = (UInt32Value)0U };
            A.SchemeColor schemeColor18 = new A.SchemeColor() { Val = A.SchemeColorValues.Accent1 };

            fillReference1.Append(schemeColor18);

            A.EffectReference effectReference1 = new A.EffectReference() { Index = (UInt32Value)0U };
            A.SchemeColor schemeColor19 = new A.SchemeColor() { Val = A.SchemeColorValues.Accent1 };

            effectReference1.Append(schemeColor19);

            A.FontReference fontReference1 = new A.FontReference() { Index = A.FontCollectionIndexValues.Minor };
            A.SchemeColor schemeColor20 = new A.SchemeColor() { Val = A.SchemeColorValues.Text1 };

            fontReference1.Append(schemeColor20);

            shapeStyle1.Append(lineReference1);
            shapeStyle1.Append(fillReference1);
            shapeStyle1.Append(effectReference1);
            shapeStyle1.Append(fontReference1);

            connectionShape1.Append(nonVisualConnectionShapeProperties1);
            connectionShape1.Append(shapeProperties1);
            connectionShape1.Append(shapeStyle1);
            Xdr.ClientData clientData1 = new Xdr.ClientData();

            twoCellAnchor1.Append(fromMarker1);
            twoCellAnchor1.Append(toMarker1);
            twoCellAnchor1.Append(connectionShape1);
            twoCellAnchor1.Append(clientData1);

            Xdr.TwoCellAnchor twoCellAnchor2 = new Xdr.TwoCellAnchor();

            Xdr.FromMarker fromMarker2 = new Xdr.FromMarker();
            Xdr.ColumnId columnId3 = new Xdr.ColumnId();
            columnId3.Text = "0";
            Xdr.ColumnOffset columnOffset3 = new Xdr.ColumnOffset();
            columnOffset3.Text = "0";
            Xdr.RowId rowId3 = new Xdr.RowId();
            rowId3.Text = "2";
            Xdr.RowOffset rowOffset3 = new Xdr.RowOffset();
            rowOffset3.Text = "0";

            fromMarker2.Append(columnId3);
            fromMarker2.Append(columnOffset3);
            fromMarker2.Append(rowId3);
            fromMarker2.Append(rowOffset3);

            Xdr.ToMarker toMarker2 = new Xdr.ToMarker();
            Xdr.ColumnId columnId4 = new Xdr.ColumnId();
            columnId4.Text = "43";
            Xdr.ColumnOffset columnOffset4 = new Xdr.ColumnOffset();
            columnOffset4.Text = "342900";
            Xdr.RowId rowId4 = new Xdr.RowId();
            rowId4.Text = "2";
            Xdr.RowOffset rowOffset4 = new Xdr.RowOffset();
            rowOffset4.Text = "9525";

            toMarker2.Append(columnId4);
            toMarker2.Append(columnOffset4);
            toMarker2.Append(rowId4);
            toMarker2.Append(rowOffset4);

            Xdr.ConnectionShape connectionShape2 = new Xdr.ConnectionShape() { Macro = "" };

            Xdr.NonVisualConnectionShapeProperties nonVisualConnectionShapeProperties2 = new Xdr.NonVisualConnectionShapeProperties();
            Xdr.NonVisualDrawingProperties nonVisualDrawingProperties2 = new Xdr.NonVisualDrawingProperties() { Id = (UInt32Value)5U, Name = "Straight Connector 4" };
            Xdr.NonVisualConnectorShapeDrawingProperties nonVisualConnectorShapeDrawingProperties2 = new Xdr.NonVisualConnectorShapeDrawingProperties();

            nonVisualConnectionShapeProperties2.Append(nonVisualDrawingProperties2);
            nonVisualConnectionShapeProperties2.Append(nonVisualConnectorShapeDrawingProperties2);

            Xdr.ShapeProperties shapeProperties2 = new Xdr.ShapeProperties();

            A.Transform2D transform2D2 = new A.Transform2D() { VerticalFlip = true };
            A.Offset offset2 = new A.Offset() { X = 0L, Y = 609600L };
            A.Extents extents2 = new A.Extents() { Cx = 10972800L, Cy = 9525L };

            transform2D2.Append(offset2);
            transform2D2.Append(extents2);

            A.PresetGeometry presetGeometry2 = new A.PresetGeometry() { Preset = A.ShapeTypeValues.Line };
            A.AdjustValueList adjustValueList2 = new A.AdjustValueList();

            presetGeometry2.Append(adjustValueList2);

            A.Outline outline5 = new A.Outline() { Width = 12700 };

            A.SolidFill solidFill8 = new A.SolidFill();
            A.SchemeColor schemeColor21 = new A.SchemeColor() { Val = A.SchemeColorValues.Text1 };

            solidFill8.Append(schemeColor21);

            outline5.Append(solidFill8);

            shapeProperties2.Append(transform2D2);
            shapeProperties2.Append(presetGeometry2);
            shapeProperties2.Append(outline5);

            Xdr.ShapeStyle shapeStyle2 = new Xdr.ShapeStyle();

            A.LineReference lineReference2 = new A.LineReference() { Index = (UInt32Value)1U };
            A.SchemeColor schemeColor22 = new A.SchemeColor() { Val = A.SchemeColorValues.Accent1 };

            lineReference2.Append(schemeColor22);

            A.FillReference fillReference2 = new A.FillReference() { Index = (UInt32Value)0U };
            A.SchemeColor schemeColor23 = new A.SchemeColor() { Val = A.SchemeColorValues.Accent1 };

            fillReference2.Append(schemeColor23);

            A.EffectReference effectReference2 = new A.EffectReference() { Index = (UInt32Value)0U };
            A.SchemeColor schemeColor24 = new A.SchemeColor() { Val = A.SchemeColorValues.Accent1 };

            effectReference2.Append(schemeColor24);

            A.FontReference fontReference2 = new A.FontReference() { Index = A.FontCollectionIndexValues.Minor };
            A.SchemeColor schemeColor25 = new A.SchemeColor() { Val = A.SchemeColorValues.Text1 };

            fontReference2.Append(schemeColor25);

            shapeStyle2.Append(lineReference2);
            shapeStyle2.Append(fillReference2);
            shapeStyle2.Append(effectReference2);
            shapeStyle2.Append(fontReference2);

            connectionShape2.Append(nonVisualConnectionShapeProperties2);
            connectionShape2.Append(shapeProperties2);
            connectionShape2.Append(shapeStyle2);
            Xdr.ClientData clientData2 = new Xdr.ClientData();

            twoCellAnchor2.Append(fromMarker2);
            twoCellAnchor2.Append(toMarker2);
            twoCellAnchor2.Append(connectionShape2);
            twoCellAnchor2.Append(clientData2);

            worksheetDrawing1.Append(twoCellAnchor1);
            worksheetDrawing1.Append(twoCellAnchor2);

            drawingsPart1.WorksheetDrawing = worksheetDrawing1;
        }

        // Generates content of spreadsheetPrinterSettingsPart1.
        private void GenerateSpreadsheetPrinterSettingsPart1Content(SpreadsheetPrinterSettingsPart spreadsheetPrinterSettingsPart1)
        {
            System.IO.Stream data = GetBinaryDataStream(spreadsheetPrinterSettingsPart1Data);
            spreadsheetPrinterSettingsPart1.FeedData(data);
            data.Close();
        }

        // Generates content of calculationChainPart1.
        private void GenerateCalculationChainPart1Content(CalculationChainPart calculationChainPart1)
        {
            CalculationChain calculationChain1 = new CalculationChain();
            CalculationCell calculationCell1 = new CalculationCell() { CellReference = "J3", SheetId = 1, NewLevel = true };
            CalculationCell calculationCell2 = new CalculationCell() { CellReference = "P3", SheetId = 1 };
            CalculationCell calculationCell3 = new CalculationCell() { CellReference = "X3", SheetId = 1 };
            CalculationCell calculationCell4 = new CalculationCell() { CellReference = "AD3", SheetId = 1 };
            CalculationCell calculationCell5 = new CalculationCell() { CellReference = "AK3", SheetId = 1 };
            CalculationCell calculationCell6 = new CalculationCell() { CellReference = "D3", SheetId = 1, NewLevel = true };

            calculationChain1.Append(calculationCell1);
            calculationChain1.Append(calculationCell2);
            calculationChain1.Append(calculationCell3);
            calculationChain1.Append(calculationCell4);
            calculationChain1.Append(calculationCell5);
            calculationChain1.Append(calculationCell6);

            calculationChainPart1.CalculationChain = calculationChain1;
        }

        // Generates content of sharedStringTablePart1.
        private void GenerateSharedStringTablePart1Content(SharedStringTablePart sharedStringTablePart1)
        {
            SharedStringTable sharedStringTable1 = new SharedStringTable() { Count = (UInt32Value)24U, UniqueCount = (UInt32Value)24U };

            SharedStringItem sharedStringItem1 = new SharedStringItem();
            Text text1 = new Text();
            text1.Text = "Result Broadsheet (Form C)";

            sharedStringItem1.Append(text1);

            SharedStringItem sharedStringItem2 = new SharedStringItem();
            Text text2 = new Text();
            text2.Text = "Department:";

            sharedStringItem2.Append(text2);

            SharedStringItem sharedStringItem3 = new SharedStringItem();
            Text text3 = new Text();
            text3.Text = department;

            sharedStringItem3.Append(text3);

            SharedStringItem sharedStringItem4 = new SharedStringItem();
            Text text4 = new Text();
            text4.Text = "Faculty:";

            sharedStringItem4.Append(text4);

            SharedStringItem sharedStringItem5 = new SharedStringItem();
            Text text5 = new Text();
            text5.Text = faculty;

            sharedStringItem5.Append(text5);

            SharedStringItem sharedStringItem6 = new SharedStringItem();
            Text text6 = new Text();
            text6.Text = "Session:";

            sharedStringItem6.Append(text6);

            SharedStringItem sharedStringItem7 = new SharedStringItem();
            Text text7 = new Text();
            text7.Text = academicYear;

            sharedStringItem7.Append(text7);

            SharedStringItem sharedStringItem8 = new SharedStringItem();
            Text text8 = new Text();
            text8.Text = "Semester:";

            sharedStringItem8.Append(text8);

            SharedStringItem sharedStringItem9 = new SharedStringItem();
            Text text9 = new Text();
            text9.Text = "Level:";

            sharedStringItem9.Append(text9);

            SharedStringItem sharedStringItem10 = new SharedStringItem();
            Text text10 = new Text();
            text10.Text = "Summary of Grade";

            sharedStringItem10.Append(text10);

            SharedStringItem sharedStringItem11 = new SharedStringItem();
            Text text11 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text11.Text = "0-39 (F) ";

            sharedStringItem11.Append(text11);

            SharedStringItem sharedStringItem12 = new SharedStringItem();
            Text text12 = new Text();
            text12.Text = "40-44 (E)";

            sharedStringItem12.Append(text12);

            SharedStringItem sharedStringItem13 = new SharedStringItem();
            Text text13 = new Text();
            text13.Text = "45-49 (D)";

            sharedStringItem13.Append(text13);

            SharedStringItem sharedStringItem14 = new SharedStringItem();
            Text text14 = new Text();
            text14.Text = "50-59 ( C )";

            sharedStringItem14.Append(text14);

            SharedStringItem sharedStringItem15 = new SharedStringItem();
            Text text15 = new Text();
            text15.Text = "60-69 (B)";

            sharedStringItem15.Append(text15);

            SharedStringItem sharedStringItem16 = new SharedStringItem();
            Text text16 = new Text();
            text16.Text = "70-100 (A)";

            sharedStringItem16.Append(text16);

            SharedStringItem sharedStringItem17 = new SharedStringItem();
            Text text17 = new Text();
            text17.Text = "S/No";

            sharedStringItem17.Append(text17);

            SharedStringItem sharedStringItem18 = new SharedStringItem();
            Text text18 = new Text();
            text18.Text = "NAMES";

            sharedStringItem18.Append(text18);

            SharedStringItem sharedStringItem19 = new SharedStringItem();
            Text text19 = new Text();
            text19.Text = "REGNO";

            sharedStringItem19.Append(text19);

            SharedStringItem sharedStringItem20 = new SharedStringItem();
            Text text20 = new Text();
            text20.Text = "Actual Unit";

            sharedStringItem20.Append(text20);

            SharedStringItem sharedStringItem21 = new SharedStringItem();
            Text text21 = new Text();
            text21.Text = "Total unit";

            sharedStringItem21.Append(text21);

            SharedStringItem sharedStringItem22 = new SharedStringItem();
            Text text22 = new Text();
            text22.Text = "TGP";

            sharedStringItem22.Append(text22);

            SharedStringItem sharedStringItem23 = new SharedStringItem();
            Text text23 = new Text();
            text23.Text = "GPA";

            sharedStringItem23.Append(text23);

            SharedStringItem sharedStringItem24 = new SharedStringItem();
            Text text24 = new Text();
            text24.Text = semester;

            sharedStringItem24.Append(text24);

            sharedStringTable1.Append(sharedStringItem1);
            sharedStringTable1.Append(sharedStringItem2);
            sharedStringTable1.Append(sharedStringItem3);
            sharedStringTable1.Append(sharedStringItem4);
            sharedStringTable1.Append(sharedStringItem5);
            sharedStringTable1.Append(sharedStringItem6);
            sharedStringTable1.Append(sharedStringItem7);
            sharedStringTable1.Append(sharedStringItem8);
            sharedStringTable1.Append(sharedStringItem9);
            sharedStringTable1.Append(sharedStringItem10);
            sharedStringTable1.Append(sharedStringItem11);
            sharedStringTable1.Append(sharedStringItem12);
            sharedStringTable1.Append(sharedStringItem13);
            sharedStringTable1.Append(sharedStringItem14);
            sharedStringTable1.Append(sharedStringItem15);
            sharedStringTable1.Append(sharedStringItem16);
            sharedStringTable1.Append(sharedStringItem17);
            sharedStringTable1.Append(sharedStringItem18);
            sharedStringTable1.Append(sharedStringItem19);
            sharedStringTable1.Append(sharedStringItem20);
            sharedStringTable1.Append(sharedStringItem21);
            sharedStringTable1.Append(sharedStringItem22);
            sharedStringTable1.Append(sharedStringItem23);
            sharedStringTable1.Append(sharedStringItem24);

            sharedStringTablePart1.SharedStringTable = sharedStringTable1;
        }

        private void SetPackageProperties(OpenXmlPackage document)
        {
            document.PackageProperties.Creator = "Xwizard";
            document.PackageProperties.Created = System.Xml.XmlConvert.ToDateTime("2013-06-23T16:25:07Z", System.Xml.XmlDateTimeSerializationMode.RoundtripKind);
            document.PackageProperties.Modified = System.Xml.XmlConvert.ToDateTime("2013-06-24T17:21:52Z", System.Xml.XmlDateTimeSerializationMode.RoundtripKind);
            document.PackageProperties.LastModifiedBy = "Xwizard";
            document.PackageProperties.LastPrinted = System.Xml.XmlConvert.ToDateTime("2013-06-24T11:59:51Z", System.Xml.XmlDateTimeSerializationMode.RoundtripKind);
        }

        #region Binary Data
        private string spreadsheetPrinterSettingsPart1Data = "RgBvAHgAaQB0ACAAUgBlAGEAZABlAHIAIABQAEQARgAgAFAAcgBpAG4AdABlAHIAAAAAAAAAAAAAAAAAAAAAAAEEAQTcAAAAX/+BBwIAAQDqCm8IUQABAAcALAECAAEALAECAAAATABlAHQAdABlAHIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAACAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAAAAAAAAAA==";

        private System.IO.Stream GetBinaryDataStream(string base64String)
        {
            return new System.IO.MemoryStream(System.Convert.FromBase64String(base64String));
        }

        #endregion

    }
}
