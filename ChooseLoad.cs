using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ARMS
{
    public partial class ChooseLoad : Form
    {
        private bool local;
        public ChooseLoad(string[] roles, string user, string secret, bool gender, bool local)
        {
            InitializeComponent();
            this.local = local;
            _usr = user;
            this.secret = secret;
            List<Button> buttons = new List<Button>();
            buttons.AddRange(new Button[] {button1, button2, button3});
            if (roles.Length > 3)
            {
                for (int i = 3; i < roles.Length; i++)
                {
                    RibbonMenuButton rb = new MyButton();
                    rb.ImageList = imageList1;
                    rb.Click += buttonClick;
                    flowLayoutPanel1.Controls.Add(rb);
                    buttons.Add(rb);
                }
            }
            int added = 0;
            for (int i = 0; i < roles.Length; i++)
            {
                if (roles[i] == "")
                    continue;
                buttons[i].Text = roles[i];
                buttons[i].Visible = true;
                added++;
                string imlink = "male";
                if (!gender)
                {
                    imlink = "fe" + imlink;
                }
                switch (roles[i])
                {
                    case "Lecturer":
                        buttons[i].ImageKey = imlink;
                        break;
                    case "HOD":
                        buttons[i].ImageKey = imlink + 2;
                        break;
                    case "Department Admin":
                        buttons[i].ImageKey = imlink + 1;
                        break;
                    case "Bursar":
                        buttons[i].ImageKey = imlink + 1;
                        break;
                    case "Dean":
                        buttons[i].ImageKey = imlink + 2;
                        break;
                    case "Exam Officer":
                        buttons[i].ImageKey = imlink + 1;
                        break;
                    case "MIS":
                        buttons[i].ImageKey = "male3";
                        break;
                }
            }
            if (added == 0)
            {
                MessageBox.Show("You have not been assigned any roles. You must be assigned a role first before you can access any interface.", "No Roles Assigned", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
            }
        }

        private static string _usr;

        private readonly string secret; //contains specific information for specified usertype
        private LecturerDetails lcd;
        private DeptDetails dd;

        private void buttonClick(object sender, EventArgs e)
        {
            var utype = ((Button)sender).Text;
            Hide();
            var lecData = new string[] {};
            try
            {
                switch (utype)
                {
                    case "Lecturer":
                    case "HOD":
                    case "Department Admin":
                    case "Dean":
                    case "Exam Officer":
                        if (local)
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(LecturerDetails));
                            FileStream fs = new FileStream(Application.StartupPath + "\\lecturer.xml", FileMode.Open);
                            lcd = (LecturerDetails)serializer.Deserialize(fs);
                            fs.Close();
                        }
                        else
                        {
                            lecData =
                                Helper.selectResults(new[] { "title", "lastName", "department", "email", "firstName", "lecturers.faculty as faculty", "shortName", "courseDuration", "programmeType" },
                                    "lecturers", new[] {"username"}, new[] {_usr}, "join departments on deptName = department")[0];

                            lcd = new LecturerDetails
                                {
                                    UserName = _usr,
                                    Title = lecData[0],
                                    LastName = lecData[1],
                                    Department = lecData[2],
                                    Email = lecData[3],
                                    FirstName = lecData[4],
                                    Faculty = lecData[5]
                                };
                            XmlSerializer serializer = new XmlSerializer(typeof (LecturerDetails));
                            TextWriter writer = new StreamWriter(Application.StartupPath + "\\lecturer.xml");
                            serializer.Serialize(writer, lcd);
                            writer.Close();
                        }

                        switch (utype)
                        {
                            case "Lecturer":
                                homeLecturer hl = new homeLecturer(lcd, local);
                                try
                                {
                                    hl.ShowDialog();
                                }
                                        catch (Exception ex)
                                        {
                                            Logger.WriteLog(ex);
                                        }
                                finally
                                {
                                    hl.Dispose();
                                }
                                break;
                            case "HOD":
                            case "Department Admin":
                            case "Exam Officer":
                                if (local)
                                {
                                    XmlSerializer serializer = new XmlSerializer(typeof(DeptDetails));
                                    FileStream fs = new FileStream(Application.StartupPath + "\\department.xml", FileMode.Open);
                                    dd = (DeptDetails)serializer.Deserialize(fs);
                                    fs.Close();
                                }
                                else
                                {
                                     dd = new DeptDetails
                                        {
                                            DeptName = lcd.Department,
                                            ShortName = lecData[6],
                                            Faculty = lcd.Faculty,
                                            CourseDuration = lecData[7],
                                            ProgrammeType = lecData[8]
                                        };
                                    XmlSerializer serializer = new XmlSerializer(typeof(DeptDetails));
                                     TextWriter writer = new StreamWriter(Application.StartupPath + "\\department.xml");
                                     serializer.Serialize(writer, dd);
                                     writer.Close();
                                }

                                switch (utype)
                                {
                                    case "HOD":
                                        homeHOD hh = new homeHOD(_usr, lcd.NiceName, dd, local);
                                        try
                                        {
                                            hh.ShowDialog();
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.WriteLog(ex);
                                        }
                                        finally
                                        {
                                            hh.Dispose();
                                        }
                                        break;
                                    case "Department Admin":
                                        var da = new homeDept(dd, lcd.NiceName, _usr, local);
                                        try
                                        {
                                            da.ShowDialog();
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.WriteLog(ex);
                                        }
                                        finally
                                        {
                                            da.Dispose();
                                        }
                                        break;
                                    case "Exam Officer":
                                        ExamOfficer eo = new ExamOfficer(dd, lcd.NiceName, _usr, local);
                                        try
                                        {
                                            eo.ShowDialog();
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.WriteLog(ex);
                                        }
                                        finally
                                        {
                                            eo.Dispose();
                                        }
                                        break;
                                }
                                break;
                            case "Dean":
                                homeDean hd = new homeDean(_usr, lcd.NiceName, lcd.Faculty, local);
                                try
                                {
                                    hd.ShowDialog();
                                }
                                catch (Exception ex)
                                {
                                    Logger.WriteLog(ex);
                                }
                                finally
                                {
                                    hd.Dispose();
                                }
                                break;

                        }
                        break;
                    case "Bursar":
                        if (File.Exists("secret.tgs") && secret != "")
                        {
                            if (File.ReadAllText("secret.tgs") == secret)
                            {
                                homeBursar hb = new homeBursar(_usr);
                                hb.ShowDialog();
                            }
                            else
                            {
                                MessageBox.Show(
                                    "Bursary module login can only be done from original computer where the account was first operated.",
                                    "Cannot Login", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }
                        }
                        else if (secret == "")
                        {
                            Random r = new Random();
                            int i = r.Next();
                            string tuse = Crypto.computeMD5(i.ToString(CultureInfo.InvariantCulture));
                            if (
                                Helper.ExecuteNonQuery(
                                    string.Format("UPDATE users SET secret = '{0}' WHERE  username = '{1}'",
                                                  tuse, _usr)) != -1)
                            {
                                File.WriteAllText("secret.tgs", tuse);
                            }
                            homeBursar hb = new homeBursar(_usr);
                            try
                            {
                                hb.ShowDialog();
                            }
                            finally
                            {
                                hb.Dispose();
                            }
                        }
                        break;
                    case "MIS":
                        if (local && File.ReadAllText("secret.tgs") == secret)
                        {
                            ControlDashboard cd = new ControlDashboard(_usr, local);
                            cd.ShowDialog();
                            return;
                        }
                        if (File.Exists("secret.tgs") && secret != "")
                        {
                            if (File.ReadAllText("secret.tgs") == secret)
                            {
                                ControlDashboard cd = new ControlDashboard(_usr, local);
                                cd.ShowDialog();
                            }
                            else
                            {
                                MessageBox.Show(
                                    "MIS module login can only be done from original computer where the account was first operated.",
                                    "Cannot Login", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }
                        }
                        else if (secret == "")
                        {
                            Random r = new Random();
                            int i = r.Next();
                            string tuse = Crypto.computeMD5(i.ToString(CultureInfo.InvariantCulture));
                            if (
                                Helper.ExecuteNonQuery(
                                    string.Format("UPDATE users SET secret = '{0}' WHERE  username = '{1}'",
                                                  tuse, _usr)) != -1)
                            {
                                File.WriteAllText("secret.tgs", tuse);
                            }
                            ControlDashboard cd = new ControlDashboard(_usr, local);
                            try
                            {
                                cd.ShowDialog();
                            }
                            finally
                            {
                                cd.Dispose();
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Close();
        }
    }

    public class MyButton : RibbonMenuButton
    {
        public MyButton()
        {
            this.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.BackColor = System.Drawing.Color.Transparent;
            this.ColorBase = System.Drawing.Color.FromArgb(((int) (((byte) (192)))), ((int) (((byte) (255)))),((int) (((byte) (192)))));
            this.ColorBaseStroke = System.Drawing.Color.FromArgb(((int) (((byte) (0)))), ((int) (((byte) (64)))),((int) (((byte) (0)))));
            this.ColorOn = System.Drawing.Color.FromArgb(((int) (((byte) (224)))), ((int) (((byte) (224)))),((int) (((byte) (224)))));
            this.ColorOnStroke = System.Drawing.Color.FromArgb(((int) (((byte) (192)))), ((int) (((byte) (192)))),((int) (((byte) (255)))));
            this.ColorPress = System.Drawing.Color.FromArgb(((int) (((byte) (255)))), ((int) (((byte) (192)))),((int) (((byte) (128)))));
            this.ColorPressStroke = System.Drawing.Color.Aqua;
            this.FadingSpeed = 35;
            this.FlatAppearance.BorderSize = 0;
            this.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.ImageKey = "(none)";
            this.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.ImageOffset = 5;
            this.IsPressed = false;
            this.KeepPress = false;
            this.Location = new System.Drawing.Point(18, 18);
            this.Margin = new System.Windows.Forms.Padding(9);
            this.MaxImageSize = new System.Drawing.Point(96, 96);
            this.MenuPos = new System.Drawing.Point(0, 0);
            this.Name = "button1";
            this.Padding = new System.Windows.Forms.Padding(9);
            this.Radius = 6;
            this.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.Size = new System.Drawing.Size(150, 141);
            this.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.SplitDistance = 0;
            this.TabIndex = 3;
            this.Title = "Department Admin";
            this.UseVisualStyleBackColor = false;
            this.Visible = false;
        }
    }
}
