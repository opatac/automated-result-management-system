namespace ARMS
{
    partial class ControlDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlDashboard));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolLabelAcademicYear = new System.Windows.Forms.ToolStripLabel();
            this.toolLabelSemester = new System.Windows.Forms.ToolStripLabel();
            this.btnAnnouncements = new System.Windows.Forms.ToolStripButton();
            this.toolButtonLock = new System.Windows.Forms.ToolStripButton();
            this.btnEditPassword = new System.Windows.Forms.ToolStripButton();
            this.toolBtnAbout = new System.Windows.Forms.ToolStripButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageHODHome = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dataGridActivity = new System.Windows.Forms.DataGridView();
            this.activityDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activityText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelWelcome = new System.Windows.Forms.Label();
            this.tabPageStaff = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonAssignHOD = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxLecturers = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxFaculty = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxHDept = new System.Windows.Forms.ComboBox();
            this.buttonAddStaff = new System.Windows.Forms.Button();
            this.dataGridViewStaff = new System.Windows.Forms.DataGridView();
            this.staffID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.staffName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.staffCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPageMessages = new System.Windows.Forms.TabPage();
            this.tabPageLogs = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Username = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageSysConfig = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.labelStatus = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.buttonBackup = new System.Windows.Forms.Button();
            this.buttonRestore = new System.Windows.Forms.Button();
            this.textBoxBackupRestore = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBoxSemester = new System.Windows.Forms.ComboBox();
            this.pictureBoxDefaultPic = new System.Windows.Forms.PictureBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxAcademicYear = new System.Windows.Forms.TextBox();
            this.textBoxInstitution = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.toolStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageHODHome.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridActivity)).BeginInit();
            this.tabPageStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStaff)).BeginInit();
            this.tabPageLogs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPageSysConfig.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDefaultPic)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.toolStrip1.Font = new System.Drawing.Font("Trebuchet MS", 11F);
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolLabelAcademicYear,
            this.toolLabelSemester,
            this.btnAnnouncements,
            this.toolButtonLock,
            this.btnEditPassword,
            this.toolBtnAbout});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(834, 62);
            this.toolStrip1.TabIndex = 12;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolLabelAcademicYear
            // 
            this.toolLabelAcademicYear.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.toolLabelAcademicYear.Name = "toolLabelAcademicYear";
            this.toolLabelAcademicYear.Padding = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.toolLabelAcademicYear.Size = new System.Drawing.Size(117, 59);
            this.toolLabelAcademicYear.Text = "Academic Year:";
            // 
            // toolLabelSemester
            // 
            this.toolLabelSemester.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.toolLabelSemester.Name = "toolLabelSemester";
            this.toolLabelSemester.Padding = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.toolLabelSemester.Size = new System.Drawing.Size(84, 59);
            this.toolLabelSemester.Text = "Semester:";
            // 
            // btnAnnouncements
            // 
            this.btnAnnouncements.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAnnouncements.Image = global::ARMS.Properties.Resources.announcements;
            this.btnAnnouncements.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAnnouncements.Name = "btnAnnouncements";
            this.btnAnnouncements.Size = new System.Drawing.Size(52, 59);
            this.btnAnnouncements.Text = "New Announcement";
            this.btnAnnouncements.Click += new System.EventHandler(this.btnAnnouncements_Click);
            // 
            // toolButtonLock
            // 
            this.toolButtonLock.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonLock.Image = global::ARMS.Properties.Resources.Lock;
            this.toolButtonLock.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonLock.Name = "toolButtonLock";
            this.toolButtonLock.Size = new System.Drawing.Size(52, 59);
            this.toolButtonLock.Text = "Lock Session";
            this.toolButtonLock.Click += new System.EventHandler(this.toolButtonLock_Click);
            // 
            // btnEditPassword
            // 
            this.btnEditPassword.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEditPassword.Image = global::ARMS.Properties.Resources.advancedsettings;
            this.btnEditPassword.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditPassword.Name = "btnEditPassword";
            this.btnEditPassword.Size = new System.Drawing.Size(52, 59);
            this.btnEditPassword.Text = "Click to Update Password";
            this.btnEditPassword.Click += new System.EventHandler(this.btnEditPassword_Click);
            // 
            // toolBtnAbout
            // 
            this.toolBtnAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnAbout.Image = global::ARMS.Properties.Resources.arms128;
            this.toolBtnAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnAbout.Name = "toolBtnAbout";
            this.toolBtnAbout.Size = new System.Drawing.Size(52, 59);
            this.toolBtnAbout.Text = "About Automated Result Management System";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPageHODHome);
            this.tabControl1.Controls.Add(this.tabPageStaff);
            this.tabControl1.Controls.Add(this.tabPageMessages);
            this.tabControl1.Controls.Add(this.tabPageLogs);
            this.tabControl1.Controls.Add(this.tabPageSysConfig);
            this.tabControl1.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.tabControl1.Location = new System.Drawing.Point(6, 72);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(9, 6);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(822, 509);
            this.tabControl1.TabIndex = 13;
            // 
            // tabPageHODHome
            // 
            this.tabPageHODHome.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageHODHome.Controls.Add(this.panel4);
            this.tabPageHODHome.Location = new System.Drawing.Point(4, 28);
            this.tabPageHODHome.Name = "tabPageHODHome";
            this.tabPageHODHome.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHODHome.Size = new System.Drawing.Size(814, 477);
            this.tabPageHODHome.TabIndex = 6;
            this.tabPageHODHome.Text = "Home";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.dataGridActivity);
            this.panel4.Controls.Add(this.labelWelcome);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(808, 471);
            this.panel4.TabIndex = 4;
            // 
            // dataGridActivity
            // 
            this.dataGridActivity.AllowUserToAddRows = false;
            this.dataGridActivity.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridActivity.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridActivity.BackgroundColor = System.Drawing.Color.White;
            this.dataGridActivity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridActivity.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dataGridActivity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridActivity.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.activityDate,
            this.activityText});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridActivity.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridActivity.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridActivity.EnableHeadersVisualStyles = false;
            this.dataGridActivity.GridColor = System.Drawing.Color.White;
            this.dataGridActivity.Location = new System.Drawing.Point(0, 54);
            this.dataGridActivity.MultiSelect = false;
            this.dataGridActivity.Name = "dataGridActivity";
            this.dataGridActivity.ReadOnly = true;
            this.dataGridActivity.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridActivity.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridActivity.Size = new System.Drawing.Size(805, 414);
            this.dataGridActivity.TabIndex = 3;
            this.dataGridActivity.TabStop = false;
            // 
            // activityDate
            // 
            this.activityDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.activityDate.DividerWidth = 1;
            this.activityDate.FillWeight = 25F;
            this.activityDate.HeaderText = "Date";
            this.activityDate.MinimumWidth = 150;
            this.activityDate.Name = "activityDate";
            this.activityDate.ReadOnly = true;
            this.activityDate.Width = 160;
            // 
            // activityText
            // 
            this.activityText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.activityText.HeaderText = "Details";
            this.activityText.Name = "activityText";
            this.activityText.ReadOnly = true;
            // 
            // labelWelcome
            // 
            this.labelWelcome.BackColor = System.Drawing.Color.Transparent;
            this.labelWelcome.Font = new System.Drawing.Font("Trebuchet MS", 18F);
            this.labelWelcome.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelWelcome.Location = new System.Drawing.Point(6, 11);
            this.labelWelcome.Name = "labelWelcome";
            this.labelWelcome.Size = new System.Drawing.Size(488, 33);
            this.labelWelcome.TabIndex = 1;
            this.labelWelcome.Text = "System Activity";
            this.labelWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPageStaff
            // 
            this.tabPageStaff.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageStaff.Controls.Add(this.splitContainer1);
            this.tabPageStaff.Location = new System.Drawing.Point(4, 28);
            this.tabPageStaff.Name = "tabPageStaff";
            this.tabPageStaff.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageStaff.Size = new System.Drawing.Size(814, 477);
            this.tabPageStaff.TabIndex = 5;
            this.tabPageStaff.Text = "Manage Staff";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Size = new System.Drawing.Size(808, 471);
            this.splitContainer1.SplitterDistance = 132;
            this.splitContainer1.TabIndex = 4;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.buttonAssignHOD);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.comboBoxLecturers);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.comboBoxFaculty);
            this.groupBox3.Location = new System.Drawing.Point(9, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(790, 115);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dean of Faculty Assignment";
            // 
            // buttonAssignHOD
            // 
            this.buttonAssignHOD.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonAssignHOD.BackColor = System.Drawing.Color.Transparent;
            this.buttonAssignHOD.ForeColor = System.Drawing.Color.Black;
            this.buttonAssignHOD.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAssignHOD.ImageKey = "cap_graduate.png";
            this.buttonAssignHOD.ImageList = this.imageList1;
            this.buttonAssignHOD.Location = new System.Drawing.Point(621, 45);
            this.buttonAssignHOD.Name = "buttonAssignHOD";
            this.buttonAssignHOD.Padding = new System.Windows.Forms.Padding(6);
            this.buttonAssignHOD.Size = new System.Drawing.Size(125, 38);
            this.buttonAssignHOD.TabIndex = 3;
            this.buttonAssignHOD.Text = "&Assign";
            this.buttonAssignHOD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAssignHOD.UseVisualStyleBackColor = false;
            this.buttonAssignHOD.Click += new System.EventHandler(this.buttonAssignHOD_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "print_48.png");
            this.imageList1.Images.SetKeyName(1, "cap_graduate.png");
            this.imageList1.Images.SetKeyName(2, "pencil.png");
            this.imageList1.Images.SetKeyName(3, "edit_add.png");
            this.imageList1.Images.SetKeyName(4, "edit_remove.png");
            this.imageList1.Images.SetKeyName(5, "apply.png");
            this.imageList1.Images.SetKeyName(6, "Occup�.png");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(322, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(168, 18);
            this.label3.TabIndex = 1;
            this.label3.Text = "Choose Designated Staff:";
            // 
            // comboBoxLecturers
            // 
            this.comboBoxLecturers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLecturers.Font = new System.Drawing.Font("Trebuchet MS", 11F);
            this.comboBoxLecturers.FormattingEnabled = true;
            this.comboBoxLecturers.Location = new System.Drawing.Point(325, 55);
            this.comboBoxLecturers.Name = "comboBoxLecturers";
            this.comboBoxLecturers.Size = new System.Drawing.Size(264, 28);
            this.comboBoxLecturers.TabIndex = 0;
            this.comboBoxLecturers.SelectedIndexChanged += new System.EventHandler(this.comboBoxLecturers_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Faculty:";
            // 
            // comboBoxFaculty
            // 
            this.comboBoxFaculty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFaculty.Font = new System.Drawing.Font("Trebuchet MS", 11F);
            this.comboBoxFaculty.FormattingEnabled = true;
            this.comboBoxFaculty.Location = new System.Drawing.Point(9, 55);
            this.comboBoxFaculty.Name = "comboBoxFaculty";
            this.comboBoxFaculty.Size = new System.Drawing.Size(264, 28);
            this.comboBoxFaculty.TabIndex = 0;
            this.comboBoxFaculty.SelectedIndexChanged += new System.EventHandler(this.comboBoxFaculty_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.comboBoxHDept);
            this.groupBox1.Controls.Add(this.buttonAddStaff);
            this.groupBox1.Controls.Add(this.dataGridViewStaff);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(802, 329);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "View All Staff";
            // 
            // comboBoxHDept
            // 
            this.comboBoxHDept.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHDept.Font = new System.Drawing.Font("Trebuchet MS", 11F);
            this.comboBoxHDept.FormattingEnabled = true;
            this.comboBoxHDept.Location = new System.Drawing.Point(231, 289);
            this.comboBoxHDept.Name = "comboBoxHDept";
            this.comboBoxHDept.Size = new System.Drawing.Size(294, 28);
            this.comboBoxHDept.TabIndex = 5;
            // 
            // buttonAddStaff
            // 
            this.buttonAddStaff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAddStaff.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonAddStaff.BackColor = System.Drawing.Color.Transparent;
            this.buttonAddStaff.ForeColor = System.Drawing.Color.Black;
            this.buttonAddStaff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAddStaff.ImageKey = "edit_add.png";
            this.buttonAddStaff.ImageList = this.imageList1;
            this.buttonAddStaff.Location = new System.Drawing.Point(550, 280);
            this.buttonAddStaff.Name = "buttonAddStaff";
            this.buttonAddStaff.Padding = new System.Windows.Forms.Padding(6);
            this.buttonAddStaff.Size = new System.Drawing.Size(130, 43);
            this.buttonAddStaff.TabIndex = 4;
            this.buttonAddStaff.Text = "&Add Staff";
            this.buttonAddStaff.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAddStaff.UseVisualStyleBackColor = false;
            this.buttonAddStaff.Click += new System.EventHandler(this.buttonAddStaff_Click);
            // 
            // dataGridViewStaff
            // 
            this.dataGridViewStaff.AllowUserToAddRows = false;
            this.dataGridViewStaff.AllowUserToDeleteRows = false;
            this.dataGridViewStaff.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewStaff.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewStaff.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewStaff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStaff.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.staffID,
            this.staffName,
            this.Email,
            this.staffCode});
            this.dataGridViewStaff.Location = new System.Drawing.Point(6, 22);
            this.dataGridViewStaff.Name = "dataGridViewStaff";
            this.dataGridViewStaff.Size = new System.Drawing.Size(790, 254);
            this.dataGridViewStaff.TabIndex = 3;
            // 
            // staffID
            // 
            this.staffID.HeaderText = "ID";
            this.staffID.Name = "staffID";
            this.staffID.Width = 44;
            // 
            // staffName
            // 
            this.staffName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.staffName.HeaderText = "Name";
            this.staffName.Name = "staffName";
            // 
            // Email
            // 
            this.Email.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Email.FillWeight = 50F;
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            // 
            // staffCode
            // 
            this.staffCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.staffCode.FillWeight = 33F;
            this.staffCode.HeaderText = "Staff Code";
            this.staffCode.Name = "staffCode";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(93, 296);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(132, 18);
            this.label9.TabIndex = 1;
            this.label9.Text = "Select Department:";
            // 
            // tabPageMessages
            // 
            this.tabPageMessages.Location = new System.Drawing.Point(4, 28);
            this.tabPageMessages.Name = "tabPageMessages";
            this.tabPageMessages.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMessages.Size = new System.Drawing.Size(814, 477);
            this.tabPageMessages.TabIndex = 8;
            this.tabPageMessages.Text = "Messages";
            this.tabPageMessages.UseVisualStyleBackColor = true;
            // 
            // tabPageLogs
            // 
            this.tabPageLogs.BackColor = System.Drawing.Color.FloralWhite;
            this.tabPageLogs.Controls.Add(this.dataGridView1);
            this.tabPageLogs.Location = new System.Drawing.Point(4, 28);
            this.tabPageLogs.Name = "tabPageLogs";
            this.tabPageLogs.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageLogs.Size = new System.Drawing.Size(814, 477);
            this.tabPageLogs.TabIndex = 7;
            this.tabPageLogs.Text = "System Logs";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Username,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.GridColor = System.Drawing.Color.White;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(808, 471);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.TabStop = false;
            // 
            // Username
            // 
            this.Username.HeaderText = "Username";
            this.Username.MinimumWidth = 135;
            this.Username.Name = "Username";
            this.Username.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn1.DividerWidth = 1;
            this.dataGridViewTextBoxColumn1.FillWeight = 25F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Date";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 150;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 160;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.HeaderText = "Details";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // tabPageSysConfig
            // 
            this.tabPageSysConfig.BackColor = System.Drawing.Color.FloralWhite;
            this.tabPageSysConfig.Controls.Add(this.groupBox4);
            this.tabPageSysConfig.Controls.Add(this.groupBox2);
            this.tabPageSysConfig.Location = new System.Drawing.Point(4, 28);
            this.tabPageSysConfig.Name = "tabPageSysConfig";
            this.tabPageSysConfig.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSysConfig.Size = new System.Drawing.Size(814, 477);
            this.tabPageSysConfig.TabIndex = 9;
            this.tabPageSysConfig.Text = "System Config";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.labelStatus);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.progressBar1);
            this.groupBox4.Controls.Add(this.buttonBackup);
            this.groupBox4.Controls.Add(this.buttonRestore);
            this.groupBox4.Controls.Add(this.textBoxBackupRestore);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Location = new System.Drawing.Point(9, 215);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(796, 259);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Backup && Restore";
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(18, 187);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(0, 18);
            this.labelStatus.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.AliceBlue;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.label8.Location = new System.Drawing.Point(18, 28);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(3);
            this.label8.Size = new System.Drawing.Size(766, 50);
            this.label8.TabIndex = 2;
            this.label8.Text = "Use this panel to restore a saved database backup or to create and download backu" +
    "ps of the current system. NB: The backups are encrypted and should only ";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(3, 221);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(787, 32);
            this.progressBar1.TabIndex = 1;
            // 
            // buttonBackup
            // 
            this.buttonBackup.Font = new System.Drawing.Font("Trebuchet MS", 12F);
            this.buttonBackup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonBackup.ImageKey = "edit_add.png";
            this.buttonBackup.ImageList = this.imageList1;
            this.buttonBackup.Location = new System.Drawing.Point(364, 148);
            this.buttonBackup.Name = "buttonBackup";
            this.buttonBackup.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.buttonBackup.Size = new System.Drawing.Size(207, 57);
            this.buttonBackup.TabIndex = 0;
            this.buttonBackup.Text = "Backup";
            this.buttonBackup.UseVisualStyleBackColor = true;
            this.buttonBackup.Click += new System.EventHandler(this.buttonBackup_Click);
            // 
            // buttonRestore
            // 
            this.buttonRestore.Font = new System.Drawing.Font("Trebuchet MS", 12F);
            this.buttonRestore.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonRestore.ImageKey = "apply.png";
            this.buttonRestore.ImageList = this.imageList1;
            this.buttonRestore.Location = new System.Drawing.Point(577, 148);
            this.buttonRestore.Name = "buttonRestore";
            this.buttonRestore.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.buttonRestore.Size = new System.Drawing.Size(207, 57);
            this.buttonRestore.TabIndex = 0;
            this.buttonRestore.Text = "Restore";
            this.buttonRestore.UseVisualStyleBackColor = true;
            this.buttonRestore.Click += new System.EventHandler(this.buttonRestore_Click);
            // 
            // textBoxBackupRestore
            // 
            this.textBoxBackupRestore.Font = new System.Drawing.Font("Trebuchet MS", 12F);
            this.textBoxBackupRestore.Location = new System.Drawing.Point(18, 109);
            this.textBoxBackupRestore.Name = "textBoxBackupRestore";
            this.textBoxBackupRestore.Size = new System.Drawing.Size(766, 26);
            this.textBoxBackupRestore.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Trebuchet MS", 12F);
            this.label6.Location = new System.Drawing.Point(17, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 22);
            this.label6.TabIndex = 1;
            this.label6.Text = "File:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBoxSemester);
            this.groupBox2.Controls.Add(this.pictureBoxDefaultPic);
            this.groupBox2.Controls.Add(this.buttonSave);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.textBoxAcademicYear);
            this.groupBox2.Controls.Add(this.textBoxInstitution);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(799, 184);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "University Information";
            // 
            // comboBoxSemester
            // 
            this.comboBoxSemester.FormattingEnabled = true;
            this.comboBoxSemester.Items.AddRange(new object[] {
            "FIRST",
            "SECOND"});
            this.comboBoxSemester.Location = new System.Drawing.Point(198, 109);
            this.comboBoxSemester.Name = "comboBoxSemester";
            this.comboBoxSemester.Size = new System.Drawing.Size(429, 26);
            this.comboBoxSemester.TabIndex = 4;
            // 
            // pictureBoxDefaultPic
            // 
            this.pictureBoxDefaultPic.Location = new System.Drawing.Point(648, 44);
            this.pictureBoxDefaultPic.Name = "pictureBoxDefaultPic";
            this.pictureBoxDefaultPic.Size = new System.Drawing.Size(139, 134);
            this.pictureBoxDefaultPic.TabIndex = 3;
            this.pictureBoxDefaultPic.TabStop = false;
            this.pictureBoxDefaultPic.Click += new System.EventHandler(this.pictureBoxDefaultPic_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(500, 144);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(127, 34);
            this.buttonSave.TabIndex = 2;
            this.buttonSave.Text = "Save All";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 18);
            this.label5.TabIndex = 1;
            this.label5.Text = "Current Semester:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 18);
            this.label4.TabIndex = 1;
            this.label4.Text = "Name of Institution:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(541, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(246, 18);
            this.label7.TabIndex = 1;
            this.label7.Text = "Default User Picture. Click to change:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(159, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Academic Year/Session:";
            // 
            // textBoxAcademicYear
            // 
            this.textBoxAcademicYear.Location = new System.Drawing.Point(198, 77);
            this.textBoxAcademicYear.Name = "textBoxAcademicYear";
            this.textBoxAcademicYear.Size = new System.Drawing.Size(429, 23);
            this.textBoxAcademicYear.TabIndex = 0;
            // 
            // textBoxInstitution
            // 
            this.textBoxInstitution.Location = new System.Drawing.Point(198, 48);
            this.textBoxInstitution.Name = "textBoxInstitution";
            this.textBoxInstitution.Size = new System.Drawing.Size(429, 23);
            this.textBoxInstitution.TabIndex = 0;
            // 
            // timer1
            // 
            this.timer1.Interval = 3000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileName = "Backup";
            this.saveFileDialog1.RestoreDirectory = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "*.sql";
            this.openFileDialog1.FileName = "BackupFile";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.BalloonTipText = "The complete automated result management system for Universities";
            this.notifyIcon1.BalloonTipTitle = "Automated Result Management System";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Automated Result Management System";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.BalloonTipClicked += new System.EventHandler(this.notifyIcon1_BalloonTipClicked);
            this.notifyIcon1.BalloonTipShown += new System.EventHandler(this.notifyIcon1_BalloonTipShown);
            // 
            // ControlDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 589);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ControlDashboard";
            this.Text = " ";
            this.Load += new System.EventHandler(this.ControlDashboard_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPageHODHome.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridActivity)).EndInit();
            this.tabPageStaff.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStaff)).EndInit();
            this.tabPageLogs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPageSysConfig.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDefaultPic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolLabelSemester;
        private System.Windows.Forms.ToolStripButton btnAnnouncements;
        private System.Windows.Forms.ToolStripButton toolButtonLock;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageHODHome;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label labelWelcome;
        private System.Windows.Forms.DataGridView dataGridActivity;
        private System.Windows.Forms.DataGridViewTextBoxColumn activityDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn activityText;
        private System.Windows.Forms.TabPage tabPageStaff;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridViewStaff;
        private System.Windows.Forms.DataGridViewTextBoxColumn staffID;
        private System.Windows.Forms.DataGridViewTextBoxColumn staffName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn staffCode;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonAssignHOD;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxLecturers;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxFaculty;
        private System.Windows.Forms.TabPage tabPageLogs;
        private System.Windows.Forms.TabPage tabPageMessages;
        private MessageDisplay messageDisplay1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripLabel toolLabelAcademicYear;
        private System.Windows.Forms.ToolStripButton toolBtnAbout;
        private System.Windows.Forms.TabPage tabPageSysConfig;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBoxDefaultPic;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxAcademicYear;
        private System.Windows.Forms.TextBox textBoxInstitution;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button buttonBackup;
        private System.Windows.Forms.Button buttonRestore;
        private System.Windows.Forms.TextBox textBoxBackupRestore;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxSemester;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Username;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Button buttonAddStaff;
        private System.Windows.Forms.ComboBox comboBoxHDept;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ToolStripButton btnEditPassword;
    }
}
