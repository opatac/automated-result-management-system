using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Media;
using System.Windows.Forms;
using ARMS.Properties;

namespace ARMS
{
    public partial class ControlDashboard : Form
    {
        private string username;
        private readonly List<Messages> msgs = new List<Messages>();
        private int unreadCount;
        private UserDetails ud = new UserDetails();
        private readonly string userPath = Application.StartupPath + "\\controlUser.xml";
        private bool notificationIsMessage;//true if is message, false if else
        private bool local;

        public ControlDashboard(string usr, bool local)
        {
            this.local = local;
            InitializeComponent();
            this.username = usr;
            if (msgs != null)
            {
                if (local)
                {
                    msgs = new List<Messages>();
                }
                else
                {
                    msgs = Helper.GetReceivedMessages(username);
                    if (msgs == null)
                    {
                        msgs = new List<Messages>();
                    }
                    messageDisplay1.loadUsers(username);
                }
                messageDisplay1.AddMessages(msgs);
                unreadCount = msgs.Count(n => n.Unread);
                UpdateUnread();
                ud.Messages = msgs;
            }
        }

        private void loadLocalData()
        {
            ud = (UserDetails)Helper.Deserialize(userPath, typeof(UserDetails));
        }

        /// <summary>
        /// Serializes and stores all data using the UserDetails model
        /// </summary>
        private void saveLocalData()
        {
            Type type = typeof(UserDetails);
            Helper.SerializeData(type, userPath, ud);
        }

        private void UpdateUnread()
        {
            tabPageMessages.Text = string.Format("Messages ({0})", unreadCount);
        }

        private void ControlDashboard_Load(object sender, System.EventArgs e)
        {
            textBoxAcademicYear.Text = Helper.AcademicYear;
            textBoxInstitution.Text = Helper.getProperty("schoolName");
            comboBoxSemester.Text = Helper.getProperty("semester");
            timer1.Enabled = true;
            toolLabelAcademicYear.Text += " "+ textBoxAcademicYear.Text;
            toolLabelSemester.Text += " " +comboBoxSemester.Text;
            tt = new Timer{Interval = 100, Enabled = true};
            tt.Tick += LoadForm;
        }

        private Timer tt;

        private void LoadForm(object state, EventArgs eventArgs)
        {
            tt.Enabled = false;
            if (local)
                return;
            LoadActivities(false);

            //Load Faculties
            var results = Helper.selectResults(new[]{"facultyName"}, "faculties", "");
            foreach (var faculty in results)
            {
                comboBoxFaculty.Items.Add(faculty[0]);
            }
            comboBoxFaculty.SelectedIndex = 0;
            LoadLogs();
        }

        private List<string[]> logs;

        private void LoadLogs()
        {
            try
            {
                foreach (
                    string[] s in
                        Helper.selectResults(
                            new[] {"username", "logDate", "details"},
                            "logs", "")
                    )
                {
                    if (logs.Contains(s))
                        continue;
                    logs.Add(s);
                    dataGridView1.Rows.Add(s);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
            }
        }

        private void LoadStaff()
        {
            dataGridViewStaff.Rows.Clear();
            comboBoxLecturers.Items.Clear();
            allLecturers.Clear();
            try
            {
                //Load Lecturers
                int count = 0;
                foreach (
                    string[] s in
                        Helper.selectResults(
                            new[]
                                {
                                    "lecturers.username as username", "title", "lastName", "firstName", "email",
                                    "staffCode"
                                },
                            "lecturers",
                            new[] {"faculty"}, new[] {comboBoxFaculty.Text},
                            "JOIN users ON lecturers.username = users.username")
                    )
                {
                    count++;
                    LecturerDetails ld = new LecturerDetails
                        {
                            UserName = s[0],
                            Title = s[1],
                            LastName = s[2],
                            FirstName = s[3],
                            Email = s[4],
                            StaffCode = s[5]
                        };
                    var lname = string.Format("{0} {1} {2}", ld.Title, ld.LastName, ld.FirstName);
                    comboBoxLecturers.Items.Add(lname);
                    allLecturers.Add(lname, ld);
                    dataGridViewStaff.Rows.Add(new object[] {count, ld.UserName, lname, ld.Email, ld.StaffCode});
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
                MessageBox.Show("An error occurred while trying to load all staff for current department.");
            }

            //Load Departments
            try
            {
                comboBoxHDept.Items.Clear();
                var depts = Helper.selectResults(new[] {"deptName"}, "departments",
                                                 string.Format("WHERE faculty = '{0}'", comboBoxFaculty.Text));
                foreach (var dept in depts)
                {
                    comboBoxHDept.Items.Add(dept[0]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
                MessageBox.Show("An error occurred while trying to load the departments in the selected faculty.");                
            }
        }

        private readonly Dictionary<string, LecturerDetails> allLecturers = new Dictionary<string, LecturerDetails>();

        readonly List<Activity> acts = new List<Activity>();

        private void LoadActivities(bool notify)
        {
            foreach (var ac in Helper.GetActivities("'Y'", "Y"))
            {
                if (ac.IsIn(acts))
                {
                    continue;
                }
                acts.Add(ac);
                ud.Activities = acts;
                notificationIsMessage = false;
                dataGridActivity.Rows.Insert(0, new[] { ac.ActivityDate.ToLongDateString(), ac.Text });
                if (notify)
                {
                    notifyIcon1.ShowBalloonTip(3000, "New Activity", ac.Text, ToolTipIcon.Info);
                }
            }
        }

        private int timerCount;
        /// <summary>
        /// The amount of time to wait before session is locked. Since timer runs every 3 seconds, this means 180 seconds or three minutes
        /// </summary>
        private int timerTime = 60;


        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            if (local)
                return;
            if (acts.Count != Helper.GetActivitiesCount("'Y'", "Y"))
            {
                LoadActivities(true);
            }
            LoadLogs();
            timerCount++;
            if (timerCount == timerTime)
            {
                toolButtonLock.PerformClick();
            }
            timer1.Enabled = true;
        }

        private void buttonBackup_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException("This method is not yet implemented. Please contact the developer for further information.");
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBoxBackupRestore.Text = saveFileDialog1.FileName;

            }
        }

        private void buttonRestore_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBoxBackupRestore.Text = openFileDialog1.FileName;
                if (File.Exists(textBoxBackupRestore.Text))
                {
                    labelStatus.Text = "Backup File Loaded. Performing Restore...";
                    BackgroundWorker worker = new BackgroundWorker();
                    worker.DoWork += worker_DoWork;
                    worker.RunWorkerAsync(textBoxBackupRestore.Text);
                    worker.WorkerReportsProgress = true;
                    worker.RunWorkerCompleted += worker_RunWorkerCompleted;
                    worker.ProgressChanged += worker_ProgressChanged;
                }
            }
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true)
            {
                labelStatus.Text = "Canceled!";
            }
            else if (e.Error != null)
            {
                labelStatus.Text = "Error: " + e.Error.Message;
            }
            else
            {
                labelStatus.Text = "Restore Complete! Please relogin to load new data.";
            }
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            string file = e.Argument as string;
            /*string dump = Crypto.Encrypt(File.ReadAllText(file));
            File.WriteAllText(file + ".encrypted", dump);*/
            string[] queries = Crypto.Decrypt(File.ReadAllText(file)).Split(';');
            int length = queries.Length;
            for (int i = 0; i < length; i++)
            {
                string s = queries[i];
                if (s.Trim() != "")
                {
                    Helper.ExecuteNonQuery(s + ";", Helper.BackupConnection);
                    worker.ReportProgress((i * 100)/ length);
                }
            }
        }

        private SoundPlayer sp;

        private void notifyIcon1_BalloonTipShown(object sender, EventArgs e)
        {
            sp = sp ?? new SoundPlayer();
            sp.Stream = Resources._2cool;
            sp.Play();
        }

        private void messageDisplay1_MsgReadEvent(object sender, MessageEvent args)
        {
            unreadCount--;
            UpdateUnread();
        }

        private void messageDisplay1_NewMsgEvent(object sender, MessageEvent args)
        {
            sp = sp ?? new SoundPlayer();
            sp.Stream = Resources.kutelarm;
            sp.Play();
            string msg = args.msg.Subject + args.msg.Message.StripRTF().Excerpt(40);
            notifyIcon1.ShowBalloonTip(5000, "New Message From: " + args.msg.Sender,
                                       "Subject: " + msg,
                                       ToolTipIcon.Info);
            notificationIsMessage = true;
            unreadCount++;
            UpdateUnread();
        }

        private void notifyIcon1_BalloonTipClicked(object sender, EventArgs e)
        {
            if (notificationIsMessage)
            {
                tabControl1.SelectTab(tabPageMessages);
            }
            else
            {
                tabControl1.SelectTab(0);
            }
            BringToFront();
            Focus();
            if (WindowState == FormWindowState.Minimized)
            {
                WindowState = FormWindowState.Normal;
            }
        }

        private void assignRole(string role)
        {
            if (buttonAssignHOD.Text == "Un&Assign")
            {
                var dr =
                    MessageBox.Show(
                        string.Format(
                            "The {1} role is already assigned to {0}. If you want to unassign this role, Click Retry, otherwise Click Abort/Ignore.",
                            currentLecturer.FormalName, role), "Role Already Assigned",
                        MessageBoxButtons.AbortRetryIgnore,
                        MessageBoxIcon.Warning);
                if (dr == DialogResult.Retry)
                {
                    int d = Helper.DbDelete("roles", new[] { "username", "role" },
                                            new[] { currentLecturer.UserName, role });
                    if (d == -1)
                    {
                        MessageBox.Show("An error occured while trying to unassign this role. Please try again.");
                        return;
                    }
                    else
                    {
                        MessageBox.Show(
                            string.Format(
                                "{0} has been unassigned as {1} of {2}.",
                                currentLecturer.FormalName, role, comboBoxFaculty.Text), "Role UnAssigned Successfully",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);

                        Helper.logActivity(
                            new Activity
                            {
                                Faculty = comboBoxFaculty.Text,
                                Username = username,
                                Text =
                                    string.Format(
                                        "The Dean of Faculty has unassigned {0} as the Head of Department of {2}.",
                                        currentLecturer.NiceName, role, comboBoxFaculty.Text)
                            });

                    }
                }
            }
            else
            {
                int di = Helper.dbInsert("roles", new[] { "username", "role" },
                                         new[] { currentLecturer.UserName, role });
                if (di == -1)
                {
                    MessageBox.Show("An error occured while trying to assign this role. Please try again.");
                }
                else
                {
                    MessageBox.Show(
                        string.Format(
                            "The {1} role has been assigned successfully to {0}.",
                            currentLecturer.FormalName, role), "Role Assigned Successfully", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    Helper.logActivity(
                        new Activity
                        {
                            Faculty = comboBoxFaculty.Text,
                            Username = username,
                            Text =
                                string.Format(
                                    "The Dean of Faculty has assigned {0} as the Dean of Faculty of {2}.",
                                    currentLecturer.NiceName, role, comboBoxFaculty.Text)
                        });

                }
            }
        }

        private void buttonAssignHOD_Click(object sender, EventArgs e)
        {
            assignRole(UserRoles.Dean);
        }

        private LecturerDetails currentLecturer;

        private void comboBoxLecturers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var combo = (ComboBox)sender;
                currentLecturer = allLecturers[combo.Text];
                var pCount = Helper.selectResults(new[] { "COUNT(*)" }, "roles", new[] { "department", "role" }, new[] { currentLecturer.UserName, "Dean" }, "JOIN lecturers on lecturers.username = roles.username");
                if (pCount != null)
                {
                    if (Convert.ToInt32(pCount[0][0]) > 0)
                    {
                        MessageBox.Show(
                            "This role is already assigned to someone else in the department. Please ensure you have selected the proper department and try again.",
                            "Role Already Assigned", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                string rCount = Helper.selectResults(new[] { "COUNT(*)" }, "roles", new[] { "username", "role" }, new[] { currentLecturer.UserName, "Dean" }, "")[0][0];
                buttonAssignHOD.Text = Convert.ToInt32(rCount) > 0 ? "Un&Assign" : "&Assign";
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
            }
        }

        private void comboBoxFaculty_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadStaff();
        }

        private void toolButtonLock_Click(object sender, EventArgs e)
        {
            Locked lc = new Locked(username);
            Visible = false;
            lc.UnlockEvent += (o, args) => { Visible = args.Unlocked; timerCount = 0; };
            lc.ShowDialog();
        }

        private void btnAnnouncements_Click(object sender, EventArgs e)
        {
            Announcer anr = new Announcer(username, "", "");
            anr.ShowDialog();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                Helper.Update("schooldetails", new[] {"academicYear", "semester", "schoolName"},
                              new[] {textBoxAcademicYear.Text, comboBoxSemester.Text, textBoxInstitution.Text},
                              new[] {"Y"},
                              new[] {"Y"}, "");
                MessageBox.Show("School details updated successfully.",
                "Save Successful", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
                MessageBox.Show("An error occurred while trying to save the school details. Please try again.",
                                "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBoxDefaultPic_Click(object sender, EventArgs e)
        {

        }

        private void btnEditPassword_Click(object sender, EventArgs e)
        {
            UpdatePassword up = new UpdatePassword();
            up.ShowDialog();
        }

        private void buttonAddStaff_Click(object sender, EventArgs e)
        {
            //Let's generate a random staff code
            Random rnd = new Random(DateTime.Now.Millisecond);
            if (comboBoxHDept.Text == "")
            {
                MessageBox.Show("Please select a department first and try again.");
                return;
            }
            var department =
                Helper.selectResults(new[] { "shortName", "faculty", "courseDuration" },
                                      "departments", new[] { "deptName" }, new[] { comboBoxHDept.Text },
                                      "");
            DeptDetails dd = new DeptDetails
            {
                DeptName = comboBoxHDept.Text,
                ShortName = department[0][0],

                Faculty = department[0][1],
                CourseDuration = department[0][2]
            };

            //Let's retrieve a list of current staff codes
            var staffcodes = allLecturers.Select(n => n.Value.StaffCode).ToList();
            string staffCode = dd.ShortName + rnd.Next(1000, 9999);

            while (staffcodes.Contains(staffCode))
            {
                staffCode = dd.ShortName + rnd.Next(1000, 9999);
            }
            LecturerReg lr = new LecturerReg(username, dd.DeptName, dd.Faculty, staffCode);
            lr.ShowDialog();
            LoadStaff();
        }
    }
}
