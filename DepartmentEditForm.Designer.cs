namespace ARMS
{
    partial class DepartmentEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DepartmentEditForm));
            this.panelRegForm = new System.Windows.Forms.Panel();
            this.comboBoxPType = new System.Windows.Forms.ComboBox();
            this.richTextBoxPrograms = new System.Windows.Forms.RichTextBox();
            this.textBoxCourseDuration = new System.Windows.Forms.TextBox();
            this.textBoxDeptShortName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelTitle = new System.Windows.Forms.Label();
            this.panelRegForm.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelRegForm
            // 
            this.panelRegForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelRegForm.Controls.Add(this.comboBoxPType);
            this.panelRegForm.Controls.Add(this.richTextBoxPrograms);
            this.panelRegForm.Controls.Add(this.textBoxCourseDuration);
            this.panelRegForm.Controls.Add(this.textBoxDeptShortName);
            this.panelRegForm.Controls.Add(this.label5);
            this.panelRegForm.Controls.Add(this.label4);
            this.panelRegForm.Controls.Add(this.label6);
            this.panelRegForm.Controls.Add(this.label3);
            this.panelRegForm.Controls.Add(this.label2);
            this.panelRegForm.Location = new System.Drawing.Point(83, 89);
            this.panelRegForm.Name = "panelRegForm";
            this.panelRegForm.Size = new System.Drawing.Size(450, 205);
            this.panelRegForm.TabIndex = 12;
            // 
            // comboBoxPType
            // 
            this.comboBoxPType.Font = new System.Drawing.Font("Tahoma", 11F);
            this.comboBoxPType.FormattingEnabled = true;
            this.comboBoxPType.Items.AddRange(new object[] {
            "B.Sc.",
            "B.Eng.",
            "B.A."});
            this.comboBoxPType.Location = new System.Drawing.Point(168, 77);
            this.comboBoxPType.Name = "comboBoxPType";
            this.comboBoxPType.Size = new System.Drawing.Size(266, 26);
            this.comboBoxPType.TabIndex = 5;
            // 
            // richTextBoxPrograms
            // 
            this.richTextBoxPrograms.Location = new System.Drawing.Point(168, 115);
            this.richTextBoxPrograms.Name = "richTextBoxPrograms";
            this.richTextBoxPrograms.Size = new System.Drawing.Size(266, 73);
            this.richTextBoxPrograms.TabIndex = 4;
            this.richTextBoxPrograms.Text = "";
            // 
            // textBoxCourseDuration
            // 
            this.textBoxCourseDuration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxCourseDuration.Font = new System.Drawing.Font("Tahoma", 11F);
            this.textBoxCourseDuration.Location = new System.Drawing.Point(168, 43);
            this.textBoxCourseDuration.Name = "textBoxCourseDuration";
            this.textBoxCourseDuration.Size = new System.Drawing.Size(266, 25);
            this.textBoxCourseDuration.TabIndex = 3;
            // 
            // textBoxDeptShortName
            // 
            this.textBoxDeptShortName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDeptShortName.Font = new System.Drawing.Font("Tahoma", 11F);
            this.textBoxDeptShortName.Location = new System.Drawing.Point(168, 12);
            this.textBoxDeptShortName.MaxLength = 3;
            this.textBoxDeptShortName.Name = "textBoxDeptShortName";
            this.textBoxDeptShortName.Size = new System.Drawing.Size(266, 25);
            this.textBoxDeptShortName.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8F);
            this.label5.Location = new System.Drawing.Point(21, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 40);
            this.label5.TabIndex = 2;
            this.label5.Text = "Enter supported programmes separated by new line:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label4.Location = new System.Drawing.Point(18, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 18);
            this.label4.TabIndex = 2;
            this.label4.Text = "Programs Offered:";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label6.Location = new System.Drawing.Point(21, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 18);
            this.label6.TabIndex = 2;
            this.label6.Text = "Programme Type:";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label3.Location = new System.Drawing.Point(21, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Course Duration:";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label2.Location = new System.Drawing.Point(21, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Short Name:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnSave);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 307);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(609, 54);
            this.panel3.TabIndex = 11;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnSave.Location = new System.Drawing.Point(500, 11);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(97, 34);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.panel1.Controls.Add(this.labelTitle);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(609, 51);
            this.panel1.TabIndex = 10;
            // 
            // labelTitle
            // 
            this.labelTitle.BackColor = System.Drawing.Color.Black;
            this.labelTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTitle.Font = new System.Drawing.Font("Candara", 18F);
            this.labelTitle.ForeColor = System.Drawing.Color.FloralWhite;
            this.labelTitle.Location = new System.Drawing.Point(0, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(609, 51);
            this.labelTitle.TabIndex = 4;
            this.labelTitle.Text = "Department Settings";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DepartmentEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FloralWhite;
            this.ClientSize = new System.Drawing.Size(609, 361);
            this.Controls.Add(this.panelRegForm);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(625, 399);
            this.MinimumSize = new System.Drawing.Size(535, 352);
            this.Name = "DepartmentEditForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Department Edit Form";
            this.Load += new System.EventHandler(this.DepartmentEditForm_Load);
            this.panelRegForm.ResumeLayout(false);
            this.panelRegForm.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelRegForm;
        private System.Windows.Forms.TextBox textBoxDeptShortName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.RichTextBox richTextBoxPrograms;
        private System.Windows.Forms.TextBox textBoxCourseDuration;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxPType;
        private System.Windows.Forms.Label label6;
    }
}
