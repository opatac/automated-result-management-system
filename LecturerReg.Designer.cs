namespace ARMS
{
    partial class LecturerReg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LecturerReg));
            this.labelTitle = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelMSG = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textRePassword = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textPassword = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textEmail = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textLName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textFName = new System.Windows.Forms.TextBox();
            this.textTitle = new System.Windows.Forms.TextBox();
            this.textUserName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panelRegForm = new System.Windows.Forms.Panel();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButtonMale = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.textMName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.labelWelcome = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelRegForm.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelTitle.Font = new System.Drawing.Font("Candara", 18F);
            this.labelTitle.ForeColor = System.Drawing.Color.White;
            this.labelTitle.Location = new System.Drawing.Point(0, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(564, 51);
            this.labelTitle.TabIndex = 4;
            this.labelTitle.Text = "Lecturer Update Form: ";
            this.labelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.panel1.Controls.Add(this.labelTitle);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(564, 51);
            this.panel1.TabIndex = 5;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.labelMSG);
            this.panel3.Controls.Add(this.btnSave);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 425);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(564, 64);
            this.panel3.TabIndex = 7;
            // 
            // labelMSG
            // 
            this.labelMSG.AutoSize = true;
            this.labelMSG.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelMSG.ForeColor = System.Drawing.Color.Red;
            this.labelMSG.Location = new System.Drawing.Point(14, 24);
            this.labelMSG.Name = "labelMSG";
            this.labelMSG.Size = new System.Drawing.Size(47, 17);
            this.labelMSG.TabIndex = 1;
            this.labelMSG.Text = "Ready";
            this.labelMSG.Visible = false;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnSave.Location = new System.Drawing.Point(429, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(123, 39);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "&Save && Continue";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label2.Location = new System.Drawing.Point(14, 215);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password:";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label3.Location = new System.Drawing.Point(14, 246);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Retype Password:";
            // 
            // textRePassword
            // 
            this.textRePassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textRePassword.Font = new System.Drawing.Font("Tahoma", 11F);
            this.textRePassword.Location = new System.Drawing.Point(168, 243);
            this.textRePassword.Name = "textRePassword";
            this.textRePassword.Size = new System.Drawing.Size(363, 25);
            this.textRePassword.TabIndex = 9;
            this.textRePassword.UseSystemPasswordChar = true;
            this.textRePassword.TextChanged += new System.EventHandler(this.textRePassword_Leave);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label4.Location = new System.Drawing.Point(14, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 18);
            this.label4.TabIndex = 2;
            this.label4.Text = "Title:";
            // 
            // textPassword
            // 
            this.textPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textPassword.Font = new System.Drawing.Font("Tahoma", 11F);
            this.textPassword.Location = new System.Drawing.Point(168, 212);
            this.textPassword.Name = "textPassword";
            this.textPassword.Size = new System.Drawing.Size(363, 25);
            this.textPassword.TabIndex = 8;
            this.textPassword.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label5.Location = new System.Drawing.Point(14, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 18);
            this.label5.TabIndex = 2;
            this.label5.Text = "First Name:";
            // 
            // textEmail
            // 
            this.textEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textEmail.Font = new System.Drawing.Font("Tahoma", 11F);
            this.textEmail.Location = new System.Drawing.Point(168, 276);
            this.textEmail.Name = "textEmail";
            this.textEmail.Size = new System.Drawing.Size(363, 25);
            this.textEmail.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label6.Location = new System.Drawing.Point(14, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 18);
            this.label6.TabIndex = 2;
            this.label6.Text = "Last Name:";
            // 
            // textLName
            // 
            this.textLName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textLName.Font = new System.Drawing.Font("Tahoma", 11F);
            this.textLName.Location = new System.Drawing.Point(168, 44);
            this.textLName.Name = "textLName";
            this.textLName.Size = new System.Drawing.Size(363, 25);
            this.textLName.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label9.Location = new System.Drawing.Point(14, 279);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 18);
            this.label9.TabIndex = 2;
            this.label9.Text = "Email:";
            // 
            // textFName
            // 
            this.textFName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textFName.Font = new System.Drawing.Font("Tahoma", 11F);
            this.textFName.Location = new System.Drawing.Point(168, 76);
            this.textFName.Name = "textFName";
            this.textFName.Size = new System.Drawing.Size(363, 25);
            this.textFName.TabIndex = 3;
            // 
            // textTitle
            // 
            this.textTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textTitle.Font = new System.Drawing.Font("Tahoma", 11F);
            this.textTitle.Location = new System.Drawing.Point(168, 13);
            this.textTitle.Name = "textTitle";
            this.textTitle.Size = new System.Drawing.Size(363, 25);
            this.textTitle.TabIndex = 1;
            // 
            // textUserName
            // 
            this.textUserName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textUserName.BackColor = System.Drawing.SystemColors.Window;
            this.textUserName.Font = new System.Drawing.Font("Tahoma", 11F);
            this.textUserName.Location = new System.Drawing.Point(168, 181);
            this.textUserName.Name = "textUserName";
            this.textUserName.ReadOnly = true;
            this.textUserName.Size = new System.Drawing.Size(363, 25);
            this.textUserName.TabIndex = 7;
            this.textUserName.Enter += new System.EventHandler(this.textUserName_Enter);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label1.Location = new System.Drawing.Point(14, 184);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Username:";
            // 
            // panelRegForm
            // 
            this.panelRegForm.Controls.Add(this.radioButton2);
            this.panelRegForm.Controls.Add(this.radioButtonMale);
            this.panelRegForm.Controls.Add(this.label1);
            this.panelRegForm.Controls.Add(this.textUserName);
            this.panelRegForm.Controls.Add(this.textTitle);
            this.panelRegForm.Controls.Add(this.textFName);
            this.panelRegForm.Controls.Add(this.label12);
            this.panelRegForm.Controls.Add(this.label9);
            this.panelRegForm.Controls.Add(this.textLName);
            this.panelRegForm.Controls.Add(this.textMName);
            this.panelRegForm.Controls.Add(this.label6);
            this.panelRegForm.Controls.Add(this.textEmail);
            this.panelRegForm.Controls.Add(this.label5);
            this.panelRegForm.Controls.Add(this.textPassword);
            this.panelRegForm.Controls.Add(this.label4);
            this.panelRegForm.Controls.Add(this.textRePassword);
            this.panelRegForm.Controls.Add(this.label7);
            this.panelRegForm.Controls.Add(this.label3);
            this.panelRegForm.Controls.Add(this.label2);
            this.panelRegForm.Font = new System.Drawing.Font("Candara", 10F);
            this.panelRegForm.Location = new System.Drawing.Point(9, 105);
            this.panelRegForm.Name = "panelRegForm";
            this.panelRegForm.Size = new System.Drawing.Size(547, 314);
            this.panelRegForm.TabIndex = 8;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(299, 144);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(67, 21);
            this.radioButton2.TabIndex = 6;
            this.radioButton2.Text = "Female";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButtonMale
            // 
            this.radioButtonMale.AutoSize = true;
            this.radioButtonMale.Checked = true;
            this.radioButtonMale.Location = new System.Drawing.Point(168, 144);
            this.radioButtonMale.Name = "radioButtonMale";
            this.radioButtonMale.Size = new System.Drawing.Size(54, 21);
            this.radioButtonMale.TabIndex = 5;
            this.radioButtonMale.TabStop = true;
            this.radioButtonMale.Text = "Male";
            this.radioButtonMale.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label12.Location = new System.Drawing.Point(14, 114);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 18);
            this.label12.TabIndex = 2;
            this.label12.Text = "Middle Name:";
            // 
            // textMName
            // 
            this.textMName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textMName.Font = new System.Drawing.Font("Tahoma", 11F);
            this.textMName.Location = new System.Drawing.Point(168, 111);
            this.textMName.Name = "textMName";
            this.textMName.Size = new System.Drawing.Size(363, 25);
            this.textMName.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 11F);
            this.label7.Location = new System.Drawing.Point(14, 144);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 18);
            this.label7.TabIndex = 2;
            this.label7.Text = "Gender:";
            // 
            // labelWelcome
            // 
            this.labelWelcome.AutoSize = true;
            this.labelWelcome.Font = new System.Drawing.Font("Candara", 11F);
            this.labelWelcome.Location = new System.Drawing.Point(12, 68);
            this.labelWelcome.Name = "labelWelcome";
            this.labelWelcome.Size = new System.Drawing.Size(473, 18);
            this.labelWelcome.TabIndex = 9;
            this.labelWelcome.Text = "Welcome to ARMS Staff Regisration Form. Enter Staff details in form below:";
            // 
            // LecturerReg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(564, 489);
            this.Controls.Add(this.labelWelcome);
            this.Controls.Add(this.panelRegForm);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(580, 527);
            this.MinimumSize = new System.Drawing.Size(580, 527);
            this.Name = "LecturerReg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lecturer Update Form";
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panelRegForm.ResumeLayout(false);
            this.panelRegForm.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox textRePassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox textPassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textEmail;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textLName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textFName;
        private System.Windows.Forms.TextBox textTitle;
        private System.Windows.Forms.TextBox textUserName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelRegForm;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textMName;
        private System.Windows.Forms.Label labelMSG;
        private System.Windows.Forms.Label labelWelcome;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButtonMale;
        private System.Windows.Forms.Label label7;
    }
}
