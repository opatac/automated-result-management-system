using System;
using System.Windows.Forms;
using ARMS.Properties;

namespace ARMS
{
    public partial class LecturerReg : Form
    {
        private readonly string _department;
        private readonly string _faculty;
        private readonly string _staffCode;
        private readonly bool _updating;
        private readonly string username;
        private Timer msgTimer;

        public LecturerReg(string username, bool gender, LecturerDetails lcd, bool updating)
        {
            InitializeComponent();
            labelTitle.Text += " : " + lcd.StaffCode;
            _department = lcd.Department;
            _faculty = lcd.Faculty;
            _staffCode = lcd.StaffCode;
            textUserName.Text = lcd.UserName;
            _updating = updating;
            textFName.Text = lcd.FirstName;
            textMName.Text = lcd.MiddleName;
            textLName.Text = lcd.LastName;
            textEmail.Text = lcd.Email;
            textTitle.Text = lcd.Title;
            textPassword.Text = textRePassword.Text = lcd.StaffCode;
            this.username = username;
            radioButtonMale.Checked = gender;
        }

        public LecturerReg(string username, string department, string faculty, string staffCode)
        {
            InitializeComponent();
            this.username = username;
            _department = department;
            _faculty = faculty;
            _staffCode = staffCode;
            labelTitle.Text += " : " + staffCode;
            textPassword.UseSystemPasswordChar = textRePassword.UseSystemPasswordChar = false;
            textPassword.Text = textRePassword.Text = staffCode;
            textPassword.Enabled = textRePassword.Enabled = false;
            textUserName.Enabled = true;
            textUserName.ReadOnly = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (textPassword.Text != textRePassword.Text)
            {
                MessageBox.Show(Resources.correctPassword, "Incorrect Password", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return;
            }

            if (_updating)
            {
                if (textPassword.Text == _staffCode)
                {
                    MessageBox.Show(
                        "You have not yet changed the default password. Please change the default password which is your staff code to something else. This will help ensure that your profile is secure.",
                        "No Default Password", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    return;
                }
                if (textPassword.Text.Length < 8)
                {
                    MessageBox.Show("Password cannot be less than 8 characters.", "Password too Short",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Asterisk);
                    return;
                }
            }
            //Update User Password and Gender
            string gender = radioButtonMale.Checked ? "M" : "F";
            if (!_updating)
            {
                var dr = DialogResult.None;
                try
                {
                    var dlecturers =
                        Helper.selectResults(new[] {"username, deleted"}, "users",
                                             string.Format("WHERE username = '{0}'", textUserName.Text));
                    if (dlecturers.Length > 0)
                    {
                        var dlecturer = dlecturers[0];
                        if (dlecturer[1] == "Y")
                        {
                            DialogResult dd =
                                MessageBox.Show(
                                    "A staff with the specified username already exists. Do you want to restore the original staff records?",
                                    "Staff Deleted Previously", MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);
                            if (dd == DialogResult.Yes)
                            {
                                int rst = Helper.Update("users", new[] { "deleted" }, new[] { "N" }, new[] { "username" },
                                                        new[] { textUserName.Text }, "");
                                if (rst != -1)
                                {
                                    MessageBox.Show(
                                        "The specified staff has been restored successfully and may now login with their previous details.",
                                        "Staff Restored Successfully", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }
                            }
                            else if (dd == DialogResult.No)
                            {
                                MessageBox.Show(
                                    "Please enter a different username and try again.",
                                    "Staff Already Exists", MessageBoxButtons.OK,
                                    MessageBoxIcon.Asterisk);
                                textUserName.Focus();
                                return;
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            MessageBox.Show(
                                "A staff with the specified username already exists. Please enter a different username and try again.",
                                "Staff Already Exists", MessageBoxButtons.OK,
                                MessageBoxIcon.Asterisk);
                            textUserName.Focus();
                            return;
                        }
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show(Resources.errorAddingStaff);
                    return;
                }
                if (Helper.dbInsert("users", new[] {"username"}, new[] {textUserName.Text}) == -1)
                {
                    MessageBox.Show(Resources.errorAddingStaff);
                    return;
                }
                if (Helper.dbInsert("roles", new[] {"username", "role"}, new[] {textUserName.Text, "Lecturer"}) == -1)
                {
                    MessageBox.Show(Resources.errorSavingStaff);
                    return;
                }
                while (
                    Helper.dbInsert("lecturers", new[] {"username", "staffCode"}, new[] {textUserName.Text, _staffCode}) ==
                    -1 && dr == DialogResult.Yes)
                {
                    dr = MessageBox.Show(Resources.errorSavingStaff, "Error Adding Staff", MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question);
                }
                if (dr == DialogResult.No) return;
            }
            int result = Helper.Update("users", new[] {"password", "gender"},
                                       new[] {Crypto.computeMD5(textPassword.Text), gender}, new[] {"username"},
                                       new[] {textUserName.Text}, "");
            if (result == -1)
            {
                MessageBox.Show(Resources.errorSavingStaff);
                return;
            }

            //Update Lecturer
            result = Helper.Update("lecturers",
                                   new[]
                                       {
                                           "username", "title", "firstName", "middleName", "lastName", "email",
                                           "faculty", "department"
                                       },
                                   new[]
                                       {
                                           textUserName.Text, textTitle.Text, textFName.Text, textLName.Text,
                                           textLName.Text, textEmail.Text, _faculty, _department
                                       }, new[] {"staffCode"}, new[] {_staffCode}, "");
            if (result == -1)
            {
                MessageBox.Show(Resources.errorSavingStaff);
                return;
            }

            string success = _updating
                                 ? "Your details have been updated successfully. You may now login into the system."
                                 : string.Format(
                                     "The specified staff has been added successfully. Please note the StaffCode {0}. They can now login with the specifed username and the StaffCode as their password",
                                     _staffCode);

            MessageBox.Show(success, "User Updated Successfully", MessageBoxButtons.OK, MessageBoxIcon.Information);
            if (_updating)
            {
                try
                {
                    Helper.logActivity(new Activity
                        {
                            Text =
                                string.Format("{0} has updated {1} details successfully.", username,
                                              gender == "M" ? "his" : "her"),
                            Department = _department,
                            Faculty = _faculty,
                            Username = username
                        });
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                        "An error has occurred. Please contact the admin for more info. Error Message: " + ex.Message,
                        "Error Encountered.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                //Globals.rk.SetValue("FirstRun", 1);//This will enable the application to know that this is the first time the lecturer is using the app
                //startLogin();
            }
            else
            {
                try
                {
                    Helper.logActivity(new Activity
                        {
                            Text =
                                string.Format("{0} added {1} as a new staff of the Department of {2}.", username,
                                              textTitle.Text + " " + textFName.Text + " " + textLName.Text, _department),
                            Department = _department,
                            Faculty = _faculty,
                            Username = username
                        });
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                        "An error has occurred. Please contact the admin for more info. Error Message: " + ex.Message,
                        "Error Encountered.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            Close();
        }

/*
        private void startLogin()
        {
            Hide();
            var ch = new ChooseLoad(new[] {"Lecturer"}, textUserName.Text, "", radioButtonMale.Checked,
                                    false);
            ch.Show();
        }
*/

        private void textRePassword_Leave(object sender, EventArgs e)
        {
            if (textPassword.Text == textRePassword.Text)
            {
                return;
            }
            StopMsgTimer();
            labelMSG.Text = Resources.pwdNoMatch;
            BeginMsgTimer();
        }

        private void BeginMsgTimer()
        {
            if (msgTimer == null)
            {
                msgTimer = new Timer {Interval = 500};
                msgTimer.Tick += msgTimer_Tick;
            }
            msgTimer.Enabled = true;
        }

        private void StopMsgTimer()
        {
            if (msgTimer == null)
            {
                return;
            }
            msgTimer.Enabled = false;
            labelMSG.Visible = false;
        }

        private void msgTimer_Tick(object sender, EventArgs e)
        {
            if (textPassword.Text == textRePassword.Text)
            {
                StopMsgTimer();
                return;
            }

            if (labelMSG.Visible)
            {
                labelMSG.Visible = false;
            }
            else
            {
                labelMSG.Visible = true;
            }
        }

        private void textUserName_Enter(object sender, EventArgs e)
        {
            textUserName.Text = (textFName.Text + "." + textLName.Text).ToLowerInvariant();
            textEmail.Text = textUserName.Text + "@unn.edu.ng";
        }
    }
}
