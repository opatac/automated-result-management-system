using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ARMS
{
    public partial class MessageDisplay : UserControl
    {
        private int unreadCount;
        public string username;


        public void loadUsers(string department, string username)
        {
            try
            {
                comboBoxTo.Items.Clear();
                this.username = username;
                foreach (
                    var item in
                        Helper.selectResults(new[] {"username"}, "lecturers", new[] {"department"}, new[] {department},
                                             string.Format("AND username != '{0}'", username)))
                {
                    comboBoxTo.Items.Add(item[0]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
            }
            timer1.Enabled = true;
        }

        public void loadUsers(string username)
        {
            try
            {
                comboBoxTo.Items.Clear();
                this.username = username;
                foreach (
                    var item in
                        Helper.selectResults(new[] {"username"}, "users",
                                             string.Format(" WHERE username != '{0}'", username)))
                {
                    comboBoxTo.Items.Add(item[0]);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
            }
            timer1.Enabled = true;
        }

        [Description("Message Read Event. Subscribe to this event to be notified when a message is read.")]
        public event MsgRead MsgReadEvent;

        [Description("New Message Event for the control. Subscribe to this event to be notified of new messages.")]
        public event NewMessage NewMsgEvent;

        protected virtual void OnNewMsgEvent(MessageEvent args)
        {
            NewMessage handler = NewMsgEvent;
            if (handler != null) handler(this, args);
        }

        protected virtual void OnMsgReadEvent(MessageEvent args)
        {
            MsgRead handler = MsgReadEvent;
            if (handler != null) handler(this, args);
        }

        private bool local;

        public MessageDisplay(IEnumerable<Messages> msgs, string username)
        {
            InitializeComponent();
            this.username = username;
            unreadCount = 0;
            foreach (Messages msg in msgs)
            {
                AddMessage(msg);
            }
            panelMessageStats.BackColor = Color.DarkSlateBlue;
        }

        public int msgcount;

        private void UpdateMessageCount()
        {
            labelStatus.Text = unreadCount == 1 ? "1 new message." : unreadCount + " new messages.";
        }

        public MessageDisplay(bool local)
        {
            InitializeComponent();
            this.local = local;
            panelMessageStats.BackColor = Color.DarkSlateBlue;
        }

        private List<Messages> messages = new List<Messages>();

        public void AddMessages(List<Messages> msgs)
        {
            foreach (Messages msg in msgs)
            {
                AddMessage(msg);
            }
        }

        private Color unreadBackColor;
        private Color readBackColor;

        [Description("Sets the background color for unread messages")]
        public Color UnreadBackColor
        {
            get { return unreadBackColor; }
            set { unreadBackColor = value; }
        }

        [Description("Sets the background color for unread messages")]
        public Color ReadBackColor
        {
            get { return readBackColor; }
            set { readBackColor = value; }
        }

        public void AddMessage(Messages msg)
        {
            if (msg.Sender == username)
            {
                return;
            }
            if (msg.Unread)
            {
                msgcount++;
                unreadCount++;
                UpdateMessageCount();
            }
            messages.Add(msg);
            MessageItem mi = new MessageItem(msg, msg.SenderImage);
            mi.Dock = DockStyle.Top;
            mi.BackColor = msg.Unread ? UnreadBackColor : ReadBackColor;
            mi.Click += messageItem1_Click;
            flowPanel1.Controls.Add(mi);
            flowPanel1.Controls.SetChildIndex(mi, 1);
        }


        private void messageItem1_Click(object sender, EventArgs e)
        {
            buttonSend.Text = "&Reply";
            var messageItem = (MessageItem) sender;
            Messages msg = messageItem.message;
            pictureBoxTo.Image = messageItem.Picture;
            labelMsgDate.Text = msg.Date.ToLongDateString();
            try
            {
                richTextBoxMessage.Rtf = msg.Message;
            }
            catch (ArgumentException)
            {
                richTextBoxMessage.Text = msg.Message;
            }
            comboBoxTo.Text = msg.Sender;
            textBoxSubject.Text = msg.Subject;
            if (msg.Unread)
            {
                try
                {
                    if (Helper.Update("messages", new[] {"Unread"}, new[] {"N"}, new[] {"id"}, new[] {msg.Id}, "") > 0)
                    {
                        messageItem.BackColor = readBackColor;
                        unreadCount--;
                        OnMsgReadEvent(new MessageEvent(msg));
                        msg.Unread = false;
                        UpdateMessageCount();
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(ex);
                }
            }
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            if (buttonSend.Text == "&Compose Message" || buttonSend.Text == "&Reply")
            {
                richTextBoxMessage.ReadOnly = false;
                richTextBoxMessage.Clear();
                richTextBoxMessage.BackColor = Color.White;
                richTextBoxMessage.Focus();
                return;
            }
            if (comboBoxTo.Text == "")
            {
                MessageBox.Show("Please choose a recipient for your message.", "No Recipient Selected",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                comboBoxTo.Focus();
                return;
            }
            var rl = Helper.selectResults(new[] {"CONCAT_WS(' ', title, firstName, lastName) as lecturerName"},
                                           "lecturers",
                                           string.Format("WHERE username = '{0}'", comboBoxTo.Text));
            if (rl == null)
            {
                MessageBox.Show("A network error has occurred. Please try again.", "Message Sending Failed",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (rl.Length == 0)
            {
                MessageBox.Show(
                    "The user you are trying to send this message to does not exist. Please try again.",
                    "User Does not Exist", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            int insert = 0;
            try
            {
                insert = Helper.dbInsert("messages", new[] {"recipient", "sender", "message", "subject"},
                                         new[] {comboBoxTo.Text, username, richTextBoxMessage.Rtf, textBoxSubject.Text},
                                         true, true);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex);
                MessageBox.Show("An error occurred while trying to send the message. Please try again.",
                                "Message Send Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (insert > 0)
            {
                MessageBox.Show(string.Format("Your messsages has been sent successsfully to {0}.", rl[0][0]),
                                "Message Sent Successfully.",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);

                richTextBoxMessage.ReadOnly = true;
                richTextBoxMessage.BackColor = SystemColors.Control;
                richTextBoxMessage.Clear();
                buttonSend.Text = "&Compose Message";
            }
            else
            {
                MessageBox.Show(
                    "An error occurred while trying to send this message to the specfied user. Please try again.",
                    "Unable to Send Message", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void composeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            buttonSend.Text = "&Send";
        }

        private void replyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            buttonSend.Text = "&Reply";
        }

        private void viewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            messageItem1_Click(sender, e);
        }

        private void labelStatus_Click(object sender, EventArgs e)
        {

        }

        private void richTextBoxMessage_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (buttonSend.Text == "&Compose Message" || buttonSend.Text == "&Reply")
            {
                buttonSend.Text = "&Send";
            }
        }

        private void comboBoxTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBoxSubject.Clear();
            buttonSend.Text = "&Compose Message";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            if (local)
                return;

            if (Helper.GetUnreadMessageCount(username) > messages.Count)
            {
                var nmsgs = Helper.GetReceivedMessages(username);
                foreach (var msg in nmsgs)
                {
                    if (msg.IsIn(messages))
                    {
                        continue;
                    }
                    messages.Add(msg);
                    AddMessage(msg);
                    OnNewMsgEvent(new MessageEvent(msg));
                }
            }

            timer1.Enabled = true;
        }
    }

    public delegate void NewMessage(object sender, MessageEvent args);

    public delegate void MsgRead(object sender, MessageEvent args);

    public class MessageEvent : EventArgs
    {
        public MessageEvent(Messages msg)
        {
            this.msg = msg;
        }

        public Messages msg;
    }
}
