namespace ARMS
{
    partial class MessageUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageUser));
            this.labelTo = new System.Windows.Forms.Label();
            this.richTextBoxMsg = new System.Windows.Forms.RichTextBox();
            this.buttonSend = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelTo
            // 
            this.labelTo.BackColor = System.Drawing.Color.Black;
            this.labelTo.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelTo.Font = new System.Drawing.Font("Trebuchet MS", 16F, System.Drawing.FontStyle.Italic);
            this.labelTo.ForeColor = System.Drawing.Color.FloralWhite;
            this.labelTo.Location = new System.Drawing.Point(0, 0);
            this.labelTo.Name = "labelTo";
            this.labelTo.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.labelTo.Size = new System.Drawing.Size(563, 44);
            this.labelTo.TabIndex = 19;
            this.labelTo.Text = "Leave a message for ";
            this.labelTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // richTextBoxMsg
            // 
            this.richTextBoxMsg.Font = new System.Drawing.Font("Trebuchet MS", 11F);
            this.richTextBoxMsg.Location = new System.Drawing.Point(12, 57);
            this.richTextBoxMsg.Name = "richTextBoxMsg";
            this.richTextBoxMsg.Size = new System.Drawing.Size(538, 210);
            this.richTextBoxMsg.TabIndex = 20;
            this.richTextBoxMsg.Text = "";
            // 
            // buttonSend
            // 
            this.buttonSend.Location = new System.Drawing.Point(448, 273);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(103, 37);
            this.buttonSend.TabIndex = 21;
            this.buttonSend.Text = "Send";
            this.buttonSend.UseVisualStyleBackColor = true;
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(339, 273);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(103, 37);
            this.buttonCancel.TabIndex = 21;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // MesssageUser
            // 
            this.AcceptButton = this.buttonSend;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FloralWhite;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(563, 322);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSend);
            this.Controls.Add(this.richTextBoxMsg);
            this.Controls.Add(this.labelTo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MesssageUser";
            this.Text = "MesssageUser";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelTo;
        private System.Windows.Forms.RichTextBox richTextBoxMsg;
        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.Button buttonCancel;
    }
}
