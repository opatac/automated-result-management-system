using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ARMS
{
    /// <summary>
    /// Form to simply leave a message for a particular user, presumably a student/lecturer.
    /// Originally built for the Exam Officer module
    /// </summary>
    public partial class MessageUser : Form
    {
        /// <summary>
        /// The default public constructor for the component
        /// </summary>
        public MessageUser()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The username of the message sender (The person using the form)
        /// </summary>
        public string Sender;

        /// <summary>
        /// The username of the message recipient
        /// </summary>
        public string Recipient;

        public MessageUser(string sender, string recipient, string name)
        {
            InitializeComponent();
            labelTo.Text += name;
            this.Sender = sender;
            this.Recipient = recipient;
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            var rl = Helper.selectResults(new[] { "CONCAT_WS(' ', title, firstName, lastName) as lecturerName" },
                               "lecturers",
                               string.Format("WHERE Sender = '{0}'", Recipient));
            if (rl == null)
            {
                MessageBox.Show("A network error has occurred. Please try again.", "Message Sending Failed",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (rl.Length == 0)
            {
                MessageBox.Show(
                    "The user you are trying to send this message to does not exist. Please try again.",
                    "User Does not Exist", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            if (Helper.dbInsert("messages", new[] { "recipient", "sender", "message", "subject" }, new[] { Recipient, Sender, richTextBoxMsg.Rtf, "Urgent Message" }, true, true) > 0)
            {
                MessageBox.Show(string.Format("Your messsages has been sent successsfully to {0}.", rl[0][0]),
                                "Message Sent Successfully.",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);

                buttonSend.Text = "&Compose Message";
            }
            else
            {
                MessageBox.Show(
                    "An error occurred while trying to send this message to the specfied user. Please try again.",
                    "Unable to Send Message", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }
    }
}
