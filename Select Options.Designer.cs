namespace ARMS
{
    partial class Select_Options
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Select_Options));
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonProceed = new System.Windows.Forms.Button();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBox1.CheckOnClick = true;
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(0, 2);
            this.checkedListBox1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(345, 274);
            this.checkedListBox1.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonProceed);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 279);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(345, 67);
            this.panel1.TabIndex = 8;
            // 
            // buttonProceed
            // 
            this.buttonProceed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonProceed.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonProceed.ImageKey = "etips.png";
            this.buttonProceed.ImageList = this.imageList2;
            this.buttonProceed.Location = new System.Drawing.Point(222, 14);
            this.buttonProceed.Name = "buttonProceed";
            this.buttonProceed.Padding = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.buttonProceed.Size = new System.Drawing.Size(111, 41);
            this.buttonProceed.TabIndex = 9;
            this.buttonProceed.Text = "&Proceed";
            this.buttonProceed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonProceed.UseVisualStyleBackColor = true;
            this.buttonProceed.Click += new System.EventHandler(this.buttonProceed_Click);
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "etips.png");
            // 
            // Select_Options
            // 
            this.AcceptButton = this.buttonProceed;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(345, 342);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.checkedListBox1);
            this.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.MaximumSize = new System.Drawing.Size(361, 380);
            this.MinimumSize = new System.Drawing.Size(361, 380);
            this.Name = "Select_Options";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Choose Options";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonProceed;
        private System.Windows.Forms.ImageList imageList2;
    }
}
