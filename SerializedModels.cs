using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ARMS
{
    [Serializable]
    [XmlInclude(typeof (LecturerDetails))]
    [XmlInclude(typeof (Activity))]
    [XmlInclude(typeof (Messages))]
    [XmlInclude(typeof (StudentData))]
    [XmlInclude(typeof (LecturerCourse))]
    public class UserDetails
    {
        public DateTime lastUpdate;
        public LecturerDetails Lecturer;

        public SerializableDictionary<string, LecturerCourse> Courses =
            new SerializableDictionary<string, LecturerCourse>();

        public List<string> Announcements = new List<string>();
        public List<Activity> Activities = new List<Activity>();

        public SerializableDictionary<string, List<StudentData>> CourseData =
            new SerializableDictionary<string, List<StudentData>>();

        public List<Messages> Messages = new List<Messages>();
        public SerializableDictionary<string, string[]> CourseDetails = new SerializableDictionary<string, string[]>();
        public SerializableDictionary<string, string> CourseDuration = new SerializableDictionary<string, string>();
        public UserDetails User;
    }

    [XmlRoot("dictionary")]
    public class SerializableDictionary<TKey, TValue>
        : Dictionary<TKey, TValue>, IXmlSerializable
    {
        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            XmlSerializer keySerializer = new XmlSerializer(typeof (TKey));
            XmlSerializer valueSerializer = new XmlSerializer(typeof (TValue));

            bool wasEmpty = reader.IsEmptyElement;
            reader.Read();

            if (wasEmpty)
                return;

            while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
            {
                reader.ReadStartElement("item");

                reader.ReadStartElement("key");
                TKey key = (TKey) keySerializer.Deserialize(reader);
                reader.ReadEndElement();

                reader.ReadStartElement("value");
                TValue value = (TValue) valueSerializer.Deserialize(reader);
                reader.ReadEndElement();

                this.Add(key, value);

                reader.ReadEndElement();
                reader.MoveToContent();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            XmlSerializer keySerializer = new XmlSerializer(typeof (TKey));
            XmlSerializer valueSerializer = new XmlSerializer(typeof (TValue));

            foreach (TKey key in this.Keys)
            {
                writer.WriteStartElement("item");

                writer.WriteStartElement("key");
                keySerializer.Serialize(writer, key);
                writer.WriteEndElement();

                writer.WriteStartElement("value");
                TValue value = this[key];
                valueSerializer.Serialize(writer, value);
                writer.WriteEndElement();

                writer.WriteEndElement();
            }
        }

        #endregion
    }

    [Serializable]
    public class User
    {
        public string _username;
        public string _password;
        public string _gender;
        public string _secret;
        public string _nicename;
        public string[] _roles;

        [XmlIgnore]
        public string Username
        {
            get { return Crypto.Decrypt(_username); }
            set { _username = Crypto.Encrypt(value); }
        }

        [XmlIgnore]
        public string Password
        {
            get { return Crypto.Decrypt(_password); }
            set { _password = Crypto.Encrypt(value); }
        }

        [XmlIgnore]
        public string[] Roles
        {
            get
            {
                var _troles = new string[_roles.Length];
                for (int i = 0; i < _roles.Length; i++)
                {
                    _troles[i] = Crypto.Decrypt(_roles[i]);
                }
                return _troles;
            }
            set
            {
                _roles = new string[value.Length];
                for (int i = 0; i < value.Length; i++)
                {
                    _roles[i] = Crypto.Encrypt(value[i]);
                }
            }
        }

        [XmlIgnore]
        public string Secret
        {
            get { return Crypto.Decrypt(_secret); }
            set { _secret = Crypto.Encrypt(value); }
        }

        [XmlIgnore]
        public string Gender
        {
            get { return Crypto.Decrypt(_gender); }
            set { _gender = Crypto.Encrypt(value); }
        }

        [XmlIgnore]
        public string Nicename
        {
            get { return Crypto.Decrypt(_nicename); }
            set { _nicename = Crypto.Encrypt(value); }
        }
    }
}
