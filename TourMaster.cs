using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ARMS
{
    /// <summary>
    /// Main class builty for introducing users to the application. The Tour Master should be able to introduce
    ///  users to learn how to use the application the first time the application is launched
    /// </summary>
    public partial class TourMaster : Form
    {
        /// <summary>
        /// Public constructor for the TourMaster class 
        /// </summary>
        public TourMaster()
        {
            InitializeComponent();
        }
    }
}
