using System;
using System.Collections.Generic;
using System.Drawing;
using System.Media;
using System.Windows.Forms;

namespace ARMS
{
    public partial class homeDept : Form
    {
        private DeptDetails dd;
        private string username;
        private bool local;

        public homeDept(DeptDetails dd, string formalName, string username, bool local)
        {
            InitializeComponent();
            labelWelcome.Text = string.Format("Good day {0}.", formalName);
            this.local = local;
            labelMode.Text = local ? "Local Mode" : "Online";
            this.dd = dd;
            this.username = username;
            this.MouseMove += app_MouseMove;
        }

        void app_MouseMove(object sender, MouseEventArgs e)
        {
            timerCount = 0;
        }

        private string[] cRow;
        private List<string[]> srows = new List<string[]>();
        private Dictionary<string, LecturerDetails> allLecturers= new Dictionary<string, LecturerDetails>();
        private Dictionary<string, Course> courses =  new Dictionary<string, Course>();
        private LecturerDetails currentLecturer;
        private int cIndex;
        private string lastCourseCell;
        private Dictionary<string, Course> changedCourses = new Dictionary<string, Course>();
        //old value, new value
        private Dictionary<string, string> changedCodes = new Dictionary<string, string>();
        private int announceIndex;
        private List<string> announcements = new List<string>();

        private void comboBoxLecturer1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dataGridViewCourses_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                lastCourseCell = dataGridViewCourses.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            }
            catch{}
        }

        private void dataGridViewCourses_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            var nRow = dataGridViewCourses.Rows[e.RowIndex];
            cIndex = e.ColumnIndex;
            try
            {
                if (nRow.Cells[cIndex].Value.ToString() != lastCourseCell)
                {
                    string key = nRow.Cells["CourseCode"].Value.ToString();
                    string cname = dataGridViewCourses.Columns[cIndex].Name;
                    if (!changedCourses.ContainsKey(key))
                    {
                        if (cname == "CourseCode")
                        {
                            changedCodes.Add(lastCourseCell, key);
                            changedCourses.Add(key, new Course {CourseCode = key});
                            return;
                        }
                        changedCourses.Add(key, new Course {CourseCode = key});
                    }
                    var cc = changedCourses[key];
                    switch (cname)
                    {
                        case "CourseCode":
                            cc.CourseCode = nRow.Cells["CourseCode"].Value.ToString();
                            changedCodes.Add(lastCourseCell, key);
                            break;
                        case "CourseTitle":
                            cc.CourseTitle = nRow.Cells["CourseTitle"].Value.ToString();
                            break;
                        case "semester":
                            cc.Semester = (Semester) Convert.ToInt32(nRow.Cells["semester"].Value.ToString());
                            break;
                        case "units":
                            cc.Units = Convert.ToUInt16(nRow.Cells["units"].Value.ToString());
                            break;

                    }
                    nRow.DefaultCellStyle.ForeColor = Color.Red;
                    buttonSaveCourses.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                dataGridViewCourses.CancelEdit();
                Logger.WriteLog(ex.Source + ex.Message, ex.StackTrace);
                MessageBox.Show("It seems there was an error in your input. Please try again.", "Incorrect Input",
                                MessageBoxButtons.OK);
            }
        }

        private void buttonEditCoures_Click(object sender, EventArgs e)
        {
            var selectedRows = dataGridViewCourses.SelectedRows;
            if (selectedRows.Count == 0) return;
            var cell = selectedRows[0].Cells[0];
            dataGridViewCourses.CurrentCell = cell;
            dataGridViewCourses.BeginEdit(false);
        }

        private void buttonSaveCourses_Click(object sender, EventArgs e)
        {
            List<string> doneCodes = new List<string>();
            foreach (var s in changedCodes)
            {
                string query = string.Format("UPDATE course set CourseCode='{1}' WHERE CourseCode='{0}'", s.Key, s.Value);

                int result = Helper.ExecuteNonQuery(query);
                if (result == -1)
                {
                    while (result == -1)
                    {
                        var dr = MessageBox.Show("The operation has failed. Do you want to try again?",
                                                 s.Key + " Edit Failed",
                                                 MessageBoxButtons.RetryCancel);
                        if (dr == DialogResult.Cancel) break;
                        result = Helper.ExecuteNonQuery(query);
                    }
                }
                else
                {
                    string log = string.Format(" changed the CourseCode for {0} to {1}", s.Key, s.Value);
                    Helper.logActivity(new Activity
                    {
                        Department = dd.DeptName,
                        Faculty = dd.Faculty,
                        Text = log,
                        Username = username
                    });
                    doneCodes.Add(s.Key);
                }

            }
            foreach (string s in doneCodes)
            {
                changedCodes.Remove(s);
            }
            List<string> doneKeys = new List<string>();
            foreach (var c in changedCourses)
            {
                string query = "UPDATE courses SET ";
                query += c.Value.CourseTitle == null ? "" : string.Format("CourseTitle='{0}'", c.Value.CourseTitle);
                query += c.Value.Semester == 0 ? "" : string.Format(", semester='{0}'", c.Value.Semester);
                query += c.Value.Units == 0 ? "" : string.Format(", units='{0}'", c.Value.Units);
                if (query == "UPDATE courses SET ") return;
                query += string.Format(" WHERE CourseCode='{0}'", c.Key);
                int result = Helper.ExecuteNonQuery(query);
                if (result == -1)
                {
                    while (result == -1)
                    {
                        var dr = MessageBox.Show("The operation has failed. Do you want to try again?",
                                                 c.Key + " Edit Failed",
                                                 MessageBoxButtons.RetryCancel);
                        if (dr == DialogResult.Cancel) break;
                        result = Helper.ExecuteNonQuery(query);
                    }
                }
                else
                {
                    string log = string.Format(" changed the {0} {1} {2} for {3}",
                                               c.Value.CourseTitle == null ? "" : string.Format("CourseTitle (New value: {0}), ", c.Value.CourseTitle),
                                               c.Value.Semester == 0 ? "" : string.Format("Semester (New value: {0}), ", c.Value.Semester),
                                               c.Value.Units == 0 ? "" : string.Format("Unit Load (New value: {0}), ", c.Value.Units), c.Key);
                    Helper.logActivity(new Activity
                    {
                        Department = dd.DeptName,
                        Faculty = dd.Faculty,
                        Text = log,
                        Username = username
                    });
                    doneKeys.Add(c.Key);
                }
            }
            foreach (DataGridViewRow row in dataGridViewCourses.Rows)
            {
                if (doneKeys.Contains(row.Cells["CourseCode"].Value.ToString()))
                {
                    row.DefaultCellStyle.ForeColor = Color.Black;
                }
            }
            foreach (string s in doneKeys)
            {
                changedCourses.Remove(s);
            }

            if (changedCourses.Count == 0)
            {
                buttonSaveCourses.Enabled = false;
                MessageBox.Show("All relevant changes has been made successfully.", "Course Edit Successful",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show(
                    "Relevant changes has been made successfully. However some of the courses were not updated successfully. You can try to update these ones again by clicking Save.",
                    "Course Edit Complete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void homeDept_Load(object sender, EventArgs e)
        {
            tt = new Timer{Interval = 100, Enabled = true};
            tt.Tick += LoadForm;
        }

        private Timer tt;

        private void LoadForm(object state, EventArgs eventArgs)
        {
            tt.Enabled = false;
            //Load Courses
            foreach (
                string[] s in
                    Helper.selectResults(new[] {"CourseCode", "CourseTitle", "units", "semester"}, "courses",
                                          new[] {"department"}, new[] {dd.DeptName}, "")
                )
            {
                try
                {
                    comboBoxCourse.Items.Add(s[0]);
                    dataGridViewCourses.Rows.Add(s);
                    courses.Add(s[0],
                                new Course
                                    {
                                        CourseCode = s[0],
                                        CourseTitle = s[1],
                                        Units = Convert.ToUInt16(s[2]),
                                        Semester = (Semester) Convert.ToInt32(s[3])
                                    });
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(ex.Message + Environment.NewLine + ex.StackTrace,
                                            DateTime.Now.ToShortDateString());
                }
            }


            //Load Lecturers
            int count = 0;
            foreach (
                string[] s in
                    Helper.selectResults(new[] {"lecturers.username as username", "title", "lastName", "firstName", "email", "staffCode"},
                                          "lecturers",
                                          new[] { "department", "deleted" }, new[] { dd.DeptName, "N" }, "JOIN users ON lecturers.username = users.username")
                )
            {
                count++;
                LecturerDetails ld = new LecturerDetails()
                    {
                        Department = dd.DeptName,
                        Faculty = dd.Faculty,
                        UserName = s[0],
                        Title = s[1],
                        LastName = s[2],
                        FirstName = s[3],
                        Email = s[4],
                        StaffCode = s[5]
                    };
                var lname = string.Format("{0} {1} {2}", ld.Title, ld.LastName, ld.FirstName);
                comboBoxLecturer1.Items.Add(lname);
                allLecturers.Add(lname, ld);
            }
            LoadActivities(false);
            LoadAnnouncements(false);
        }

        private List<Activity> acts = new List<Activity>();

        private void LoadActivities(bool notify)
        {
            List<Activity> acs = Helper.GetActivities("department", dd.DeptName);
            foreach (var ac in acs)
            {
                if (ac.IsIn(acts))
                {
                    continue;
                }
                acts.Add(ac);
                dataGridActivity.Rows.Add(new[] { ac.ActivityDate.ToShortDateString(), ac.Text });
                if (notify)
                {
                    notifyIcon1.ShowBalloonTip(3000, "New Activity", ac.Text, ToolTipIcon.Info);
                }
            }
        }

        private void LoadAnnouncements(bool notify)
        {
            foreach (var an in Helper.GetAnnouncements("department", currentLecturer.Department, UserRoles.DepartmentAdmin))
            {
                if (announcements.Contains(an))
                {
                    continue;
                }
                announcements.Add(an);
                if (notify)
                {
                    notifyIcon1.ShowBalloonTip(3000, "New Annoucement Received", an.StripRTF().Excerpt(40), ToolTipIcon.Info);;
                }
            }
            if (announcements.Count == 0)
            {
                buttonPreviousAnnounce.Enabled = false;
            }
            else
            {
                announceIndex = announcements.Count - 1;
                try
                {
                    richTextBoxAnnouncements.Rtf = announcements[announceIndex];
                }
                catch (ArgumentException)
                {
                    richTextBoxAnnouncements.Text = announcements[announceIndex].StripRTF();
                }
                buttonPreviousAnnounce.Enabled = announcements.Count > 1;
            }
            buttonNextAnnounce.Enabled = false;
        }

        private void buttonAssignCourse_Click_1(object sender, EventArgs e)
        {
            try
            {
                //Check if course is already assigned to lecturer
                var results = Helper.selectResults(new[] { "username", "CourseCode" }, "lecturer_courses",
                                                    new[] { "username", "CourseCode" },
                                                    new[] { currentLecturer.UserName, checkedListDepartments.Value }, "");
                //Did an error occur?
                if (results == null)
                {
                    MessageBox.Show("An error occured while trying to assign the course. Please try again.");
                    return;
                }
                if (results.Length > 0)
                {
                    MessageBox.Show(string.Format("This course is already assigned to {0}.", comboBoxLecturer1.Text));
                    return;
                }
                var di = Helper.dbInsert("lecturer_courses", new[] { "username", "CourseCode", "departments" },
                                          new[]
                                              {
                                                  currentLecturer.UserName, comboBoxLecturer1.Text,
                                                  checkedListDepartments.Value
                                              });
                MessageBox.Show("The staff was registered successfully.", "Automated Result Management System", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                if (di == -1)
                {
                    MessageBox.Show(
                        string.Format("There was an error assigning {0} to {1}. Please try again.", comboBoxCourse.Text,
                                      comboBoxLecturer1.Text), "Automated Result Management System", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
            catch (Exception)
            {
                MessageBox.Show(
                    string.Format(
                        "There was an error assigning {0} to {1}. Please contact the admin if this continues.",
                        comboBoxCourse.Text, comboBoxLecturer1.Text), "Automated Result Management System", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Announcer an = new Announcer(username, dd.Faculty, dd.DeptName);
            an.ShowDialog();
        }

        private void btnLock_Click(object sender, EventArgs e)
        {
            Locked lc = new Locked(username);
            Visible = false;
            lc.UnlockEvent += (o, args) => { Visible = args.Unlocked; timerCount = 0; };
            lc.ShowDialog();
        }

        private int timerCount;

        /// <summary>
        /// The amount of time to wait before session is locked. Since timer runs every 3 seconds, this means 180 seconds or three minutes
        /// </summary>
        private int timerTime = 60;

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            if (announcements.Count != Helper.GetAnnouncementCount("department", dd.DeptName, UserRoles.DepartmentAdmin))
            {
                LoadAnnouncements(true);
            }
            if (acts.Count != Helper.GetActivitiesCount("department", dd.DeptName))
            {
                LoadActivities(true);
            }
            timerCount++;
            if (timerCount == timerTime)
            {
                btnLock.PerformClick();
            }

            timer1.Enabled = true;
        }

        private void notifyIcon1_BalloonTipClicked(object sender, EventArgs e)
        {
            this.tabControl1.SelectTab(0);
            this.BringToFront();
            this.Focus();
            if (WindowState == FormWindowState.Minimized)
            {
                WindowState = FormWindowState.Normal;
            }
        }

        private SoundPlayer sp;

        private void notifyIcon1_BalloonTipShown(object sender, EventArgs e)
        {
            sp = sp ?? new SoundPlayer();
            sp.Stream = Properties.Resources._2cool;
            sp.Play();
        }
    }
}
