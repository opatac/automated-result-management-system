﻿namespace ARMS
{
    partial class homeExams
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(homeExams));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.radioLevel600 = new System.Windows.Forms.RadioButton();
            this.buttonGenerateReports = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.radioBroadSheet = new System.Windows.Forms.RadioButton();
            this.radioFacultySummary = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioSemester2 = new System.Windows.Forms.RadioButton();
            this.radioSemester1 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioLevel800 = new System.Windows.Forms.RadioButton();
            this.radioLevel700 = new System.Windows.Forms.RadioButton();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.radioLevel500 = new System.Windows.Forms.RadioButton();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.fbd = new System.Windows.Forms.FolderBrowserDialog();
            this.sfd = new System.Windows.Forms.SaveFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolBtnAbout = new System.Windows.Forms.ToolStripButton();
            this.toolButtonLock = new System.Windows.Forms.ToolStripButton();
            this.btnAnnouncer = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolComboSemester = new System.Windows.Forms.ToolStripComboBox();
            this.toolTextAcademicYear = new System.Windows.Forms.ToolStripTextBox();
            this.toolLevelChooser = new System.Windows.Forms.ToolStripComboBox();
            this.labelMode = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.contactLecturerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leaveAMessageForThisStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.radioLevel400 = new System.Windows.Forms.RadioButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.activityText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridActivity = new System.Windows.Forms.DataGridView();
            this.activityDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBoxActivity = new System.Windows.Forms.GroupBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.tabPageHome = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageReports = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxDepartment = new System.Windows.Forms.ComboBox();
            this.comboBoxFaculty = new System.Windows.Forms.ComboBox();
            this.buttonView = new System.Windows.Forms.Button();
            this.numericUpDownSession = new System.Windows.Forms.NumericUpDown();
            this.labelCurrentSession = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBoxLevel = new System.Windows.Forms.GroupBox();
            this.radioLevel300 = new System.Windows.Forms.RadioButton();
            this.radioLevel200 = new System.Windows.Forms.RadioButton();
            this.radioLevel100 = new System.Windows.Forms.RadioButton();
            this.tabPageRegistration = new System.Windows.Forms.TabPage();
            this.listViewStudents = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.myProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.progressLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tabPageHODResults = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelApproved = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.studentsData = new System.Windows.Forms.DataGridView();
            this.StudentSn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StudentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RegNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExamScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBoxResultSummary = new System.Windows.Forms.GroupBox();
            this.tableDetails = new System.Windows.Forms.TableLayoutPanel();
            this.labelNR = new System.Windows.Forms.Label();
            this.labelA = new System.Windows.Forms.Label();
            this.labelAR = new System.Windows.Forms.Label();
            this.labelB = new System.Windows.Forms.Label();
            this.labelF = new System.Windows.Forms.Label();
            this.labelC = new System.Windows.Forms.Label();
            this.labelE = new System.Windows.Forms.Label();
            this.labelD = new System.Windows.Forms.Label();
            this.btnReject = new ARMS.RibbonMenuButton();
            this.btnApprove = new ARMS.RibbonMenuButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxDepartments = new System.Windows.Forms.ComboBox();
            this.comboCourse = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridActivity)).BeginInit();
            this.groupBoxActivity.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tabPageHome.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageReports.SuspendLayout();
            this.panel8.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSession)).BeginInit();
            this.groupBoxLevel.SuspendLayout();
            this.tabPageRegistration.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tabPageHODResults.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.studentsData)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBoxResultSummary.SuspendLayout();
            this.tableDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioLevel600
            // 
            this.radioLevel600.AutoSize = true;
            this.radioLevel600.Location = new System.Drawing.Point(13, 168);
            this.radioLevel600.Name = "radioLevel600";
            this.radioLevel600.Size = new System.Drawing.Size(84, 22);
            this.radioLevel600.TabIndex = 8;
            this.radioLevel600.TabStop = true;
            this.radioLevel600.Text = "600 Level";
            this.radioLevel600.UseVisualStyleBackColor = true;
            // 
            // buttonGenerateReports
            // 
            this.buttonGenerateReports.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGenerateReports.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonGenerateReports.ImageKey = "APPLY.PNG";
            this.buttonGenerateReports.ImageList = this.imageList1;
            this.buttonGenerateReports.Location = new System.Drawing.Point(583, 352);
            this.buttonGenerateReports.Name = "buttonGenerateReports";
            this.buttonGenerateReports.Padding = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.buttonGenerateReports.Size = new System.Drawing.Size(127, 40);
            this.buttonGenerateReports.TabIndex = 3;
            this.buttonGenerateReports.Text = "&Generate";
            this.buttonGenerateReports.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonGenerateReports.UseVisualStyleBackColor = true;
            this.buttonGenerateReports.Click += new System.EventHandler(this.buttonGenerateReports_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "APPLY.PNG");
            // 
            // radioBroadSheet
            // 
            this.radioBroadSheet.AutoSize = true;
            this.radioBroadSheet.Location = new System.Drawing.Point(19, 56);
            this.radioBroadSheet.Name = "radioBroadSheet";
            this.radioBroadSheet.Size = new System.Drawing.Size(160, 22);
            this.radioBroadSheet.TabIndex = 8;
            this.radioBroadSheet.TabStop = true;
            this.radioBroadSheet.Text = "Broad Sheet (Form C)";
            this.radioBroadSheet.UseVisualStyleBackColor = true;
            // 
            // radioFacultySummary
            // 
            this.radioFacultySummary.AutoSize = true;
            this.radioFacultySummary.Location = new System.Drawing.Point(19, 28);
            this.radioFacultySummary.Name = "radioFacultySummary";
            this.radioFacultySummary.Size = new System.Drawing.Size(244, 22);
            this.radioFacultySummary.TabIndex = 8;
            this.radioFacultySummary.TabStop = true;
            this.radioFacultySummary.Text = "Faculty Summary Result Evaluation";
            this.radioFacultySummary.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioBroadSheet);
            this.groupBox1.Controls.Add(this.radioFacultySummary);
            this.groupBox1.Location = new System.Drawing.Point(344, 239);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(366, 92);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Choose Template to Generate";
            // 
            // radioSemester2
            // 
            this.radioSemester2.AutoSize = true;
            this.radioSemester2.Location = new System.Drawing.Point(19, 56);
            this.radioSemester2.Name = "radioSemester2";
            this.radioSemester2.Size = new System.Drawing.Size(76, 22);
            this.radioSemester2.TabIndex = 8;
            this.radioSemester2.TabStop = true;
            this.radioSemester2.Text = "SECOND";
            this.radioSemester2.UseVisualStyleBackColor = true;
            // 
            // radioSemester1
            // 
            this.radioSemester1.AutoSize = true;
            this.radioSemester1.Location = new System.Drawing.Point(19, 28);
            this.radioSemester1.Name = "radioSemester1";
            this.radioSemester1.Size = new System.Drawing.Size(60, 22);
            this.radioSemester1.TabIndex = 8;
            this.radioSemester1.TabStop = true;
            this.radioSemester1.Text = "FIRST";
            this.radioSemester1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioSemester2);
            this.groupBox2.Controls.Add(this.radioSemester1);
            this.groupBox2.Location = new System.Drawing.Point(344, 121);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(213, 91);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Select Semester";
            // 
            // radioLevel800
            // 
            this.radioLevel800.AutoSize = true;
            this.radioLevel800.Location = new System.Drawing.Point(13, 224);
            this.radioLevel800.Name = "radioLevel800";
            this.radioLevel800.Size = new System.Drawing.Size(84, 22);
            this.radioLevel800.TabIndex = 8;
            this.radioLevel800.TabStop = true;
            this.radioLevel800.Text = "800 Level";
            this.radioLevel800.UseVisualStyleBackColor = true;
            // 
            // radioLevel700
            // 
            this.radioLevel700.AutoSize = true;
            this.radioLevel700.Location = new System.Drawing.Point(13, 196);
            this.radioLevel700.Name = "radioLevel700";
            this.radioLevel700.Size = new System.Drawing.Size(84, 22);
            this.radioLevel700.TabIndex = 8;
            this.radioLevel700.TabStop = true;
            this.radioLevel700.Text = "700 Level";
            this.radioLevel700.UseVisualStyleBackColor = true;
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "build.png");
            this.imageList2.Images.SetKeyName(1, "print_48.png");
            this.imageList2.Images.SetKeyName(2, "Delete.png");
            this.imageList2.Images.SetKeyName(3, "MyDocuments.png");
            this.imageList2.Images.SetKeyName(4, "Gloss PNGKKMenu_Office.png");
            // 
            // radioLevel500
            // 
            this.radioLevel500.AutoSize = true;
            this.radioLevel500.Location = new System.Drawing.Point(13, 140);
            this.radioLevel500.Name = "radioLevel500";
            this.radioLevel500.Size = new System.Drawing.Size(84, 22);
            this.radioLevel500.TabIndex = 8;
            this.radioLevel500.TabStop = true;
            this.radioLevel500.Text = "500 Level";
            this.radioLevel500.UseVisualStyleBackColor = true;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipText = "A robust result management solution for Educational Institutions";
            this.notifyIcon1.BalloonTipTitle = "Automated Result Management System 1.0";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Visible = true;
            // 
            // fbd
            // 
            this.fbd.Description = "Choose a folder to save the transcripts to or create a new folder.";
            this.fbd.SelectedPath = "C:\\";
            // 
            // sfd
            // 
            this.sfd.DefaultExt = "xlsx";
            this.sfd.Filter = "Microsoft Excel Worksheet|*.xlsx|Microsoft Excel Macro-Enabled Worksheet|*.xlsm|M" +
    "icrosoft Word Document|*.docx";
            this.sfd.InitialDirectory = "C:\\";
            this.sfd.RestoreDirectory = true;
            // 
            // toolBtnAbout
            // 
            this.toolBtnAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnAbout.Image = global::ARMS.Properties.Resources.arms128;
            this.toolBtnAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnAbout.Name = "toolBtnAbout";
            this.toolBtnAbout.Size = new System.Drawing.Size(52, 54);
            this.toolBtnAbout.Text = "About Automated Result Management System";
            // 
            // toolButtonLock
            // 
            this.toolButtonLock.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonLock.Image = global::ARMS.Properties.Resources.Lock;
            this.toolButtonLock.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolButtonLock.Name = "toolButtonLock";
            this.toolButtonLock.Size = new System.Drawing.Size(52, 54);
            this.toolButtonLock.Text = "Lock Session";
            // 
            // btnAnnouncer
            // 
            this.btnAnnouncer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAnnouncer.Image = global::ARMS.Properties.Resources.announcements;
            this.btnAnnouncer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAnnouncer.Name = "btnAnnouncer";
            this.btnAnnouncer.Size = new System.Drawing.Size(52, 54);
            this.btnAnnouncer.Text = "New Announcement";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 57);
            // 
            // toolComboSemester
            // 
            this.toolComboSemester.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.toolComboSemester.Items.AddRange(new object[] {
            "FIRST",
            "SECOND"});
            this.toolComboSemester.Name = "toolComboSemester";
            this.toolComboSemester.Padding = new System.Windows.Forms.Padding(3);
            this.toolComboSemester.Size = new System.Drawing.Size(86, 57);
            this.toolComboSemester.Text = "Semester";
            // 
            // toolTextAcademicYear
            // 
            this.toolTextAcademicYear.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.toolTextAcademicYear.Name = "toolTextAcademicYear";
            this.toolTextAcademicYear.Padding = new System.Windows.Forms.Padding(3);
            this.toolTextAcademicYear.Size = new System.Drawing.Size(80, 57);
            this.toolTextAcademicYear.Text = "2012/2013";
            // 
            // toolLevelChooser
            // 
            this.toolLevelChooser.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.toolLevelChooser.Items.AddRange(new object[] {
            "100",
            "200",
            "300",
            "400",
            "500",
            "600",
            "700",
            "800"});
            this.toolLevelChooser.Name = "toolLevelChooser";
            this.toolLevelChooser.Padding = new System.Windows.Forms.Padding(3);
            this.toolLevelChooser.Size = new System.Drawing.Size(116, 57);
            this.toolLevelChooser.Text = "Choose Level";
            // 
            // labelMode
            // 
            this.labelMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.labelMode.ForeColor = System.Drawing.Color.Red;
            this.labelMode.Location = new System.Drawing.Point(580, 12);
            this.labelMode.Name = "labelMode";
            this.labelMode.Size = new System.Drawing.Size(176, 39);
            this.labelMode.TabIndex = 3;
            this.labelMode.Text = "ONLINE";
            this.labelMode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SkyBlue;
            this.panel2.Controls.Add(this.labelMode);
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(768, 62);
            this.panel2.TabIndex = 9;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Font = new System.Drawing.Font("Trebuchet MS", 11F);
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolLevelChooser,
            this.toolTextAcademicYear,
            this.toolComboSemester,
            this.toolStripSeparator2,
            this.btnAnnouncer,
            this.toolButtonLock,
            this.toolBtnAbout});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(3, 2);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(519, 57);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // contactLecturerToolStripMenuItem
            // 
            this.contactLecturerToolStripMenuItem.Name = "contactLecturerToolStripMenuItem";
            this.contactLecturerToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.contactLecturerToolStripMenuItem.Text = "Contact Lecturer for this course";
            // 
            // leaveAMessageForThisStudentToolStripMenuItem
            // 
            this.leaveAMessageForThisStudentToolStripMenuItem.Name = "leaveAMessageForThisStudentToolStripMenuItem";
            this.leaveAMessageForThisStudentToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.leaveAMessageForThisStudentToolStripMenuItem.Text = "Leave a message for this student";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.leaveAMessageForThisStudentToolStripMenuItem,
            this.contactLecturerToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(246, 48);
            // 
            // radioLevel400
            // 
            this.radioLevel400.AutoSize = true;
            this.radioLevel400.Location = new System.Drawing.Point(13, 112);
            this.radioLevel400.Name = "radioLevel400";
            this.radioLevel400.Size = new System.Drawing.Size(84, 22);
            this.radioLevel400.TabIndex = 8;
            this.radioLevel400.TabStop = true;
            this.radioLevel400.Text = "400 Level";
            this.radioLevel400.UseVisualStyleBackColor = true;
            // 
            // timer1
            // 
            this.timer1.Interval = 3000;
            // 
            // activityText
            // 
            this.activityText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.activityText.HeaderText = "Details";
            this.activityText.Name = "activityText";
            this.activityText.ReadOnly = true;
            // 
            // dataGridActivity
            // 
            this.dataGridActivity.AllowUserToAddRows = false;
            this.dataGridActivity.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridActivity.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridActivity.BackgroundColor = System.Drawing.Color.White;
            this.dataGridActivity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridActivity.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dataGridActivity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridActivity.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.activityDate,
            this.activityText});
            this.dataGridActivity.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridActivity.GridColor = System.Drawing.Color.White;
            this.dataGridActivity.Location = new System.Drawing.Point(8, 22);
            this.dataGridActivity.Name = "dataGridActivity";
            this.dataGridActivity.ReadOnly = true;
            this.dataGridActivity.Size = new System.Drawing.Size(723, 382);
            this.dataGridActivity.TabIndex = 1;
            // 
            // activityDate
            // 
            this.activityDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.activityDate.HeaderText = "Date";
            this.activityDate.Name = "activityDate";
            this.activityDate.ReadOnly = true;
            this.activityDate.Width = 120;
            // 
            // groupBoxActivity
            // 
            this.groupBoxActivity.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxActivity.Controls.Add(this.dataGridActivity);
            this.groupBoxActivity.Location = new System.Drawing.Point(0, 7);
            this.groupBoxActivity.Name = "groupBoxActivity";
            this.groupBoxActivity.Size = new System.Drawing.Size(737, 410);
            this.groupBoxActivity.TabIndex = 1;
            this.groupBoxActivity.TabStop = false;
            this.groupBoxActivity.Text = "School Activity";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.AliceBlue;
            this.panel6.Controls.Add(this.groupBoxActivity);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(5);
            this.panel6.Size = new System.Drawing.Size(737, 425);
            this.panel6.TabIndex = 7;
            // 
            // tabPageHome
            // 
            this.tabPageHome.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageHome.Controls.Add(this.panel6);
            this.tabPageHome.Location = new System.Drawing.Point(4, 33);
            this.tabPageHome.Name = "tabPageHome";
            this.tabPageHome.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHome.Size = new System.Drawing.Size(743, 431);
            this.tabPageHome.TabIndex = 3;
            this.tabPageHome.Text = "Home";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPageHome);
            this.tabControl1.Controls.Add(this.tabPageReports);
            this.tabControl1.Controls.Add(this.tabPageRegistration);
            this.tabControl1.Controls.Add(this.tabPageHODResults);
            this.tabControl1.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.tabControl1.Location = new System.Drawing.Point(0, 62);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(9, 6);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(751, 468);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPageReports
            // 
            this.tabPageReports.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageReports.Controls.Add(this.panel8);
            this.tabPageReports.Location = new System.Drawing.Point(4, 33);
            this.tabPageReports.Name = "tabPageReports";
            this.tabPageReports.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageReports.Size = new System.Drawing.Size(743, 431);
            this.tabPageReports.TabIndex = 2;
            this.tabPageReports.Text = "Generate Broadsheets";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.AliceBlue;
            this.panel8.Controls.Add(this.groupBox3);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(737, 425);
            this.panel8.TabIndex = 15;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.comboBoxDepartment);
            this.groupBox3.Controls.Add(this.comboBoxFaculty);
            this.groupBox3.Controls.Add(this.buttonView);
            this.groupBox3.Controls.Add(this.numericUpDownSession);
            this.groupBox3.Controls.Add(this.labelCurrentSession);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.groupBoxLevel);
            this.groupBox3.Controls.Add(this.groupBox2);
            this.groupBox3.Controls.Add(this.groupBox1);
            this.groupBox3.Controls.Add(this.buttonGenerateReports);
            this.groupBox3.Location = new System.Drawing.Point(5, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(729, 398);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Report Generation Panel";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(341, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 18);
            this.label2.TabIndex = 17;
            this.label2.Text = "Select Department:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 18);
            this.label1.TabIndex = 17;
            this.label1.Text = "Select Faculty:";
            // 
            // comboBoxDepartment
            // 
            this.comboBoxDepartment.FormattingEnabled = true;
            this.comboBoxDepartment.Location = new System.Drawing.Point(344, 58);
            this.comboBoxDepartment.Name = "comboBoxDepartment";
            this.comboBoxDepartment.Size = new System.Drawing.Size(366, 26);
            this.comboBoxDepartment.TabIndex = 16;
            // 
            // comboBoxFaculty
            // 
            this.comboBoxFaculty.FormattingEnabled = true;
            this.comboBoxFaculty.Location = new System.Drawing.Point(27, 58);
            this.comboBoxFaculty.Name = "comboBoxFaculty";
            this.comboBoxFaculty.Size = new System.Drawing.Size(281, 26);
            this.comboBoxFaculty.TabIndex = 16;
            this.comboBoxFaculty.SelectedIndexChanged += new System.EventHandler(this.comboBoxFaculty_SelectedIndexChanged);
            // 
            // buttonView
            // 
            this.buttonView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonView.ImageKey = "Gloss PNGKKMenu_Office.png";
            this.buttonView.ImageList = this.imageList2;
            this.buttonView.Location = new System.Drawing.Point(458, 352);
            this.buttonView.Name = "buttonView";
            this.buttonView.Padding = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.buttonView.Size = new System.Drawing.Size(116, 40);
            this.buttonView.TabIndex = 15;
            this.buttonView.Text = "&View File";
            this.buttonView.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonView.UseVisualStyleBackColor = true;
            this.buttonView.Click += new System.EventHandler(this.buttonView_Click);
            // 
            // numericUpDownSession
            // 
            this.numericUpDownSession.Location = new System.Drawing.Point(642, 119);
            this.numericUpDownSession.Maximum = new decimal(new int[] {
            2999,
            0,
            0,
            0});
            this.numericUpDownSession.Minimum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.numericUpDownSession.Name = "numericUpDownSession";
            this.numericUpDownSession.Size = new System.Drawing.Size(68, 23);
            this.numericUpDownSession.TabIndex = 14;
            this.numericUpDownSession.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // labelCurrentSession
            // 
            this.labelCurrentSession.AutoSize = true;
            this.labelCurrentSession.Location = new System.Drawing.Point(576, 151);
            this.labelCurrentSession.Name = "labelCurrentSession";
            this.labelCurrentSession.Size = new System.Drawing.Size(71, 18);
            this.labelCurrentSession.TabIndex = 13;
            this.labelCurrentSession.Text = "2000/2001";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(576, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 18);
            this.label4.TabIndex = 13;
            this.label4.Text = "Session:";
            // 
            // groupBoxLevel
            // 
            this.groupBoxLevel.Controls.Add(this.radioLevel800);
            this.groupBoxLevel.Controls.Add(this.radioLevel700);
            this.groupBoxLevel.Controls.Add(this.radioLevel600);
            this.groupBoxLevel.Controls.Add(this.radioLevel500);
            this.groupBoxLevel.Controls.Add(this.radioLevel400);
            this.groupBoxLevel.Controls.Add(this.radioLevel300);
            this.groupBoxLevel.Controls.Add(this.radioLevel200);
            this.groupBoxLevel.Controls.Add(this.radioLevel100);
            this.groupBoxLevel.Location = new System.Drawing.Point(27, 108);
            this.groupBoxLevel.Name = "groupBoxLevel";
            this.groupBoxLevel.Size = new System.Drawing.Size(281, 223);
            this.groupBoxLevel.TabIndex = 11;
            this.groupBoxLevel.TabStop = false;
            this.groupBoxLevel.Text = "Select Level";
            // 
            // radioLevel300
            // 
            this.radioLevel300.AutoSize = true;
            this.radioLevel300.Location = new System.Drawing.Point(13, 84);
            this.radioLevel300.Name = "radioLevel300";
            this.radioLevel300.Size = new System.Drawing.Size(84, 22);
            this.radioLevel300.TabIndex = 8;
            this.radioLevel300.TabStop = true;
            this.radioLevel300.Text = "300 Level";
            this.radioLevel300.UseVisualStyleBackColor = true;
            // 
            // radioLevel200
            // 
            this.radioLevel200.AutoSize = true;
            this.radioLevel200.Location = new System.Drawing.Point(13, 56);
            this.radioLevel200.Name = "radioLevel200";
            this.radioLevel200.Size = new System.Drawing.Size(84, 22);
            this.radioLevel200.TabIndex = 8;
            this.radioLevel200.TabStop = true;
            this.radioLevel200.Text = "200 Level";
            this.radioLevel200.UseVisualStyleBackColor = true;
            // 
            // radioLevel100
            // 
            this.radioLevel100.AutoSize = true;
            this.radioLevel100.Location = new System.Drawing.Point(13, 28);
            this.radioLevel100.Name = "radioLevel100";
            this.radioLevel100.Size = new System.Drawing.Size(84, 22);
            this.radioLevel100.TabIndex = 8;
            this.radioLevel100.TabStop = true;
            this.radioLevel100.Text = "100 Level";
            this.radioLevel100.UseVisualStyleBackColor = true;
            // 
            // tabPageRegistration
            // 
            this.tabPageRegistration.Controls.Add(this.listViewStudents);
            this.tabPageRegistration.Location = new System.Drawing.Point(4, 33);
            this.tabPageRegistration.Name = "tabPageRegistration";
            this.tabPageRegistration.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRegistration.Size = new System.Drawing.Size(743, 431);
            this.tabPageRegistration.TabIndex = 4;
            this.tabPageRegistration.Text = "Manage Online Course Registration";
            this.tabPageRegistration.UseVisualStyleBackColor = true;
            // 
            // listViewStudents
            // 
            this.listViewStudents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewStudents.BackColor = System.Drawing.Color.White;
            this.listViewStudents.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7});
            this.listViewStudents.Location = new System.Drawing.Point(6, 6);
            this.listViewStudents.Name = "listViewStudents";
            this.listViewStudents.Size = new System.Drawing.Size(731, 422);
            this.listViewStudents.TabIndex = 7;
            this.listViewStudents.UseCompatibleStateImageBehavior = false;
            this.listViewStudents.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "SN.";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Student Name";
            this.columnHeader2.Width = 188;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Reg. No.";
            this.columnHeader3.Width = 107;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Department";
            this.columnHeader4.Width = 158;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Course";
            this.columnHeader5.Width = 72;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Session";
            this.columnHeader6.Width = 71;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Semester";
            this.columnHeader7.Width = 69;
            // 
            // myProgress
            // 
            this.myProgress.AutoSize = false;
            this.myProgress.Name = "myProgress";
            this.myProgress.Size = new System.Drawing.Size(500, 16);
            // 
            // progressLabel
            // 
            this.progressLabel.AutoSize = false;
            this.progressLabel.Name = "progressLabel";
            this.progressLabel.Size = new System.Drawing.Size(245, 17);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.progressLabel,
            this.myProgress});
            this.statusStrip1.Location = new System.Drawing.Point(0, 541);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(768, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tabPageHODResults
            // 
            this.tabPageHODResults.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPageHODResults.Controls.Add(this.panel1);
            this.tabPageHODResults.Location = new System.Drawing.Point(4, 33);
            this.tabPageHODResults.Name = "tabPageHODResults";
            this.tabPageHODResults.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHODResults.Size = new System.Drawing.Size(743, 431);
            this.tabPageHODResults.TabIndex = 8;
            this.tabPageHODResults.Text = "Approve Results";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelApproved);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.studentsData);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.comboBoxDepartments);
            this.panel1.Controls.Add(this.comboCourse);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(737, 425);
            this.panel1.TabIndex = 10;
            // 
            // labelApproved
            // 
            this.labelApproved.AutoEllipsis = true;
            this.labelApproved.Location = new System.Drawing.Point(682, 17);
            this.labelApproved.Name = "labelApproved";
            this.labelApproved.Size = new System.Drawing.Size(47, 21);
            this.labelApproved.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(602, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 18);
            this.label7.TabIndex = 13;
            this.label7.Text = "Approved:";
            // 
            // studentsData
            // 
            this.studentsData.AllowUserToAddRows = false;
            this.studentsData.AllowUserToDeleteRows = false;
            this.studentsData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.studentsData.BackgroundColor = System.Drawing.Color.White;
            this.studentsData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.studentsData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StudentSn,
            this.StudentName,
            this.RegNo,
            this.CAScore,
            this.ExamScore,
            this.TotalScore,
            this.Grade});
            this.studentsData.Location = new System.Drawing.Point(0, 53);
            this.studentsData.Name = "studentsData";
            this.studentsData.ReadOnly = true;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentsData.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.studentsData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.studentsData.Size = new System.Drawing.Size(729, 249);
            this.studentsData.TabIndex = 11;
            // 
            // StudentSn
            // 
            this.StudentSn.DividerWidth = 1;
            this.StudentSn.HeaderText = "SN.";
            this.StudentSn.MaxInputLength = 900;
            this.StudentSn.Name = "StudentSn";
            this.StudentSn.ReadOnly = true;
            this.StudentSn.ToolTipText = "Serial Number";
            this.StudentSn.Width = 40;
            // 
            // StudentName
            // 
            this.StudentName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StudentName.DividerWidth = 1;
            this.StudentName.HeaderText = "Name of Student";
            this.StudentName.Name = "StudentName";
            this.StudentName.ReadOnly = true;
            this.StudentName.ToolTipText = "Student Name";
            // 
            // RegNo
            // 
            this.RegNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.RegNo.DividerWidth = 1;
            this.RegNo.HeaderText = "Reg. No.";
            this.RegNo.Name = "RegNo";
            this.RegNo.ReadOnly = true;
            this.RegNo.ToolTipText = "Student Registration Number";
            this.RegNo.Width = 81;
            // 
            // CAScore
            // 
            this.CAScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CAScore.DividerWidth = 1;
            this.CAScore.HeaderText = "CA Mark";
            this.CAScore.MaxInputLength = 2;
            this.CAScore.Name = "CAScore";
            this.CAScore.ReadOnly = true;
            this.CAScore.ToolTipText = "Continous Assessment";
            this.CAScore.Width = 79;
            // 
            // ExamScore
            // 
            this.ExamScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.ExamScore.DividerWidth = 1;
            this.ExamScore.HeaderText = "Exam Mark";
            this.ExamScore.MaxInputLength = 2;
            this.ExamScore.Name = "ExamScore";
            this.ExamScore.ReadOnly = true;
            this.ExamScore.ToolTipText = "Examination Mark";
            this.ExamScore.Width = 93;
            // 
            // TotalScore
            // 
            this.TotalScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.TotalScore.DividerWidth = 1;
            this.TotalScore.FillWeight = 10F;
            this.TotalScore.HeaderText = "Total";
            this.TotalScore.MaxInputLength = 3;
            this.TotalScore.Name = "TotalScore";
            this.TotalScore.ReadOnly = true;
            this.TotalScore.ToolTipText = "Total Mark";
            this.TotalScore.Width = 66;
            // 
            // Grade
            // 
            this.Grade.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Grade.DividerWidth = 1;
            this.Grade.FillWeight = 10F;
            this.Grade.HeaderText = "Grade";
            this.Grade.MaxInputLength = 2;
            this.Grade.Name = "Grade";
            this.Grade.ReadOnly = true;
            this.Grade.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Grade.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Grade.ToolTipText = "Result Letter Grade";
            this.Grade.Width = 52;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBoxResultSummary);
            this.panel3.Controls.Add(this.btnReject);
            this.panel3.Controls.Add(this.btnApprove);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 300);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(5);
            this.panel3.Size = new System.Drawing.Size(737, 125);
            this.panel3.TabIndex = 3;
            // 
            // groupBoxResultSummary
            // 
            this.groupBoxResultSummary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxResultSummary.Controls.Add(this.tableDetails);
            this.groupBoxResultSummary.Location = new System.Drawing.Point(8, 8);
            this.groupBoxResultSummary.Name = "groupBoxResultSummary";
            this.groupBoxResultSummary.Size = new System.Drawing.Size(374, 106);
            this.groupBoxResultSummary.TabIndex = 3;
            this.groupBoxResultSummary.TabStop = false;
            this.groupBoxResultSummary.Text = "Result Summary";
            // 
            // tableDetails
            // 
            this.tableDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tableDetails.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableDetails.ColumnCount = 4;
            this.tableDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.93617F));
            this.tableDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.06383F));
            this.tableDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this.tableDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 192F));
            this.tableDetails.Controls.Add(this.labelNR, 3, 1);
            this.tableDetails.Controls.Add(this.labelA, 0, 0);
            this.tableDetails.Controls.Add(this.labelAR, 3, 0);
            this.tableDetails.Controls.Add(this.labelB, 0, 1);
            this.tableDetails.Controls.Add(this.labelF, 2, 1);
            this.tableDetails.Controls.Add(this.labelC, 1, 0);
            this.tableDetails.Controls.Add(this.labelE, 2, 0);
            this.tableDetails.Controls.Add(this.labelD, 1, 1);
            this.tableDetails.Location = new System.Drawing.Point(6, 22);
            this.tableDetails.Name = "tableDetails";
            this.tableDetails.RowCount = 2;
            this.tableDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableDetails.Size = new System.Drawing.Size(362, 78);
            this.tableDetails.TabIndex = 4;
            // 
            // labelNR
            // 
            this.labelNR.AutoSize = true;
            this.labelNR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelNR.Location = new System.Drawing.Point(170, 40);
            this.labelNR.Name = "labelNR";
            this.labelNR.Size = new System.Drawing.Size(187, 36);
            this.labelNR.TabIndex = 0;
            this.labelNR.Text = "No Results:";
            this.labelNR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelA
            // 
            this.labelA.AutoSize = true;
            this.labelA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelA.Location = new System.Drawing.Point(5, 2);
            this.labelA.Name = "labelA";
            this.labelA.Size = new System.Drawing.Size(39, 36);
            this.labelA.TabIndex = 0;
            this.labelA.Text = "As:";
            this.labelA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAR
            // 
            this.labelAR.AutoSize = true;
            this.labelAR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAR.Location = new System.Drawing.Point(170, 2);
            this.labelAR.Name = "labelAR";
            this.labelAR.Size = new System.Drawing.Size(187, 36);
            this.labelAR.TabIndex = 0;
            this.labelAR.Text = "Ommitted Results:";
            this.labelAR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelB
            // 
            this.labelB.AutoSize = true;
            this.labelB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelB.Location = new System.Drawing.Point(5, 40);
            this.labelB.Name = "labelB";
            this.labelB.Size = new System.Drawing.Size(39, 36);
            this.labelB.TabIndex = 0;
            this.labelB.Text = "Bs:";
            this.labelB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelF
            // 
            this.labelF.AutoSize = true;
            this.labelF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelF.Location = new System.Drawing.Point(100, 40);
            this.labelF.Name = "labelF";
            this.labelF.Size = new System.Drawing.Size(62, 36);
            this.labelF.TabIndex = 0;
            this.labelF.Text = "Fs:";
            this.labelF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelC
            // 
            this.labelC.AutoSize = true;
            this.labelC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC.Location = new System.Drawing.Point(52, 2);
            this.labelC.Name = "labelC";
            this.labelC.Size = new System.Drawing.Size(40, 36);
            this.labelC.TabIndex = 0;
            this.labelC.Text = "Cs:";
            this.labelC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelE
            // 
            this.labelE.AutoSize = true;
            this.labelE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelE.Location = new System.Drawing.Point(100, 2);
            this.labelE.Name = "labelE";
            this.labelE.Size = new System.Drawing.Size(62, 36);
            this.labelE.TabIndex = 0;
            this.labelE.Text = "Es:";
            this.labelE.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelD
            // 
            this.labelD.AutoSize = true;
            this.labelD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelD.Location = new System.Drawing.Point(52, 40);
            this.labelD.Name = "labelD";
            this.labelD.Size = new System.Drawing.Size(40, 36);
            this.labelD.TabIndex = 0;
            this.labelD.Text = "Ds:";
            this.labelD.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnReject
            // 
            this.btnReject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReject.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.btnReject.BackColor = System.Drawing.Color.Transparent;
            this.btnReject.ColorBase = System.Drawing.Color.Gainsboro;
            this.btnReject.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(76)))), ((int)(((byte)(76)))), ((int)(((byte)(76)))));
            this.btnReject.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnReject.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnReject.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnReject.ColorPressStroke = System.Drawing.Color.Aqua;
            this.btnReject.Enabled = false;
            this.btnReject.FadingSpeed = 35;
            this.btnReject.FlatAppearance.BorderSize = 0;
            this.btnReject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReject.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.btnReject.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.btnReject.Image = global::ARMS.Properties.Resources.Delete;
            this.btnReject.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.btnReject.ImageOffset = 5;
            this.btnReject.IsPressed = false;
            this.btnReject.KeepPress = false;
            this.btnReject.Location = new System.Drawing.Point(586, 8);
            this.btnReject.MaxImageSize = new System.Drawing.Point(72, 72);
            this.btnReject.MenuPos = new System.Drawing.Point(0, 0);
            this.btnReject.Name = "btnReject";
            this.btnReject.Radius = 6;
            this.btnReject.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.btnReject.Size = new System.Drawing.Size(143, 106);
            this.btnReject.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.btnReject.SplitDistance = 0;
            this.btnReject.TabIndex = 1;
            this.btnReject.Text = "Reject Results";
            this.btnReject.Title = "";
            this.btnReject.UseVisualStyleBackColor = false;
            // 
            // btnApprove
            // 
            this.btnApprove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApprove.Arrow = ARMS.RibbonMenuButton.e_arrow.None;
            this.btnApprove.BackColor = System.Drawing.Color.Transparent;
            this.btnApprove.ColorBase = System.Drawing.Color.Gainsboro;
            this.btnApprove.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(76)))), ((int)(((byte)(76)))), ((int)(((byte)(76)))));
            this.btnApprove.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnApprove.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnApprove.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnApprove.ColorPressStroke = System.Drawing.Color.Aqua;
            this.btnApprove.Enabled = false;
            this.btnApprove.FadingSpeed = 35;
            this.btnApprove.FlatAppearance.BorderSize = 0;
            this.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApprove.Font = new System.Drawing.Font("Trebuchet MS", 11F, System.Drawing.FontStyle.Bold);
            this.btnApprove.GroupPos = ARMS.RibbonMenuButton.e_groupPos.None;
            this.btnApprove.Image = global::ARMS.Properties.Resources.apply;
            this.btnApprove.ImageLocation = ARMS.RibbonMenuButton.e_imagelocation.Top;
            this.btnApprove.ImageOffset = 5;
            this.btnApprove.IsPressed = false;
            this.btnApprove.KeepPress = false;
            this.btnApprove.Location = new System.Drawing.Point(435, 8);
            this.btnApprove.MaxImageSize = new System.Drawing.Point(72, 72);
            this.btnApprove.MenuPos = new System.Drawing.Point(0, 0);
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Radius = 6;
            this.btnApprove.ShowBase = ARMS.RibbonMenuButton.e_showbase.Yes;
            this.btnApprove.Size = new System.Drawing.Size(143, 106);
            this.btnApprove.SplitButton = ARMS.RibbonMenuButton.e_splitbutton.No;
            this.btnApprove.SplitDistance = 0;
            this.btnApprove.TabIndex = 2;
            this.btnApprove.Text = "Approve Results";
            this.btnApprove.Title = "";
            this.btnApprove.UseVisualStyleBackColor = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Candara", 9.75F);
            this.label6.Location = new System.Drawing.Point(17, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 15);
            this.label6.TabIndex = 6;
            this.label6.Text = "Department:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Candara", 9.75F);
            this.label3.Location = new System.Drawing.Point(335, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Course:";
            // 
            // comboBoxDepartments
            // 
            this.comboBoxDepartments.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDepartments.Font = new System.Drawing.Font("Tahoma", 10F);
            this.comboBoxDepartments.FormattingEnabled = true;
            this.comboBoxDepartments.Location = new System.Drawing.Point(101, 15);
            this.comboBoxDepartments.Name = "comboBoxDepartments";
            this.comboBoxDepartments.Size = new System.Drawing.Size(206, 24);
            this.comboBoxDepartments.TabIndex = 4;
            // 
            // comboCourse
            // 
            this.comboCourse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCourse.Font = new System.Drawing.Font("Tahoma", 10F);
            this.comboCourse.FormattingEnabled = true;
            this.comboCourse.Location = new System.Drawing.Point(389, 15);
            this.comboCourse.Name = "comboCourse";
            this.comboCourse.Size = new System.Drawing.Size(174, 24);
            this.comboCourse.TabIndex = 4;
            // 
            // homeExams
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 563);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "homeExams";
            this.Text = "Exams Unit Control Dashboard - ARMS";
            this.Load += new System.EventHandler(this.homeExams_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridActivity)).EndInit();
            this.groupBoxActivity.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.tabPageHome.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPageReports.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSession)).EndInit();
            this.groupBoxLevel.ResumeLayout(false);
            this.groupBoxLevel.PerformLayout();
            this.tabPageRegistration.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabPageHODResults.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.studentsData)).EndInit();
            this.panel3.ResumeLayout(false);
            this.groupBoxResultSummary.ResumeLayout(false);
            this.tableDetails.ResumeLayout(false);
            this.tableDetails.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioLevel600;
        private System.Windows.Forms.Button buttonGenerateReports;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.RadioButton radioBroadSheet;
        private System.Windows.Forms.RadioButton radioFacultySummary;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioSemester2;
        private System.Windows.Forms.RadioButton radioSemester1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioLevel800;
        private System.Windows.Forms.RadioButton radioLevel700;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.RadioButton radioLevel500;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.FolderBrowserDialog fbd;
        private System.Windows.Forms.SaveFileDialog sfd;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripButton toolBtnAbout;
        private System.Windows.Forms.ToolStripButton toolButtonLock;
        private System.Windows.Forms.ToolStripButton btnAnnouncer;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripComboBox toolComboSemester;
        private System.Windows.Forms.ToolStripTextBox toolTextAcademicYear;
        private System.Windows.Forms.ToolStripComboBox toolLevelChooser;
        private System.Windows.Forms.Label labelMode;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripMenuItem contactLecturerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leaveAMessageForThisStudentToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.RadioButton radioLevel400;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn activityText;
        private System.Windows.Forms.DataGridView dataGridActivity;
        private System.Windows.Forms.DataGridViewTextBoxColumn activityDate;
        private System.Windows.Forms.GroupBox groupBoxActivity;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TabPage tabPageHome;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageReports;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxDepartment;
        private System.Windows.Forms.ComboBox comboBoxFaculty;
        private System.Windows.Forms.Button buttonView;
        private System.Windows.Forms.NumericUpDown numericUpDownSession;
        private System.Windows.Forms.Label labelCurrentSession;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBoxLevel;
        private System.Windows.Forms.RadioButton radioLevel300;
        private System.Windows.Forms.RadioButton radioLevel200;
        private System.Windows.Forms.RadioButton radioLevel100;
        private System.Windows.Forms.ToolStripProgressBar myProgress;
        private System.Windows.Forms.ToolStripStatusLabel progressLabel;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TabPage tabPageRegistration;
        private System.Windows.Forms.ListView listViewStudents;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.TabPage tabPageHODResults;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelApproved;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView studentsData;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudentSn;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn RegNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn CAScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExamScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grade;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBoxResultSummary;
        private System.Windows.Forms.TableLayoutPanel tableDetails;
        private System.Windows.Forms.Label labelNR;
        private System.Windows.Forms.Label labelA;
        private System.Windows.Forms.Label labelAR;
        private System.Windows.Forms.Label labelB;
        private System.Windows.Forms.Label labelF;
        private System.Windows.Forms.Label labelC;
        private System.Windows.Forms.Label labelE;
        private System.Windows.Forms.Label labelD;
        private RibbonMenuButton btnReject;
        private RibbonMenuButton btnApprove;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxDepartments;
        private System.Windows.Forms.ComboBox comboCourse;
    }
}