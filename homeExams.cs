﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ARMS
{
    public partial class homeExams : Form
    {
        public homeExams()
        {
            InitializeComponent();
        }

        private void buttonGenerateReports_Click(object sender, EventArgs e)
        {

        }

        private void buttonView_Click(object sender, EventArgs e)
        {

        }


        private Dictionary<string, string> departments = new Dictionary<string, string>();

        private void loadDepartments(string faculty)
        {
            var depts = Helper.selectResults(new[] { "deptName", "faculty" }, "departments", string.Format("WHERE faculty = '{0}'", faculty));
            foreach (var dept in depts)
            {
                if (!comboBoxDepartment.Items.Contains(dept[0]))
                {
                    comboBoxDepartment.Items.Add(dept[0]);
                    departments.Add(dept[0], dept[1]);
                }
            }
            comboBoxDepartment.SelectedIndex = 0;
        }

        private void comboBoxFaculty_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadDepartments(comboBoxFaculty.Text);
        }

        private Timer tt;

        private void LoadForm(object state, EventArgs eventArgs)
        {
            tt.Enabled = false;
            //if (local) return;
            //LoadActivities(false);

            //Load Faculties
            var results = Helper.selectResults(new[] { "facultyName" }, "faculties", "");
            foreach (var faculty in results)
            {
                comboBoxFaculty.Items.Add(faculty[0]);
            }
            comboBoxFaculty.SelectedIndex = 0;
        }

        private void homeExams_Load(object sender, EventArgs e)
        {
            //textBoxAcademicYear.Text = Helper.AcademicYear;
            //textBoxInstitution.Text = Helper.getProperty("schoolName");
            //comboBoxSemester.Text = Helper.getProperty("semester");
            //toolLabelAcademicYear.Text += " " + textBoxAcademicYear.Text;
            //toolLabelSemester.Text += " " + comboBoxSemester.Text;
            tt = new Timer { Interval = 1000, Enabled = true };
            tt.Tick += LoadForm;
        }
    }
}
