using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ARMS
{
    /// <summary>
    /// Initially designed mainly to show charts and reporting details to the Faculty User
    /// </summary>
    public partial class HomeFaculty : Form
    {
        private string username;
        private readonly string faculty;

        /// <summary>
        /// Initializes the form using the specified username and faculty
        /// </summary>
        /// <param name="username">The username of the current module</param>
        /// <param name="faculty">The faculty of the current user</param>
        public HomeFaculty(string username, string faculty)
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = false;
            var c = ChartColorPalette.SemiTransparent;
            chart1.Series[0].Palette = c;
            this.username = username;
            this.faculty = faculty;
            switch (faculty)
            {
                case "Agriculture":
                    var arr = new[] {
                        "Agricultural Economics",
                        "Agricultural Extension",
                        "Animal Science",
                        "Crop Science",
                        "Food Science and Technology",
                        "Home Science, Nutrition and Dietetics",
                        "Soil Science"};
                    comboDept.AutoCompleteCustomSource = listReload(arr);
                    break;
                case "Arts":
                    arr = new[] {
                        "Archeology and Tourism",
                        "English and Literary Studies",
                        "Fine and Applied Arts",
                        "Foreign Languages and Literature",
                        "History and International Studies",
                        "Linguistics and Nigerian Languages",
                        "Mass Communication",
                        "Music",
                        "Theatre and Film Studies"
                    };
                    comboDept.AutoCompleteCustomSource = listReload(arr);
                    break;
                case "Biological Sciences":
                    arr = new[] {
                        "Biochemistry",
                        "Botany",
                        "Microbiology",
                        "Zoology"};
                    comboDept.AutoCompleteCustomSource = listReload(arr);
                    break;
                case "Business Administration":
                    arr = new[] {
                        "Accountancy",
                        "Banking and Finance",
                        "Management",
                        "Marketing"};
                    comboDept.AutoCompleteCustomSource = listReload(arr);
                    break;
                case "Education":
                    arr = new[] {
                        "Adult Education and Extra-Mural Studies",
                        "Arts Education",
                        "Educational Foundations",
                        "Health and Physical Education",
                        "Library and Information Science",
                        "Science Education",
                        "Social Sciences Education",
                        "Vocational Teacher Education",
                        "Religion",
                        "Igbo Linguistics"
                    };
                    comboDept.AutoCompleteCustomSource = listReload(arr);
                    break;
                case "Engineering":
                    arr = new[] {
                        "Agricultural and Bioresources Engineering",
                        "Civil Engineering",
                        "Electrical Engineering",
                        "Electronic Engineering",
                        "Mechanical Engineering",
                        "Metallurgical and Materials Engineering"};
                    comboDept.AutoCompleteCustomSource = listReload(arr);
                    break;
                case "Environmental Studies":
                    arr = new[] {
                        "Architecture",
                        "Estate Management",
                        "Geoinformatics and Surveying",
                        "Urban and Regional Planning"
                    };
                    comboDept.AutoCompleteCustomSource = listReload(arr);
                    break;
                case "Health Sciences & Technology":
                    arr = new[] {
                        "Health Administration and Management",
                        "Medical Laboratory Sciences",
                        "Medical Radiography and Radiological Sciences",
                        "Medical Rehabilitation",
                        "Nursing Sciences"
                    };
                    comboDept.AutoCompleteCustomSource = listReload(arr);
                    break;
                case "Law":
                    arr = new[] {
                        "Commercial and Property Law",
                        "International and Jurisprudence",
                        "Private and Public Law"
                    };
                    comboDept.AutoCompleteCustomSource = listReload(arr);
                    break;
                case "Medical Sciences & Dentistry":
                    arr = new[] {
                        "Anatomy","Anesthesia","Chemical Pathology",
                        "Community Medicine","Dermatology","Hematology and Immunology",
                        "Medical Biochemistry","Medical Microbiology","Medicine",
                        "Morbid Anatomy", "Obstetrics and Gynecology", "Ophthalmology",
                        "Otolaryngology", "Pediatrics", "Pharmacology and Therapeutics",
                        "Physiology", "Radiation Medicine", "Surgery",
                        "Psychological Medicine", "Child Dental Health", "Oral Maxillofacialsurgery",
                        "Preventive Dentistry", "Restorative Dentistry"
                    };
                    comboDept.AutoCompleteCustomSource = listReload(arr);
                    break;
                case "Pharmaceutical Sciences":
                    arr = new[] {
                        "Clinical Pharmacy and Pharmacy Management",
                        "Pharmaceutical Chemistry and Industrial Pharmacy",
                        "Pharmaceutical Technology and Industrial Pharmacy",
                        "Pharmaceutics",
                        "Pharmacognosy",
                        "Pharmacology and Toxicology"
                    };
                    comboDept.AutoCompleteCustomSource = listReload(arr);
                    break;
                case "Physical Sciences":
                    arr = new[] {
                        "Computer Science",
                        "Geology",
                        "Mathematics",
                        "Physics and Astronomy",
                        "Pure and Industrial Chemistry",
                        "Statistics"
                    };
                    comboDept.AutoCompleteCustomSource = listReload(arr);
                    break;
                case "School of General Studies":
                    arr = new[] {
                        "Humanities Unit",
                        "Natural Science Unit",
                        "Social Science Unit",
                        "The Use of English Unit"
                    };
                    comboDept.AutoCompleteCustomSource = listReload(arr);
                    break;

                case "Social Sciences":
                    arr = new[] {
                        "Economics",
                        "Geography",
                        "Philosophy",
                        "Political Science",
                        "Psychology",
                        "Public Administration and Local Government",
                        "Religion",
                        "Social Work",
                        "Sociology/Anthropology"
                    };
                    comboDept.AutoCompleteCustomSource = listReload(arr);
                    break;
                case "Vertinary Medicine":
                    arr = new[] {
                        "Veterinary Physiology/Pharmacology",
                        "Veterinary Anatomy",
                        "Animal Health and Production",
                        "Veterinary Parasitology and Entomology",
                        "Veterinary Pathology and Microbiology",
                        "Veterinary Public Health and Preventive Medicine",
                        "Veterinary Surgery",
                        "Veterinary Medicine",
                        "Veterinary Obstetrics and Reproductive Diseases",
                        "Veterinary Teaching Hospital"
                    };
                    comboDept.AutoCompleteCustomSource = listReload(arr);
                    break;
            }
        }

        private AutoCompleteStringCollection listReload(string[] arr)
        {
            comboDept.Items.Clear();
            comboDept.Items.AddRange(arr);
            var ac = new AutoCompleteStringCollection();
            ac.AddRange(arr);
            return ac;
        }

        bool performanceCheck;

        private void btnViewStudents_Click(object sender, EventArgs e)
        {
            performanceCheck = true;
            updateChart();
        }

        private void loadChart()
        {
            dataPointA = dataPointA ?? new DataPoint(0D, 0D);
            dataPointB = dataPointB ?? new DataPoint(20D, 0D);
            dataPointC = dataPointC ?? new DataPoint(40D, 0D);
            dataPointD = dataPointD ?? new DataPoint(60D, 0D);
            dataPointE = dataPointE ?? new DataPoint(80D, 0D);
            dataPointF = dataPointF ?? new DataPoint(100D, 0D);
            dataPointM = dataPointM ?? new DataPoint(120D, 0D);

            dataPointA.SetValueY(countA);
            dataPointB.SetValueY(countB);
            dataPointC.SetValueY(countC);
            dataPointD.SetValueY(countD);
            dataPointE.SetValueY(countE);
            dataPointF.SetValueY(countF);
            dataPointM.SetValueY(countM);
        }

        private void updateChart()
        {
            panelSmart.Visible = panelSmart.Visible ? false : true;
            dataGridView1.Visible = dataGridView1.Visible ? false : true;
            chart1.Visible = chart1.Visible ? false : true;
        }

        /// <summary>
        /// Results approval
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpdateData_Click(object sender, EventArgs e)
        {
            string cText = comboCourse.Text;
            DialogResult dr;
            if (performanceCheck)
            {/*
 dr = MessageBox.Show("Are you sure you want to approve the listed results, analyzing the peformance first will help in making a final decision. Click the peformance button to view analysis chart.", string.Format("Approve {0} Results?", cText), MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question); */
}
            else
            {
                dr = MessageBox.Show("Are you sure you want to approve the listed results?", string.Format("Approve {0} Results?", cText), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    foreach (var reg in regNos)
                    {
                        Helper.Update("student_courses", new[]{"approved"}, new[]{"true"}, new[] { "regNo", "CourseCode" }, new[] { reg, cText }, "");
                    }
                    MessageBox.Show(comboCourse.Text + " results has been approved successfully.", "Results Approved!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            updateGrid(searchText.Text);
        }

        private void searchText_Click(object sender, EventArgs e)
        {
            searchText.Clear();
        }

        private string cLecturer = "";

        private void comboCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateGrid(comboCourse.Text);
            loadChart();
            cLecturer = Helper.selectResults(new[] { "username" }, "lecturer_courses", new[] { "CourseCode" }, new[] { comboCourse.Text }, "")[0][0];
            performanceCheck = false;
        }

        private double countA;
        private double countB;
        private double countC;
        private double countD;
        private double countE;
        private double countF;
        private double countM;

        private void clearCounts()
        {
            countA = 0;
            countB = 0;
            countC = 0;
            countD = 0;
            countE = 0;
            countF = 0;
            countM = 0;
        }

        private void updateGrid(string course)
        {
            dataGridView1.Rows.Clear();
            clearCounts();
            regNos = new List<string>();
            foreach (string[] s in Helper.selectResults(new[] { "regNo", "CourseCode", "ca_score", "exam_score", "(exam_score-1) + (ca_score-1)" }, "student_courses", new[] { "CourseCode", "dapproved", "Score !"}, new[] { course, "1", "NULL" }, ""))
            {
                regNos.Add(s[0]);
                object[] data = new object[6];
                s.CopyTo(data, 0);
                int score = Convert.ToInt32(s[4]);
                string gd = StudentData.ComputeGrade(score);
                data[5] = gd;
                switch(gd){
                    case "A":
                        countA++; break;
                    case "B":
                        countB++; break;
                    case "C":
                        countC++; break;
                    case "D":
                        countC++; break;
                    case "E":
                        countE++; break;
                    case "F":
                        countF++; break;
                    case "Missing":
                        countM++; break;
                }
                dataGridView1.Rows.Add(data);
            }
        }

        List<string> regNos;

/*
        /// <summary>
        /// Copies values of an array into a larger array
        /// </summary>
        /// <param name="arr1">The smaller array</param>
        /// <param name="arr2">The larger array</param>
        private void copyExisting(ref string[] arr1, ref string[] arr2)
        {
            for(int i = 0; i < arr1.Length; i++)
            {
                arr2[i] = arr1[i];
            }
        }
*/

        private void comboLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboCourse.Items.Clear();
            if (comboDept.Text == "") { MessageBox.Show("Please choose a department to continue.", "No Department Selected.", MessageBoxButtons.OK, MessageBoxIcon.Stop); return; }
            string level = (Convert.ToInt32(comboLevel.Text) / 100).ToString(CultureInfo.InvariantCulture);
            foreach(string[] s in Helper.selectResults(new[] { "CourseCode" }, "courses", new[] { "level", "department" }, new[] { level, comboDept.Text}, ""))
            {
                comboCourse.Items.Add(s[0]);
            }
        }

        private void btnReject_Click(object sender, EventArgs e)
        {
            string cText = comboCourse.Text;
            var dr = MessageBox.Show("Would you like to leave a message for the Lectuer of this course.", string.Format("{0} Course rejection", cText), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {/*
                sMessage s = new sMessage(cLecturer);
                s.ShowDialog();*/
                return;
            }
            Helper.sendMessage(cLecturer, faculty, string.Format("The results for {0} has been disapproved by the faculty.", comboCourse.Text), "Results Disapproved", "student_courses", "lapproved", "1", "regNo,CourseCode", string.Format("{0}, {1}", regNos[0], cText));
            Helper.sendMessage(comboDept.Text, faculty,
                                string.Format("The results for {0} has been disapproved by the faculty.",
                                              comboCourse.Text), "Results Disapproved", "student_courses", "dapproved",
                                "1", "regNo,CourseCode", string.Format("{0}, {1}", regNos[0], cText));
            MessageBox.Show(comboCourse.Text + " results has been rejected and the involved parties notified.", "Results Rejected.", MessageBoxButtons.OK, MessageBoxIcon.Information);

            foreach (var reg in regNos)
            {
                Helper.Update("student_courses", new[] { "approved" }, new[] { "false" }, new[] { "regNo", "CourseCode" }, new[] { reg, cText }, "");
                Helper.Update("student_courses", new[] { "dapproved" }, new[] { "false" }, new[] { "regNo", "CourseCode" }, new[] { reg, cText }, "");
                Helper.Update("student_courses", new[] { "lapproved" }, new[] { "false" }, new[] { "regNo", "CourseCode" }, new[] { reg, cText }, "");
            }
        }

        private void btnChat_Click(object sender, EventArgs e)
        {
            if (cLecturer != "" && comboLevel.Text != "")
            {/*
                sMessage sm = new sMessage(cLecturer);
                sm.Show();*/
            }
            else
            {
                MessageBox.Show("Please choose a course and try again.", "No Lecturer Found!", MessageBoxButtons.OK,
                                MessageBoxIcon.Stop);
            }
        }

        private void btnMsg_Click(object sender, EventArgs e)
        {/*
            eMessage em = new eMessage(username);
            em.Show();*/
        }
    }
}
