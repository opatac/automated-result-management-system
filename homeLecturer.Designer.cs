

namespace ARMS
{
    partial class homeLecturer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(homeLecturer));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBoxMessages = new System.Windows.Forms.GroupBox();
            this.richTextBoxAnnouncements = new System.Windows.Forms.RichTextBox();
            this.buttonPreviousAnnounce = new System.Windows.Forms.Button();
            this.buttonNextAnnounce = new System.Windows.Forms.Button();
            this.btnChangePic = new System.Windows.Forms.Button();
            this.labelWelcome = new System.Windows.Forms.Label();
            this.displayPic = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelMode = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.sessionTool = new System.Windows.Forms.ToolStripComboBox();
            this.btnAddLecture = new System.Windows.Forms.ToolStripButton();
            this.comboCourses = new System.Windows.Forms.ToolStripComboBox();
            this.btnLock = new System.Windows.Forms.ToolStripButton();
            this.btnEditPassword = new System.Windows.Forms.ToolStripButton();
            this.toolBtnAbout = new System.Windows.Forms.ToolStripButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dataGridActivity = new System.Windows.Forms.DataGridView();
            this.activityDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activityText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.comboBoxDataGen = new System.Windows.Forms.ComboBox();
            this.comboBoxDepartment = new System.Windows.Forms.ComboBox();
            this.comboBoxCourseGen = new System.Windows.Forms.ComboBox();
            this.buttonUpload = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.buttonImport = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonViewinExcel = new System.Windows.Forms.Button();
            this.buttonSaveAs = new System.Windows.Forms.Button();
            this.buttonPrintReport = new System.Windows.Forms.Button();
            this.pictureDialog = new System.Windows.Forms.OpenFileDialog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPagePrinting = new System.Windows.Forms.TabPage();
            this.listViewStudents = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel6 = new System.Windows.Forms.Panel();
            this.labelGenStatus = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.examDatePicker = new System.Windows.Forms.DateTimePicker();
            this.labelExanDate = new System.Windows.Forms.Label();
            this.tabPageResults = new System.Windows.Forms.TabPage();
            this.panelResults = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.labelPerformance = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelUnits = new System.Windows.Forms.Label();
            this.labelSemester = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelCourse = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.studentsData = new System.Windows.Forms.DataGridView();
            this.StudentSn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StudentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RegNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dept = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExamScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Grade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelResultControls = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.ofd = new System.Windows.Forms.OpenFileDialog();
            this.sfd = new System.Windows.Forms.SaveFileDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.panel1.SuspendLayout();
            this.groupBoxMessages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.displayPic)).BeginInit();
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridActivity)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPagePrinting.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tabPageResults.SuspendLayout();
            this.panelResults.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.studentsData)).BeginInit();
            this.panelResultControls.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.groupBoxMessages);
            this.panel1.Controls.Add(this.btnChangePic);
            this.panel1.Controls.Add(this.labelWelcome);
            this.panel1.Controls.Add(this.displayPic);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(849, 245);
            this.panel1.TabIndex = 0;
            // 
            // groupBoxMessages
            // 
            this.groupBoxMessages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxMessages.Controls.Add(this.richTextBoxAnnouncements);
            this.groupBoxMessages.Controls.Add(this.buttonPreviousAnnounce);
            this.groupBoxMessages.Controls.Add(this.buttonNextAnnounce);
            this.groupBoxMessages.Location = new System.Drawing.Point(8, 41);
            this.groupBoxMessages.Name = "groupBoxMessages";
            this.groupBoxMessages.Size = new System.Drawing.Size(701, 201);
            this.groupBoxMessages.TabIndex = 3;
            this.groupBoxMessages.TabStop = false;
            this.groupBoxMessages.Text = "Announcements";
            // 
            // richTextBoxAnnouncements
            // 
            this.richTextBoxAnnouncements.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxAnnouncements.BackColor = System.Drawing.Color.White;
            this.richTextBoxAnnouncements.Location = new System.Drawing.Point(6, 22);
            this.richTextBoxAnnouncements.Name = "richTextBoxAnnouncements";
            this.richTextBoxAnnouncements.ReadOnly = true;
            this.richTextBoxAnnouncements.Size = new System.Drawing.Size(689, 140);
            this.richTextBoxAnnouncements.TabIndex = 2;
            this.richTextBoxAnnouncements.Text = "No announcements found";
            this.toolTip1.SetToolTip(this.richTextBoxAnnouncements, "Shows general or lecturer based announcements for the department");
            this.richTextBoxAnnouncements.MouseMove += new System.Windows.Forms.MouseEventHandler(this.app_MouseMove);
            // 
            // buttonPreviousAnnounce
            // 
            this.buttonPreviousAnnounce.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPreviousAnnounce.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.buttonPreviousAnnounce.Location = new System.Drawing.Point(513, 168);
            this.buttonPreviousAnnounce.Name = "buttonPreviousAnnounce";
            this.buttonPreviousAnnounce.Size = new System.Drawing.Size(88, 27);
            this.buttonPreviousAnnounce.TabIndex = 1;
            this.buttonPreviousAnnounce.Text = "Previous";
            this.toolTip1.SetToolTip(this.buttonPreviousAnnounce, "Show the previous annuoncement");
            this.buttonPreviousAnnounce.UseVisualStyleBackColor = true;
            this.buttonPreviousAnnounce.Click += new System.EventHandler(this.buttonPreviousAnnounce_Click);
            // 
            // buttonNextAnnounce
            // 
            this.buttonNextAnnounce.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNextAnnounce.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.buttonNextAnnounce.Location = new System.Drawing.Point(607, 168);
            this.buttonNextAnnounce.Name = "buttonNextAnnounce";
            this.buttonNextAnnounce.Size = new System.Drawing.Size(88, 27);
            this.buttonNextAnnounce.TabIndex = 1;
            this.buttonNextAnnounce.Text = "Next";
            this.toolTip1.SetToolTip(this.buttonNextAnnounce, "Show the next announcement");
            this.buttonNextAnnounce.UseVisualStyleBackColor = true;
            this.buttonNextAnnounce.Click += new System.EventHandler(this.buttonNextAnnounce_Click);
            // 
            // btnChangePic
            // 
            this.btnChangePic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChangePic.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnChangePic.Location = new System.Drawing.Point(722, 203);
            this.btnChangePic.Name = "btnChangePic";
            this.btnChangePic.Size = new System.Drawing.Size(122, 33);
            this.btnChangePic.TabIndex = 2;
            this.btnChangePic.Text = "Change";
            this.toolTip1.SetToolTip(this.btnChangePic, "Change Display Picture");
            this.btnChangePic.UseVisualStyleBackColor = true;
            this.btnChangePic.Click += new System.EventHandler(this.btnChangePic_Click);
            // 
            // labelWelcome
            // 
            this.labelWelcome.BackColor = System.Drawing.Color.Transparent;
            this.labelWelcome.Font = new System.Drawing.Font("Trebuchet MS", 18F);
            this.labelWelcome.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelWelcome.Location = new System.Drawing.Point(3, 4);
            this.labelWelcome.Name = "labelWelcome";
            this.labelWelcome.Size = new System.Drawing.Size(488, 33);
            this.labelWelcome.TabIndex = 1;
            this.labelWelcome.Text = "Good day Mr. UNN. ";
            this.labelWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // displayPic
            // 
            this.displayPic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.displayPic.Image = global::ARMS.Properties.Resources.arms128;
            this.displayPic.Location = new System.Drawing.Point(722, 54);
            this.displayPic.Name = "displayPic";
            this.displayPic.Size = new System.Drawing.Size(122, 140);
            this.displayPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.displayPic.TabIndex = 0;
            this.displayPic.TabStop = false;
            this.toolTip1.SetToolTip(this.displayPic, "User Display Picture");
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SkyBlue;
            this.panel2.Controls.Add(this.labelMode);
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(863, 62);
            this.panel2.TabIndex = 2;
            this.panel2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.app_MouseMove);
            // 
            // labelMode
            // 
            this.labelMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.labelMode.ForeColor = System.Drawing.Color.Red;
            this.labelMode.Location = new System.Drawing.Point(675, 11);
            this.labelMode.Name = "labelMode";
            this.labelMode.Size = new System.Drawing.Size(176, 39);
            this.labelMode.TabIndex = 4;
            this.labelMode.Text = "ONLINE";
            this.labelMode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Font = new System.Drawing.Font("Trebuchet MS", 11F);
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sessionTool,
            this.btnAddLecture,
            this.comboCourses,
            this.btnLock,
            this.btnEditPassword,
            this.toolBtnAbout});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(3, 2);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(535, 57);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // sessionTool
            // 
            this.sessionTool.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.sessionTool.Name = "sessionTool";
            this.sessionTool.Size = new System.Drawing.Size(121, 57);
            this.sessionTool.Text = "Select Session";
            // 
            // btnAddLecture
            // 
            this.btnAddLecture.Name = "btnAddLecture";
            this.btnAddLecture.Size = new System.Drawing.Size(23, 54);
            // 
            // comboCourses
            // 
            this.comboCourses.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.comboCourses.Name = "comboCourses";
            this.comboCourses.Size = new System.Drawing.Size(160, 57);
            this.comboCourses.Text = "Select Course";
            this.comboCourses.SelectedIndexChanged += new System.EventHandler(this.comboCourses_SelectedIndexChanged);
            // 
            // btnLock
            // 
            this.btnLock.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLock.Image = global::ARMS.Properties.Resources.Lock;
            this.btnLock.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLock.Name = "btnLock";
            this.btnLock.Size = new System.Drawing.Size(52, 54);
            this.btnLock.Text = "Lock Session";
            this.btnLock.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // btnEditPassword
            // 
            this.btnEditPassword.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEditPassword.Image = global::ARMS.Properties.Resources.advancedsettings;
            this.btnEditPassword.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditPassword.Name = "btnEditPassword";
            this.btnEditPassword.Size = new System.Drawing.Size(52, 54);
            this.btnEditPassword.Text = "Click to Update Password";
            this.btnEditPassword.Click += new System.EventHandler(this.btnEditPassword_Click);
            // 
            // toolBtnAbout
            // 
            this.toolBtnAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolBtnAbout.Image = global::ARMS.Properties.Resources.arms128;
            this.toolBtnAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolBtnAbout.Name = "toolBtnAbout";
            this.toolBtnAbout.Size = new System.Drawing.Size(52, 54);
            this.toolBtnAbout.Text = "About Automated Result Management System";
            this.toolBtnAbout.Click += new System.EventHandler(this.toolBtnAbout_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.AliceBlue;
            this.panel3.Controls.Add(this.dataGridActivity);
            this.panel3.Location = new System.Drawing.Point(3, 254);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(5);
            this.panel3.Size = new System.Drawing.Size(849, 203);
            this.panel3.TabIndex = 3;
            // 
            // dataGridActivity
            // 
            this.dataGridActivity.AllowUserToAddRows = false;
            this.dataGridActivity.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridActivity.BackgroundColor = System.Drawing.Color.White;
            this.dataGridActivity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridActivity.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dataGridActivity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridActivity.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.activityDate,
            this.activityText});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridActivity.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridActivity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridActivity.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridActivity.EnableHeadersVisualStyles = false;
            this.dataGridActivity.GridColor = System.Drawing.Color.White;
            this.dataGridActivity.Location = new System.Drawing.Point(5, 5);
            this.dataGridActivity.MultiSelect = false;
            this.dataGridActivity.Name = "dataGridActivity";
            this.dataGridActivity.ReadOnly = true;
            this.dataGridActivity.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridActivity.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridActivity.Size = new System.Drawing.Size(839, 193);
            this.dataGridActivity.TabIndex = 3;
            this.dataGridActivity.TabStop = false;
            this.dataGridActivity.MouseMove += new System.Windows.Forms.MouseEventHandler(this.app_MouseMove);
            // 
            // activityDate
            // 
            this.activityDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.activityDate.DividerWidth = 1;
            this.activityDate.FillWeight = 25F;
            this.activityDate.HeaderText = "Date";
            this.activityDate.MinimumWidth = 150;
            this.activityDate.Name = "activityDate";
            this.activityDate.ReadOnly = true;
            this.activityDate.Width = 160;
            // 
            // activityText
            // 
            this.activityText.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.activityText.HeaderText = "Details";
            this.activityText.Name = "activityText";
            this.activityText.ReadOnly = true;
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonGenerate.ImageKey = "(none)";
            this.buttonGenerate.Location = new System.Drawing.Point(773, 13);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Padding = new System.Windows.Forms.Padding(3);
            this.buttonGenerate.Size = new System.Drawing.Size(61, 41);
            this.buttonGenerate.TabIndex = 3;
            this.buttonGenerate.Text = "&Go";
            this.toolTip1.SetToolTip(this.buttonGenerate, "Generate a document based on the selected criteria");
            this.buttonGenerate.UseVisualStyleBackColor = true;
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // comboBoxDataGen
            // 
            this.comboBoxDataGen.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxDataGen.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxDataGen.FormattingEnabled = true;
            this.comboBoxDataGen.Items.AddRange(new object[] {
            "Class Attendance Template",
            "Exam Attendance Register",
            "Official Grade Report Template"});
            this.comboBoxDataGen.Location = new System.Drawing.Point(354, 21);
            this.comboBoxDataGen.Name = "comboBoxDataGen";
            this.comboBoxDataGen.Size = new System.Drawing.Size(226, 26);
            this.comboBoxDataGen.TabIndex = 1;
            this.comboBoxDataGen.Text = "Select type of Data to Generate";
            this.toolTip1.SetToolTip(this.comboBoxDataGen, "Data Type to Generate");
            this.comboBoxDataGen.SelectedIndexChanged += new System.EventHandler(this.comboBoxDataGen_SelectedIndexChanged);
            // 
            // comboBoxDepartment
            // 
            this.comboBoxDepartment.FormattingEnabled = true;
            this.comboBoxDepartment.Location = new System.Drawing.Point(155, 21);
            this.comboBoxDepartment.Name = "comboBoxDepartment";
            this.comboBoxDepartment.Size = new System.Drawing.Size(193, 26);
            this.comboBoxDepartment.TabIndex = 1;
            this.comboBoxDepartment.Text = "Select Department";
            this.toolTip1.SetToolTip(this.comboBoxDepartment, "Department");
            // 
            // comboBoxCourseGen
            // 
            this.comboBoxCourseGen.FormattingEnabled = true;
            this.comboBoxCourseGen.Location = new System.Drawing.Point(13, 21);
            this.comboBoxCourseGen.Name = "comboBoxCourseGen";
            this.comboBoxCourseGen.Size = new System.Drawing.Size(131, 26);
            this.comboBoxCourseGen.TabIndex = 1;
            this.comboBoxCourseGen.Text = "Select Course";
            this.toolTip1.SetToolTip(this.comboBoxCourseGen, "Course to Generate data for");
            this.comboBoxCourseGen.SelectedIndexChanged += new System.EventHandler(this.comboBoxCourseGen_SelectedIndexChanged);
            // 
            // buttonUpload
            // 
            this.buttonUpload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUpload.AutoSize = true;
            this.buttonUpload.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonUpload.ImageKey = "build.png";
            this.buttonUpload.ImageList = this.imageList1;
            this.buttonUpload.Location = new System.Drawing.Point(333, 3);
            this.buttonUpload.Name = "buttonUpload";
            this.buttonUpload.Padding = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.buttonUpload.Size = new System.Drawing.Size(106, 44);
            this.buttonUpload.TabIndex = 0;
            this.buttonUpload.Text = "&Submit";
            this.buttonUpload.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.buttonUpload, "Submit the imported gradesheet or result");
            this.buttonUpload.UseVisualStyleBackColor = true;
            this.buttonUpload.Click += new System.EventHandler(this.buttonUpload_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "build.png");
            this.imageList1.Images.SetKeyName(1, "print_48.png");
            this.imageList1.Images.SetKeyName(2, "Delete.png");
            this.imageList1.Images.SetKeyName(3, "MyDocuments.png");
            this.imageList1.Images.SetKeyName(4, "Gloss PNGKKMenu_Office.png");
            this.imageList1.Images.SetKeyName(5, "xcel.png");
            // 
            // buttonImport
            // 
            this.buttonImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonImport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonImport.ImageKey = "xcel.png";
            this.buttonImport.ImageList = this.imageList1;
            this.buttonImport.Location = new System.Drawing.Point(159, 3);
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Padding = new System.Windows.Forms.Padding(1, 3, 3, 3);
            this.buttonImport.Size = new System.Drawing.Size(168, 44);
            this.buttonImport.TabIndex = 0;
            this.buttonImport.Text = "&Import Marksheet";
            this.buttonImport.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.buttonImport, "Click here to import or upload your marked gradesheet or result file");
            this.buttonImport.UseVisualStyleBackColor = true;
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonClear.ImageKey = "Delete.png";
            this.buttonClear.ImageList = this.imageList1;
            this.buttonClear.Location = new System.Drawing.Point(5, 6);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Padding = new System.Windows.Forms.Padding(1, 3, 3, 3);
            this.buttonClear.Size = new System.Drawing.Size(107, 40);
            this.buttonClear.TabIndex = 0;
            this.buttonClear.Text = "&Clear All";
            this.buttonClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.buttonClear, "Clear all the current displayed results");
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonViewinExcel
            // 
            this.buttonViewinExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonViewinExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonViewinExcel.ImageKey = "Gloss PNGKKMenu_Office.png";
            this.buttonViewinExcel.ImageList = this.imageList1;
            this.buttonViewinExcel.Location = new System.Drawing.Point(509, 13);
            this.buttonViewinExcel.Name = "buttonViewinExcel";
            this.buttonViewinExcel.Padding = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.buttonViewinExcel.Size = new System.Drawing.Size(116, 40);
            this.buttonViewinExcel.TabIndex = 3;
            this.buttonViewinExcel.Text = "&View File";
            this.buttonViewinExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.buttonViewinExcel, "Open the generated document in Microsoft Office");
            this.buttonViewinExcel.UseVisualStyleBackColor = true;
            this.buttonViewinExcel.Click += new System.EventHandler(this.buttonViewinExcel_Click);
            // 
            // buttonSaveAs
            // 
            this.buttonSaveAs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSaveAs.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSaveAs.ImageKey = "MyDocuments.png";
            this.buttonSaveAs.ImageList = this.imageList1;
            this.buttonSaveAs.Location = new System.Drawing.Point(631, 13);
            this.buttonSaveAs.Name = "buttonSaveAs";
            this.buttonSaveAs.Padding = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.buttonSaveAs.Size = new System.Drawing.Size(111, 40);
            this.buttonSaveAs.TabIndex = 3;
            this.buttonSaveAs.Text = "&Save As";
            this.buttonSaveAs.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.buttonSaveAs, "Save the generated document");
            this.buttonSaveAs.UseVisualStyleBackColor = true;
            this.buttonSaveAs.Click += new System.EventHandler(this.buttonSaveAs_Click);
            // 
            // buttonPrintReport
            // 
            this.buttonPrintReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPrintReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonPrintReport.ImageKey = "print_48.png";
            this.buttonPrintReport.ImageList = this.imageList1;
            this.buttonPrintReport.Location = new System.Drawing.Point(748, 13);
            this.buttonPrintReport.Name = "buttonPrintReport";
            this.buttonPrintReport.Padding = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.buttonPrintReport.Size = new System.Drawing.Size(95, 40);
            this.buttonPrintReport.TabIndex = 3;
            this.buttonPrintReport.Text = "&Print";
            this.buttonPrintReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.buttonPrintReport, "Print the generated document");
            this.buttonPrintReport.UseVisualStyleBackColor = true;
            this.buttonPrintReport.Click += new System.EventHandler(this.buttonPrintReport_Click);
            // 
            // pictureDialog
            // 
            this.pictureDialog.DefaultExt = "jpg";
            this.pictureDialog.FileName = "MyPassport.jpg";
            this.pictureDialog.Filter = "Pictures|*.jpg|Pictures|*.bmp|Pictures|*.png";
            this.pictureDialog.RestoreDirectory = true;
            this.pictureDialog.Title = "Select Dashboard Display Picture";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPagePrinting);
            this.tabControl1.Controls.Add(this.tabPageResults);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.tabControl1.Location = new System.Drawing.Point(0, 62);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(9, 6);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(863, 502);
            this.tabControl1.TabIndex = 5;
            this.tabControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.app_MouseMove);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 33);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(855, 465);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Home";
            this.tabPage1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.app_MouseMove);
            // 
            // tabPagePrinting
            // 
            this.tabPagePrinting.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPagePrinting.Controls.Add(this.listViewStudents);
            this.tabPagePrinting.Controls.Add(this.panel6);
            this.tabPagePrinting.Controls.Add(this.panel5);
            this.tabPagePrinting.Location = new System.Drawing.Point(4, 33);
            this.tabPagePrinting.Name = "tabPagePrinting";
            this.tabPagePrinting.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePrinting.Size = new System.Drawing.Size(855, 465);
            this.tabPagePrinting.TabIndex = 2;
            this.tabPagePrinting.Text = "Generate and Print";
            this.tabPagePrinting.Enter += new System.EventHandler(this.tabPagePrinting_Enter);
            this.tabPagePrinting.MouseMove += new System.Windows.Forms.MouseEventHandler(this.app_MouseMove);
            // 
            // listViewStudents
            // 
            this.listViewStudents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewStudents.BackColor = System.Drawing.Color.White;
            this.listViewStudents.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.listViewStudents.Location = new System.Drawing.Point(3, 77);
            this.listViewStudents.MinimumSize = new System.Drawing.Size(847, 333);
            this.listViewStudents.Name = "listViewStudents";
            this.listViewStudents.Size = new System.Drawing.Size(847, 333);
            this.listViewStudents.TabIndex = 6;
            this.listViewStudents.UseCompatibleStateImageBehavior = false;
            this.listViewStudents.View = System.Windows.Forms.View.Details;
            this.listViewStudents.MouseMove += new System.Windows.Forms.MouseEventHandler(this.app_MouseMove);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "SN.";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Name";
            this.columnHeader2.Width = 188;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Reg. No.";
            this.columnHeader3.Width = 107;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Department";
            this.columnHeader4.Width = 414;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Level";
            this.columnHeader5.Width = 72;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.AliceBlue;
            this.panel6.Controls.Add(this.labelGenStatus);
            this.panel6.Controls.Add(this.buttonViewinExcel);
            this.panel6.Controls.Add(this.buttonSaveAs);
            this.panel6.Controls.Add(this.buttonPrintReport);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(3, 402);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(849, 60);
            this.panel6.TabIndex = 5;
            this.panel6.MouseMove += new System.Windows.Forms.MouseEventHandler(this.app_MouseMove);
            // 
            // labelGenStatus
            // 
            this.labelGenStatus.AutoSize = true;
            this.labelGenStatus.Location = new System.Drawing.Point(6, 20);
            this.labelGenStatus.Name = "labelGenStatus";
            this.labelGenStatus.Size = new System.Drawing.Size(248, 18);
            this.labelGenStatus.TabIndex = 4;
            this.labelGenStatus.Text = "Enter details and click Go to generate";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.AliceBlue;
            this.panel5.Controls.Add(this.examDatePicker);
            this.panel5.Controls.Add(this.labelExanDate);
            this.panel5.Controls.Add(this.buttonGenerate);
            this.panel5.Controls.Add(this.comboBoxDataGen);
            this.panel5.Controls.Add(this.comboBoxDepartment);
            this.panel5.Controls.Add(this.comboBoxCourseGen);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(849, 68);
            this.panel5.TabIndex = 4;
            // 
            // examDatePicker
            // 
            this.examDatePicker.CustomFormat = "ddd dd MMM, yyyy";
            this.examDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.examDatePicker.Location = new System.Drawing.Point(599, 34);
            this.examDatePicker.Name = "examDatePicker";
            this.examDatePicker.Size = new System.Drawing.Size(151, 23);
            this.examDatePicker.TabIndex = 6;
            this.examDatePicker.Visible = false;
            // 
            // labelExanDate
            // 
            this.labelExanDate.AutoSize = true;
            this.labelExanDate.Location = new System.Drawing.Point(603, 13);
            this.labelExanDate.Name = "labelExanDate";
            this.labelExanDate.Size = new System.Drawing.Size(85, 18);
            this.labelExanDate.TabIndex = 5;
            this.labelExanDate.Text = "Exam. Date:";
            this.labelExanDate.Visible = false;
            // 
            // tabPageResults
            // 
            this.tabPageResults.Controls.Add(this.panelResults);
            this.tabPageResults.Font = new System.Drawing.Font("Trebuchet MS", 10F);
            this.tabPageResults.Location = new System.Drawing.Point(4, 33);
            this.tabPageResults.Name = "tabPageResults";
            this.tabPageResults.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageResults.Size = new System.Drawing.Size(855, 465);
            this.tabPageResults.TabIndex = 1;
            this.tabPageResults.Text = "Results Submission";
            this.tabPageResults.UseVisualStyleBackColor = true;
            // 
            // panelResults
            // 
            this.panelResults.BackColor = System.Drawing.Color.AliceBlue;
            this.panelResults.Controls.Add(this.panel4);
            this.panelResults.Controls.Add(this.studentsData);
            this.panelResults.Controls.Add(this.panelResultControls);
            this.panelResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelResults.Location = new System.Drawing.Point(3, 3);
            this.panelResults.Name = "panelResults";
            this.panelResults.Size = new System.Drawing.Size(849, 459);
            this.panelResults.TabIndex = 11;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.AliceBlue;
            this.panel4.Controls.Add(this.labelPerformance);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.labelUnits);
            this.panel4.Controls.Add(this.labelSemester);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.labelCourse);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(849, 40);
            this.panel4.TabIndex = 11;
            // 
            // labelPerformance
            // 
            this.labelPerformance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPerformance.AutoSize = true;
            this.labelPerformance.Location = new System.Drawing.Point(755, 11);
            this.labelPerformance.Name = "labelPerformance";
            this.labelPerformance.Size = new System.Drawing.Size(0, 18);
            this.labelPerformance.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(656, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 18);
            this.label7.TabIndex = 0;
            this.label7.Text = "Performance:";
            // 
            // labelUnits
            // 
            this.labelUnits.AutoSize = true;
            this.labelUnits.Location = new System.Drawing.Point(632, 11);
            this.labelUnits.Name = "labelUnits";
            this.labelUnits.Size = new System.Drawing.Size(0, 18);
            this.labelUnits.TabIndex = 0;
            // 
            // labelSemester
            // 
            this.labelSemester.AutoSize = true;
            this.labelSemester.Location = new System.Drawing.Point(523, 11);
            this.labelSemester.Name = "labelSemester";
            this.labelSemester.Size = new System.Drawing.Size(0, 18);
            this.labelSemester.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(587, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 18);
            this.label6.TabIndex = 0;
            this.label6.Text = "Units:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(455, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 18);
            this.label5.TabIndex = 0;
            this.label5.Text = "Semester:";
            // 
            // labelCourse
            // 
            this.labelCourse.AutoSize = true;
            this.labelCourse.Location = new System.Drawing.Point(67, 11);
            this.labelCourse.Name = "labelCourse";
            this.labelCourse.Size = new System.Drawing.Size(317, 18);
            this.labelCourse.TabIndex = 0;
            this.labelCourse.Text = "Click Import Marksheet to View Gradesheet Data";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Course:";
            // 
            // studentsData
            // 
            this.studentsData.AllowUserToAddRows = false;
            this.studentsData.AllowUserToDeleteRows = false;
            this.studentsData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.studentsData.BackgroundColor = System.Drawing.Color.White;
            this.studentsData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.studentsData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StudentSn,
            this.StudentName,
            this.RegNo,
            this.Dept,
            this.CAScore,
            this.ExamScore,
            this.TotalScore,
            this.Grade});
            this.studentsData.Location = new System.Drawing.Point(5, 46);
            this.studentsData.MaximumSize = new System.Drawing.Size(841, 365);
            this.studentsData.MinimumSize = new System.Drawing.Size(841, 365);
            this.studentsData.Name = "studentsData";
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentsData.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.studentsData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.studentsData.Size = new System.Drawing.Size(841, 365);
            this.studentsData.TabIndex = 10;
            this.studentsData.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.studentsData_CellBeginEdit);
            this.studentsData.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.studentsData_CellEndEdit);
            this.studentsData.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.studentsData_RowsAdded);
            this.studentsData.MouseMove += new System.Windows.Forms.MouseEventHandler(this.app_MouseMove);
            // 
            // StudentSn
            // 
            this.StudentSn.DividerWidth = 1;
            this.StudentSn.HeaderText = "SN.";
            this.StudentSn.MaxInputLength = 900;
            this.StudentSn.Name = "StudentSn";
            this.StudentSn.ReadOnly = true;
            this.StudentSn.ToolTipText = "Serial Number";
            this.StudentSn.Width = 40;
            // 
            // StudentName
            // 
            this.StudentName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StudentName.DividerWidth = 1;
            this.StudentName.FillWeight = 92.54926F;
            this.StudentName.HeaderText = "Name of Student";
            this.StudentName.Name = "StudentName";
            this.StudentName.ReadOnly = true;
            this.StudentName.ToolTipText = "Student Name";
            // 
            // RegNo
            // 
            this.RegNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.RegNo.DividerWidth = 1;
            this.RegNo.FillWeight = 30F;
            this.RegNo.HeaderText = "Reg. No.";
            this.RegNo.MinimumWidth = 15;
            this.RegNo.Name = "RegNo";
            this.RegNo.ReadOnly = true;
            this.RegNo.ToolTipText = "Student Registration Number";
            // 
            // Dept
            // 
            this.Dept.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Dept.DividerWidth = 1;
            this.Dept.FillWeight = 74.0394F;
            this.Dept.HeaderText = "Department";
            this.Dept.MinimumWidth = 50;
            this.Dept.Name = "Dept";
            this.Dept.ReadOnly = true;
            this.Dept.ToolTipText = "Student\'s Department";
            // 
            // CAScore
            // 
            this.CAScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CAScore.DividerWidth = 1;
            this.CAScore.FillWeight = 21.01911F;
            this.CAScore.HeaderText = "CA Mark";
            this.CAScore.MaxInputLength = 2;
            this.CAScore.MinimumWidth = 15;
            this.CAScore.Name = "CAScore";
            this.CAScore.ToolTipText = "Continous Assessment";
            // 
            // ExamScore
            // 
            this.ExamScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ExamScore.DividerWidth = 1;
            this.ExamScore.FillWeight = 21F;
            this.ExamScore.HeaderText = "Exam Mark";
            this.ExamScore.MaxInputLength = 2;
            this.ExamScore.MinimumWidth = 15;
            this.ExamScore.Name = "ExamScore";
            this.ExamScore.ToolTipText = "Examination Mark";
            // 
            // TotalScore
            // 
            this.TotalScore.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TotalScore.DividerWidth = 1;
            this.TotalScore.FillWeight = 21F;
            this.TotalScore.HeaderText = "Total";
            this.TotalScore.MaxInputLength = 3;
            this.TotalScore.MinimumWidth = 15;
            this.TotalScore.Name = "TotalScore";
            this.TotalScore.ReadOnly = true;
            this.TotalScore.ToolTipText = "Total Mark";
            // 
            // Grade
            // 
            this.Grade.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Grade.DividerWidth = 1;
            this.Grade.FillWeight = 21F;
            this.Grade.HeaderText = "Grade";
            this.Grade.MaxInputLength = 2;
            this.Grade.MinimumWidth = 15;
            this.Grade.Name = "Grade";
            this.Grade.ReadOnly = true;
            this.Grade.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Grade.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Grade.ToolTipText = "Result Letter Grade";
            // 
            // panelResultControls
            // 
            this.panelResultControls.BackColor = System.Drawing.Color.AliceBlue;
            this.panelResultControls.Controls.Add(this.flowLayoutPanel1);
            this.panelResultControls.Controls.Add(this.buttonClear);
            this.panelResultControls.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelResultControls.Location = new System.Drawing.Point(0, 409);
            this.panelResultControls.Name = "panelResultControls";
            this.panelResultControls.Size = new System.Drawing.Size(849, 50);
            this.panelResultControls.TabIndex = 9;
            this.panelResultControls.MouseMove += new System.Windows.Forms.MouseEventHandler(this.app_MouseMove);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.buttonUpload);
            this.flowLayoutPanel1.Controls.Add(this.buttonImport);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(407, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(442, 50);
            this.flowLayoutPanel1.TabIndex = 1;
            this.flowLayoutPanel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.app_MouseMove);
            // 
            // ofd
            // 
            this.ofd.DefaultExt = "xlsx";
            this.ofd.FileName = "Grade.xlsx";
            this.ofd.Filter = "Microsoft Excel Worksheet|*.xlsx|Microsoft Excel Macro-Enabled Worksheet|*.xlsm";
            this.ofd.RestoreDirectory = true;
            // 
            // sfd
            // 
            this.sfd.DefaultExt = "xlsx";
            this.sfd.Filter = "Microsoft Excel Worksheet|*.xlsx|Microsoft Excel Macro-Enabled Worksheet|*.xlsm|M" +
    "icrosoft Word Document|*.docx";
            this.sfd.RestoreDirectory = true;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.BalloonTipText = "A robust result management solution for Educational Institutions";
            this.notifyIcon1.BalloonTipTitle = "Automated Result Management System 1.0";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.BalloonTipClicked += new System.EventHandler(this.notifyIcon1_BalloonTipClicked);
            this.notifyIcon1.BalloonTipShown += new System.EventHandler(this.notifyIcon1_BalloonTipShown);
            // 
            // homeLecturer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(863, 564);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(879, 602);
            this.Name = "homeLecturer";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lecturer Control Dashboard";
            this.Load += new System.EventHandler(this.homeLecturer_Load);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.app_MouseMove);
            this.panel1.ResumeLayout(false);
            this.groupBoxMessages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.displayPic)).EndInit();
            this.panel2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridActivity)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPagePrinting.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tabPageResults.ResumeLayout(false);
            this.panelResults.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.studentsData)).EndInit();
            this.panelResultControls.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnChangePic;
        private System.Windows.Forms.PictureBox displayPic;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ToolStripButton btnLock;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.OpenFileDialog pictureDialog;
        private System.Windows.Forms.GroupBox groupBoxMessages;
        private System.Windows.Forms.Button buttonPreviousAnnounce;
        private System.Windows.Forms.Button buttonNextAnnounce;
        private System.Windows.Forms.Label labelWelcome;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPageResults;
        private System.Windows.Forms.ToolStripButton btnAddLecture;
        private System.Windows.Forms.ToolStripComboBox comboCourses;
        private System.Windows.Forms.DataGridView studentsData;
        private System.Windows.Forms.Panel panelResultControls;
        private System.Windows.Forms.Button buttonUpload;
        private System.Windows.Forms.Button buttonImport;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel panelResults;
        private System.Windows.Forms.TabPage tabPagePrinting;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.OpenFileDialog ofd;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelUnits;
        private System.Windows.Forms.Label labelSemester;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelCourse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelPerformance;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button buttonPrintReport;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox comboBoxCourseGen;
        private System.Windows.Forms.ComboBox comboBoxDataGen;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.Button buttonViewinExcel;
        private System.Windows.Forms.Label labelGenStatus;
        private System.Windows.Forms.Button buttonSaveAs;
        private System.Windows.Forms.SaveFileDialog sfd;
        private System.Windows.Forms.Label labelExanDate;
        private System.Windows.Forms.DateTimePicker examDatePicker;
        private MessageDisplay messageDisplay1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ListView listViewStudents;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ComboBox comboBoxDepartment;
        private System.Windows.Forms.DataGridView dataGridActivity;
        private System.Windows.Forms.DataGridViewTextBoxColumn activityDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn activityText;
        private System.Windows.Forms.RichTextBox richTextBoxAnnouncements;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudentSn;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudentName;
        private System.Windows.Forms.DataGridViewTextBoxColumn RegNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dept;
        private System.Windows.Forms.DataGridViewTextBoxColumn CAScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExamScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grade;
        private System.Windows.Forms.Label labelMode;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ToolStripButton btnEditPassword;
        private System.Windows.Forms.ToolStripButton toolBtnAbout;
        private System.Windows.Forms.ToolStripComboBox sessionTool;
    }
}

