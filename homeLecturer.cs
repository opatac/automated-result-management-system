using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Xml.Serialization;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.Office.Interop.Excel;
using ARMS.Properties;
using Application = System.Windows.Forms.Application;
using Color = System.Drawing.Color;
using Font = System.Drawing.Font;
using Logman = ARMS.Logger;
using Workbook = Microsoft.Office.Interop.Excel.Workbook;
using Worksheet = Microsoft.Office.Interop.Excel.Worksheet;

namespace ARMS
{
    public partial class homeLecturer : Form
    {
        #region Private Variable Declarations

        private LecturerDetails _currentLecturer;
        private readonly bool _local;
        private List<Activity> _acts = new List<Activity>();
        private int announceIndex;
        private List<string> announcements = new List<string>();
        private string _cDepartment;
        private int cellNo = 12;
        private string courseCode;
        private int crowTScore;
        private int incompleteCount;

        private SerializableDictionary<string, LecturerCourse> mycourses =
            new SerializableDictionary<string, LecturerCourse>();

        private Dictionary<string, Cell> resultCells;
        private bool resultChangesMade = false;
        private SpreadsheetDocument resultDoc;
        private int rowCount;
        private string savedFileName;
        private SoundPlayer sp;
        private int studentTotalScores;
        private string tempName;
        private UserDetails ud = new UserDetails();
        private int unreadCount;
        private bool notificationIsMessage;//true if is message, false if else

        #endregion

        public homeLecturer(LecturerDetails lcd, bool local)
        {
            this._local = local;
            InitializeComponent();
            this.Lcd = lcd;
        }

        public LecturerDetails Lcd
        { get; set; }

        void app_MouseMove(object sender, MouseEventArgs e)
        {
            timerCount = 0;
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            var lc = new Locked(_currentLecturer.UserName);
            Visible = false;
            lc.UnlockEvent += (o, args) => { Visible = args.Unlocked; timerCount = 0; };
            lc.ShowDialog();
        }

        private void btnChangePic_Click(object sender, EventArgs e)
        {
            pictureDialog.FileOk += (pictureDialog_FileOk);
            pictureDialog.ShowDialog();
        }

        private void pictureDialog_FileOk(object sender, CancelEventArgs e)
        {
            displayPic.ImageLocation = pictureDialog.FileName;
            Helper.rk.SetValue("Picture", pictureDialog.FileName);
            if (!_local)
            {
                try
                {
                    //create WebClient object
                    var client = new WebClient();
                    string myFile = pictureDialog.FileName;
                    client.Credentials = CredentialCache.DefaultCredentials;
                    client.UploadFile(@"http://localhost:8080/ecampusPortal/uploader.php", myFile);
                    client.Dispose();
                    Helper.Update("lecturers", new[] {"display_image"},
                                  new[] {"http://localhost:8080/ecampusPortal/" + pictureDialog.SafeFileName},
                                  new[] {"username"},
                                  new[] {_currentLecturer.UserName}, "");
                }
                catch (Exception ex)
                {
                    Logman.WriteLog(ex);
                }
            }
        }

        private void clearStudentData()
        {
            studentTotalScores = 0;
            studentsData.Rows.Clear();
            courseCode = labelCourse.Text = labelUnits.Text = labelSemester.Text = "";
            rowCount = 0;
            cellNo = 12;
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            clearStudentData();
            ofd.Title = comboCourses.Text == "Select Course"
                            ? "Select a result grade sheet to upload."
                            : string.Format("Select result marksheet for {0}", comboCourses.Text);
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    resultCells = Helper.getRowCells(ofd.FileName, "course (1)");
                    resultDoc = Helper.GetSheetDoc(ofd.FileName);
                    if (resultCells != null)
                    {
                        courseCode = getCellValue("D7");
                        if (!comboCourses.Items.Contains(courseCode))
                        {
                            DialogResult dr =
                                MessageBox.Show(
                                    string.Format(
                                        "You are trying to upload the grade sheet for '{0}'. But '{0}' is not among the courses you teach. Please ensure you have selected the correct file and try again.",
                                        courseCode), "Result Sheet Mismatch", MessageBoxButtons.RetryCancel,
                                    MessageBoxIcon.Error);

                            if (dr == DialogResult.Retry)
                            {
                                buttonImport.PerformClick();
                            }
                            return;
                        }
                        comboCourses.Text = courseCode;
                        labelCourse.Text = comboCourses.Text;
                        _cDepartment = getCellValue("B7");
                        buttonUpload.Enabled = true;
                        loadCourse(courseCode);

                        if (!mycourses.ContainsKey(courseCode + ":" + _cDepartment))
                        {
                            DialogResult dr =
                                MessageBox.Show(
                                    string.Format(
                                        "The department on the gradesheet is not among the departments assigned for this course to you. Please ensure you have selected  the correct file and try again.",
                                        courseCode), "Result Sheet Mismatch", MessageBoxButtons.RetryCancel,
                                    MessageBoxIcon.Error);
                            if (dr == DialogResult.Retry)
                            {
                                buttonImport.PerformClick();
                            }
                            return;
                        }

                        labelCourse.Text = getCellValue("B5");
                        labelUnits.Text = getCellValue("I7");
                        labelSemester.Text = getCellValue("D8");

                        List<string> students = Helper.selectResults(new[] {"student_courses.regNo as regNo"},
                                                                     "student_courses",
                                                                     new[]
                                                                         {"CourseCode", "academicYear", "students.dept"},
                                                                     new[]
                                                                         {courseCode, sessionTool.Text, _cDepartment},
                                                                     "JOIN students on student_courses.regNo = students.regNo")
                                                      .Select(n => n[0])
                                                      .ToList();

                        StudentData dData = getNextName();
                        incompleteCount = 0;
                        string incompat = "";
                        while (dData != null)
                        {
                            if (!students.Contains(dData.RegNo))
                            {
                                incompat += dData.RegNo + ",";
                                dData = getNextName();
                                continue;
                            }
                            studentsData.Rows.Add(new[]
                                {
                                    (studentsData.RowCount + 1).ToString(), dData.StudentName, dData.RegNo, dData.Dept,
                                    dData.CaMark, dData.ExamMark, dData.TotalMark, dData.GradeLetter
                                });
                            studentTotalScores += (int) dData.TotalScore;
                            studentsData.Update();
                            dData = getNextName();
                        }
                        if (incompat != "")
                        {
                            Logman.WriteLog("Error incompatible students from " + _currentLecturer.UserName, incompat);
                            Logman.SendLog("Error incompatible students from " + _currentLecturer.UserName, incompat);
                            MessageBox.Show(
                                "The following students were excluded from the results because they are not registered for the course or are listed in the wrong departments. If this is an error, please contact the admin. The error has been logged. The affected reg nos. are:\n" +
                                String.Join("\n", incompat.Split(',')), "Incompatible Students Found",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        if (incompleteCount > 0)
                        {
                            MessageBox.Show(
                                incompleteCount +
                                " incomplete results were found in the uploaded marksheet. Please correct the errors and try again. The incomplete results have been highlighted.",
                                "Incomplete Results Found", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                            buttonUpload.Enabled = false;
                            incompleteCount = 0;
                        }
                    }
                    int prating = studentTotalScores/(studentsData.RowCount);
                    labelPerformance.Text = Helper.GetRating(prating);
                }
                catch (Exception ex)
                {
                    Logman.WriteLog(ex, true);
                    DialogResult dr =
                        MessageBox.Show("An error occurred trying to import the items file. Please try again.",
                                        "Error Occurred", MessageBoxButtons.RetryCancel);
                    if (dr == DialogResult.Retry)
                    {
                        buttonImport.PerformClick();
                    }
                }
                finally
                {
                    if (resultDoc != null) resultDoc.Dispose();
                }
            }
        }

        private string getCellValue(string cellLetter)
        {
            return Helper.cellByReference(resultCells, resultDoc, cellLetter);
        }

        private StudentData getNextName()
        {
            string sName = getCellValue("B" + cellNo);
            if (sName != null)
            {
                var dData = new StudentData
                    {
                        StudentName = sName,
                        RegNo = getCellValue("C" + cellNo),
                        Dept = getCellValue("D" + cellNo),
                        CaMark = getCellValue("F" + cellNo),
                        ExamMark = getCellValue("G" + cellNo)
                    };
                if (rowCount == 29)
                {
                    rowCount = 0;
                    cellNo += 30;
                }
                else
                {
                    rowCount++;
                    cellNo++;
                }
                return dData;
            }
            return null;
        }

        private void buttonUpload_Click(object sender, EventArgs e)
        {
            if (_local)
            {
                DialogResult dr = MessageBox.Show(
                    "You are not logged in online mode. To submit results, Do you want to try logging in online mode?",
                    "Local Mode", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (dr == DialogResult.Yes)
                {
                    Close();
                }
                else
                {
                    return;
                }
            }
            if (studentsData.Rows.Count == 0)
            {
                MessageBox.Show(
                    "You have not imported any student's results yet. Please import a completed gradesheet first and try again. If you need help with this, please contact the administrator on (Admin Number). Thanks.",
                    "No Results to Submit", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            else
            {
                if (resultChangesMade)
                {
                    /*
                                        var dr = MessageBox.Show(
                                            "You have made some changes to the result. Do you want to generate a new grade sheet with the new changes?",
                                            "Re-generate Grade Sheet", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                                        if (dr == DialogResult.OK)
                                        {
                        
                                        }
                    */
                }

                var students = new List<StudentData>();
                foreach (DataGridViewRow row in studentsData.Rows)
                {
                    //Add only the necessary data.
                    students.Add(new StudentData
                        {
                            CaMark = row.Cells["CAScore"].Value.ToString(),
                            ExamMark = row.Cells["ExamScore"].Value.ToString(),
                            RegNo = row.Cells["RegNo"].Value.ToString()
                        });
                }
                int results = 0;
                switch (buttonUpload.Text)
                {
                    case "Re&Submit":
                        results = Helper.UploadResults(students, comboCourses.Text, sessionTool.Text, true,
                                                       _cDepartment);
                        break;
                    case "&Submit":
                        results = Helper.UploadResults(students, comboCourses.Text, sessionTool.Text, false,
                                                       _cDepartment);
                        break;
                }

                if (results == students.Count)
                {
                    int logresult =
                        Helper.logActivity(new Activity
                            {
                                Department = _currentLecturer.Department,
                                Faculty = _currentLecturer.Faculty,
                                Text =
                                    string.Format("{0} uploaded {1} results. Performance Rating: {2}.",
                                                  _currentLecturer.NiceName, comboCourses.Text, labelPerformance.Text),
                                Username = _currentLecturer.UserName
                            });
                    MessageBox.Show(
                        string.Format("The results for {0} has been uploaded succesfully.", comboCourses.Text),
                        "Results Uploaded Successfully", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    buttonClear.PerformClick();
                }
                else
                {
                    MessageBox.Show(
                        "An error occured while trying to upload the imported results. Please try again. If problem persists, please contact the administrator. Thanks");
                }
            }
        }

        private readonly string userPath = Application.StartupPath + "\\userdetails.xml";

        private void loadLocalData()
        {
            ud = (UserDetails) Helper.Deserialize(userPath, typeof (UserDetails));
        }

        /// <summary>
        /// Serializes and stores all data using the UserDetails model
        /// </summary>
        private void saveLocalData()
        {
            if (_local) return;
            Type type = typeof (UserDetails);
            Helper.SerializeData(type, userPath, ud);
        }

        private void homeLecturer_Load(object sender, EventArgs e)
        {
            tt = new Timer{Interval = 100, Enabled = true};
            tt.Tick += LoadForm;
        }

        private Timer tt;

        private void LoadForm(object state, EventArgs eventArgs)
        {
            tt.Enabled = false;
            labelMode.Text = _local ? "Local Mode" : "Online";
            //Just to rectify bug with data grid
            studentsData.MaximumSize = new Size(0, 0);
            _currentLecturer = Lcd;
            labelWelcome.Text = string.Format("Good day {0}.", Lcd.NiceName);
            studentsData.Font = new Font(studentsData.Font.Name, 9F);

            this.MouseMove += app_MouseMove;
            var pickey = Helper.rk.GetValue("Picture");
            if (_local)
            {
                loadLocalData();
            }
            if (Helper.rk != null &&  pickey!= null && pickey.ToString() != "")
            {
                try
                {
                    displayPic.ImageLocation = Helper.rk.GetValue("Picture").ToString();
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(ex);
                }
            }
            LoadCourses(true);
            LoadAnnouncements(false);
            LoadActivities(false);
            for(int i = DateTime.Now.Year + 1; i > 1960; i--)
            {
                sessionTool.Items.Add((i - 1) + "/" + i);
            }
            saveLocalData();
            timer1.Enabled = true;
        }

        /// <summary>
        /// Loads activities for the current lecturer
        /// </summary>
        /// <param name="notify">Set to true to let the announcement pop up in the notification ballooon.</param>
        private void LoadActivities(bool notify)
        {
            List<Activity> acs = Helper.GetActivities("department", _currentLecturer.Department);
            foreach (Activity ac in acs)
            {
                if (ac.IsIn(_acts))
                {
                    continue;
                }
                _acts.Add(ac);
                dataGridActivity.Rows.Add(new[] {ac.ActivityDate.ToShortDateString(), ac.Text});
                if (notify)
                {
                    notifyIcon1.ShowBalloonTip(3000, "New Activity", ac.Text, ToolTipIcon.Info);
                    notificationIsMessage = false;
                }
            }
            if (_local)
            {
                _acts = ud.Activities;
            }
            else
            {
                _acts = acs;
                ud.Activities = _acts;
            }
        }

        /// <summary>
        /// Loads announcements for the lecturer
        /// </summary>
        /// <param name="notify">True to indicate for the announcement to pop up in the notification balloon</param>
        private void LoadAnnouncements(bool notify)
        {
            if (_local)
            {
                announcements = ud.Announcements;
            }
            else
            {
                foreach (
                    string an in Helper.GetAnnouncements("department", _currentLecturer.Department, UserRoles.Lecturer))
                {
                    if (announcements.Contains(an))
                    {
                        continue;
                    }
                    announcements.Add(an);
                    if (notify)
                    {
                        notifyIcon1.ShowBalloonTip(3000, "New Annoucement Received", an.StripRTF().Excerpt(40), ToolTipIcon.Info);
                        notificationIsMessage = false;
                    }
                }
                ud.Announcements = announcements;
            }
            if (announcements.Count == 0)
            {
                buttonPreviousAnnounce.Enabled = false;
            }
            else
            {
                announceIndex = announcements.Count - 1;
                try
                {
                    richTextBoxAnnouncements.Rtf = announcements[announceIndex];
                }
                catch (ArgumentException)
                {
                    richTextBoxAnnouncements.Text = announcements[announceIndex].StripRTF();
                }


                buttonPreviousAnnounce.Enabled = announcements.Count > 1;
            }
            buttonNextAnnounce.Enabled = false;
        }

        /// <summary>
        ///     Loads courses for the current lecturer
        /// </summary>
        /// <param name="loadUI">Specifies if the course drop downs should be updated.</param>
        private void LoadCourses(bool loadUI)
        {
            if (_local)
            {
                mycourses = ud.Courses;
            }
            else
            {
                mycourses = Helper.GetLecturerCourses(_currentLecturer.UserName, false);
                ud.Courses = mycourses;
            }

            if (!loadUI) return;
            foreach (var course in mycourses)
            {
                string[] arr = course.Key.Split(':');
                string key = arr[0];
                if (!comboCourses.Items.Contains(key))
                {
                    comboCourses.Items.Add(key);
                    comboBoxCourseGen.Items.Add(key);
                }
            }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            studentsData.Rows.Clear();
            labelPerformance.ResetText();
            labelCourse.ResetText();
        }

        private void studentsData_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            DataGridViewRow cRow = studentsData.Rows[e.RowIndex];
            int tscore = Convert.ToInt32(cRow.Cells["TotalScore"].Value);
            if (tscore < 40)
            {
                if (cRow.Cells["TotalScore"].Value == null)
                {
                    incompleteCount++;
                }
                cRow.DefaultCellStyle.ForeColor = Color.Red;
            }
        }

        private void studentsData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow cRow = studentsData.Rows[e.RowIndex];
            try
            {
                int totalScore = Convert.ToInt32(cRow.Cells["CAScore"].Value) +
                                 Convert.ToInt32(cRow.Cells["ExamScore"].Value);
                if (crowTScore == totalScore)
                {
                    return; //No need to update anything since total score didn't change
                }
                string lgrade = StudentData.ComputeGrade(totalScore);

                studentTotalScores -= crowTScore += totalScore;
                int prating = studentTotalScores/(studentsData.RowCount);
                labelPerformance.Text = Helper.GetRating(prating);

                cRow.Cells["TotalScore"].Value = totalScore;
                cRow.Cells["Grade"].Value = lgrade;
                if (totalScore < 40)
                {
                    cRow.DefaultCellStyle.ForeColor = Color.Red;
                }
                else
                {
                    cRow.DefaultCellStyle = studentsData.DefaultCellStyle;
                }
            }
            catch (Exception ex)
            {
                studentsData.CancelEdit();
                Logman.WriteLog(ex.Source + ex.Message, ex.StackTrace);
                MessageBox.Show("It seems there was an error in your input. Please try again.", "Incorrect Input",
                                MessageBoxButtons.OK);
            }
        }

        private void tabPagePrinting_Enter(object sender, EventArgs e)
        {
            comboBoxCourseGen.Text = comboCourses.Text;
        }

        private void generateGradeReport(string courseCode)
        {
            if (courseCode == "Select Course")
            {
                MessageBox.Show("You have not selected a course yet. Please select a course and try again.",
                                "No Course Selected", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            savedFileName = null;
            listViewStudents.Items.Clear();
            string[][] sresults;
            string[] courseDetails = null;
            string courseDuration = null;
            if (_local)
            {
                if (ud.CourseDetails.Count > 0)
                {
                    courseDetails = ud.CourseDetails[courseCode];
                }
                if (ud.CourseDuration.Count > 0)
                {
                    courseDuration = ud.CourseDuration[comboBoxDepartment.Text];
                }
            }
            else
            {
                courseDetails = Helper.selectResults(new[] {"CourseTitle", "semester", "units"}, "courses",
                                                     new[] {"CourseCode"}, new[] {courseCode}, "")[0];
                if (ud.CourseDetails.ContainsKey(courseCode))
                {
                    ud.CourseDetails[courseCode] = courseDetails;
                }
                else
                {
                    ud.CourseDetails.Add(courseCode, courseDetails);
                }
                courseDuration =
                    Helper.selectResults(new[] {"courseDuration"}, "departments", new[] {"deptName"},
                                         new[] {comboBoxDepartment.Text}, "")[0][0];
                if (ud.CourseDuration.ContainsKey(comboBoxDepartment.Text))
                {
                    ud.CourseDuration[comboBoxDepartment.Text] = courseDuration;
                }
                else
                {
                    ud.CourseDuration.Add(comboBoxDepartment.Text, courseDuration);
                }
            }

            var theStudents = new List<StudentData>();
            try
            {
                if (_local)
                {
                    for (int index = 0; index < ud.CourseData[courseCode].Count; index++)
                    {
                        StudentData sd = ud.CourseData[courseCode][index];
                        var lvi =
                            new ListViewItem(new[]
                                {sd.StudentName, sd.RegNo, sd.DeptShortName, sd.Level + "/" + courseDuration});
                        lvi.SubItems.Insert(0, new ListViewItem.ListViewSubItem(lvi, (index + 1).ToString()));
                        listViewStudents.Items.Add(lvi);
                        theStudents.Add(sd);
                    }
                }
                else
                {
                    labelGenStatus.Text = "Retrieving information for " + courseCode;

                    labelGenStatus.Text = "Retrieving student registration data for " + courseCode;

                    string[][] students = Helper.selectResults(new[] {"student_courses.regNo as regNo"},
                                                               "student_courses",
                                                               new[] {"CourseCode", "academicYear", "students.dept"},
                                                               new[]
                                                                   {
                                                                       courseCode, sessionTool.Text,
                                                                       comboBoxDepartment.Text
                                                                   },
                                                               "JOIN students on student_courses.regNo = students.regNo");

                    labelGenStatus.Text = string.Format("Retrieving data for {0} registered students...",
                                                        students.Length);

                    for (int i = 0; i < students.Length; i++)
                    {
                        string reg = students[i][0];
                        sresults =
                            Helper.selectResults(
                                new[]
                                    {
                                        "CONCAT(lName, ' ', fName, ' ', mName) as studentName", "regNo", "deptShortName"
                                        ,
                                        "level"
                                    },
                                "students", new[] {"regNo"}, new[] {reg}, "");
                        sresults[0][3] = sresults[0][3][0] + "/" + courseDuration;
                        var lvi = new ListViewItem(sresults[0]);
                        theStudents.Add(new StudentData
                            {
                                StudentName = sresults[0][0],
                                RegNo = sresults[0][1],
                                DeptShortName = sresults[0][2],
                                Level = sresults[0][3]
                            });
                        lvi.SubItems.Insert(0, new ListViewItem.ListViewSubItem(lvi, (i + 1).ToString()));
                        listViewStudents.Items.Add(lvi);

                        listViewStudents.Update();
                    }

                    theStudents = theStudents.OrderBy(n => n.StudentName).ToList();
                    if (ud.CourseData.ContainsKey(courseCode))
                    {
                        ud.CourseData[courseCode] = theStudents;
                    }
                    else
                    {
                        ud.CourseData.Add(courseCode, theStudents);
                    }

                    labelGenStatus.Text = "Data Retrieval Complete. Generating Report Template...";
                }
                if (theStudents.Count == 0)
                {
                    MessageBox.Show(
                        "It seems there are no students currently registered for this course. Please ensure you have selected the proper course and try again.",
                        "No Students Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }


                tempName = Application.UserAppDataPath + "\\tempgen.xlsx";

                if (File.Exists(tempName))
                {
                    DialogResult xd =
                        MessageBox.Show(
                            "It seems this file has already been generated. Press Cancel to cancel this operation and open the exiting file? or Press OK to overwrite the file.",
                            "File Already Exists", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                    if (xd == DialogResult.OK)
                    {
                        try
                        {
                            File.Delete(tempName);
                        }
                        catch
                        {
                            foreach (Process p in Process.GetProcessesByName("excel"))
                            {
                                p.Kill();
                            }
                            File.Delete(tempName);
                        }
                    }
                    else
                    {
                        buttonViewinExcel.PerformClick();
                        labelGenStatus.Text = "Generation Complete. You may now print or open.";
                        return;
                    }
                }
                labelGenStatus.Update();

                var gg = new GradeSheetGen();
                gg.CreatePackage(tempName, courseDetails[0].ToUpper(), examDatePicker.Text.ToUpper(),
                                 comboBoxDepartment.Text,
                                 courseCode, courseDetails[2].ToUpper(), _currentLecturer.Faculty,
                                 courseDetails[1] == "1" ? "FIRST" : "SECOND", sessionTool.Text,
                                 _currentLecturer.NiceName.ToUpper());

                labelGenStatus.Text = "Template generated. Inserting Student Data. Please wait...";
                labelGenStatus.Update();

                Helper.InsertStudenData(tempName, theStudents, "course (1)", "A", 12, 5, 30, true);

                labelGenStatus.Text = "Please choose a location to save the file.";
                labelGenStatus.Update();

                buttonSaveAs.PerformClick();

                labelGenStatus.Text = "Generation Complete. You may now print or open.";
                Logman.SendLog(_currentLecturer.UserName,
                               string.Format("Generated gradesheet for {0} for Department of {1}.", courseCode,
                                             comboBoxDepartment.Text));
            }
            catch (Exception ex)
            {
                Logman.WriteLog(ex.Source, ex.StackTrace);
                MessageBox.Show(
                    "An error occured while tyring to retrieve the values from the database. Make sure you are currently connected and try again.",
                    "Data Generation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            switch (comboBoxDataGen.Text)
            {
                case "Exam Attendance Register":
                    generateExamAttandance(comboBoxCourseGen.Text);
                    break;
                case "Official Grade Report Template":
                    generateGradeReport(comboBoxCourseGen.Text);
                    break;
                case "Class Attendance Template":
                    generateClassAttandance(comboBoxCourseGen.Text);
                    break;
                default:
                    MessageBox.Show(
                        "You have not choosen any data to generate. Please specify the type of data you want to generate and try again.",
                        "Data Unspecified", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            }
            saveLocalData();
        }

        private void generateClassAttandance(string courseCode)
        {
            if (courseCode == "Select Course")
            {
                MessageBox.Show("You have not selected a course yet. Please select a course and try again.",
                                "No Course Selected", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            savedFileName = null;
            listViewStudents.Items.Clear();
            string[][] sresults;
            var theStudents = new List<StudentData>();
            string[] courseDetails;
            string cLevel = courseCode.Substring(courseCode.Length - 3, 1);
            if (_local)
            {
                courseDetails = ud.CourseDetails[courseCode];
            }
            else
            {
                courseDetails = Helper.selectResults(new[] {"CourseTitle", "semester", "units"}, "courses",
                                                     new[] {"CourseCode"}, new[] {courseCode}, "")[0];
                if (ud.CourseDetails.ContainsKey(courseCode))
                {
                    ud.CourseDetails[courseCode] = courseDetails;
                }
                else
                {
                    ud.CourseDetails.Add(courseCode, courseDetails);
                }
            }
            if (_local)
            {
                for (int index = 0; index < ud.CourseData[courseCode].Count; index++)
                {
                    StudentData sd = ud.CourseData[courseCode][index];
                    var lvi = new ListViewItem(new[] {sd.StudentName, sd.RegNo, sd.Dept, sd.Level});
                    lvi.SubItems.Insert(0, new ListViewItem.ListViewSubItem(lvi, (index + 1).ToString()));
                    listViewStudents.Items.Add(lvi);
                    theStudents.Add(sd);
                }
            }
            else
            {
                try
                {
                    string[][] students = Helper.selectResults(new[] {"student_courses.regNo as regNo"},
                                                               "student_courses",
                                                               new[]
                                                                   {
                                                                       "student_courses.CourseCode", "academicYear",
                                                                       "students.dept"
                                                                   },
                                                               new[]
                                                                   {
                                                                       courseCode, sessionTool.Text,
                                                                       comboBoxDepartment.Text
                                                                   },
                                                               "JOIN students on student_courses.regNo = students.regNo");

                    labelGenStatus.Text = string.Format("Retrieving data for {0} registered students...",
                                                        students.Length);

                    for (int i = 0; i < students.Length; i++)
                    {
                        string reg = students[i][0];
                        sresults =
                            Helper.selectResults(
                                new[]
                                    {
                                        "CONCAT(lName, ' ', fName, ' ', mName) as studentName", "regNo", "dept", "level"
                                        ,
                                        "deptShortName"
                                    },
                                "students", new[] {"regNo"}, new[] {reg}, "");
                        sresults[0][3] = sresults[0][3][0] + "/" + cLevel;
                        var lvi = new ListViewItem(sresults[0]);
                        theStudents.Add(new StudentData
                            {
                                StudentName = sresults[0][0],
                                RegNo = sresults[0][1],
                                Dept = sresults[0][2],
                                Level = sresults[0][3],
                                DeptShortName = sresults[0][4]
                            });
                        lvi.SubItems.Insert(0, new ListViewItem.ListViewSubItem(lvi, (i + 1).ToString()));
                        listViewStudents.Items.Add(lvi);

                        listViewStudents.Update();
                    }
                    theStudents = theStudents.OrderBy(n => n.StudentName).ToList();

                    if (ud.CourseData.ContainsKey(courseCode))
                    {
                        ud.CourseData[courseCode] = theStudents;
                    }
                    else
                    {
                        ud.CourseData.Add(courseCode, theStudents);
                    }
                }
                catch (Exception ex)
                {
                    Logman.WriteLog(ex.Source, ex.StackTrace);
                    MessageBox.Show(
                        "An error occured while tyring to retrieve the values from the database. Make sure you are currently connected and try again.",
                        "Data Generation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if (theStudents.Count == 0)
            {
                MessageBox.Show(
                    "It seems there are no students currently registered for this course. Please ensure you have selected the proper course and try again.",
                    "No Students Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            labelGenStatus.Text = "Data Retrieval Complete. Generating Report Template...";

            tempName = Application.UserAppDataPath + "\\tempexam.xlsx";

            if (File.Exists(tempName))
            {
                DialogResult xd =
                    MessageBox.Show(
                        "It seems this file has already been generated. Press Cancel to cancel this operation and open the exiting file? or Press OK to overwrite the file.",
                        "File Already Exists", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (xd == DialogResult.OK)
                {
                    try
                    {
                        File.Delete(tempName);
                    }
                    catch
                    {
                        foreach (Process p in Process.GetProcessesByName("excel"))
                        {
                            p.Kill();
                        }
                        File.Delete(tempName);
                    }
                }
                else
                {
                    buttonViewinExcel.PerformClick();
                    labelGenStatus.Text = "Generation Complete. You may now print or open.";
                    return;
                }
            }
            labelGenStatus.Update();

            var cag = new ClassAttendanceGen();

            string semester = courseDetails[0] == "1" ? "1ST" : "2ND";
            cag.CreatePackage(tempName, _currentLecturer.NiceName, "COURSE: " + courseCode,
                              "SESSION: " + sessionTool.Text, "SEMESTER: " + semester,
                              "LEVEL: " + cLevel + "00", comboBoxDepartment.Text);

            labelGenStatus.Text = "Template generated. Inserting Student Data. Please wait...";
            labelGenStatus.Update();

            Helper.InsertStudenData(tempName, theStudents, "Sheet1", "A", 5, 2, 0, true);

            labelGenStatus.Text = "Please choose a location to save the file.";
            labelGenStatus.Update();

            buttonSaveAs.PerformClick();

            labelGenStatus.Text = "Generation Complete. You may now print or open.";
        }

        private void generateExamAttandance(string courseCode)
        {
            if (courseCode == "Select Course")
            {
                MessageBox.Show("You have not selected a course yet. Please select a course and try again.",
                                "No Course Selected", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            savedFileName = null;
            listViewStudents.Items.Clear();
            string[][] sresults;
            string[] courseDetails;
            if (_local)
            {
                courseDetails = ud.CourseDetails[courseCode];
            }
            else
            {
                courseDetails = Helper.selectResults(new[] {"CourseTitle", "semester", "units"}, "courses",
                                                     new[] {"CourseCode"}, new[] {courseCode}, "")[0];
                if (ud.CourseDetails.ContainsKey(courseCode))
                {
                    ud.CourseDetails[courseCode] = courseDetails;
                }
                else
                {
                    ud.CourseDetails.Add(courseCode, courseDetails);
                }
            }
            var theStudents = new List<StudentData>();
            if (_local)
            {
                for (int index = 0; index < ud.CourseData[courseCode].Count; index++)
                {
                    StudentData sd = ud.CourseData[courseCode][index];
                    var lvi = new ListViewItem(new[] {sd.StudentName, sd.RegNo, sd.Dept, sd.Level});
                    lvi.SubItems.Insert(0, new ListViewItem.ListViewSubItem(lvi, (index + 1).ToString()));
                    listViewStudents.Items.Add(lvi);
                    theStudents.Add(sd);
                }
            }
            else
            {
                try
                {
                    labelGenStatus.Text = "Retrieving information for " + courseCode;

                    courseDetails = Helper.selectResults(new[] {"CourseTitle", "semester", "units"},
                                                         "courses", new[] {"CourseCode"}, new[] {courseCode},
                                                         "")[0];

                    labelGenStatus.Text = "Retrieving student registration data for " + courseCode;

                    string[][] students = Helper.selectResults(new[] {"student_courses.regNo as regNo"},
                                                               "student_courses",
                                                               new[] {"CourseCode", "academicYear", "students.dept"},
                                                               new[]
                                                                   {
                                                                       courseCode, sessionTool.Text,
                                                                       comboBoxDepartment.Text
                                                                   },
                                                               "JOIN students on student_courses.regNo = students.regNo");

                    labelGenStatus.Text = string.Format("Retrieving data for {0} registered students...",
                                                        students.Length);

                    for (int i = 0; i < students.Length; i++)
                    {
                        string reg = students[i][0];
                        sresults =
                            Helper.selectResults(
                                new[] {"CONCAT(lName, ' ', fName, ' ', mName) as studentName", "regNo", "dept"},
                                "students", new[] {"regNo"}, new[] {reg}, "");
                        var lvi = new ListViewItem(sresults[0]);
                        theStudents.Add(new StudentData
                            {
                                StudentName = sresults[0][0],
                                RegNo = sresults[0][1],
                                Dept = sresults[0][2]
                            });
                        lvi.SubItems.Insert(0, new ListViewItem.ListViewSubItem(lvi, (i + 1).ToString()));
                        listViewStudents.Items.Add(lvi);

                        listViewStudents.Update();
                    }
                    theStudents = theStudents.OrderBy(n => n.StudentName).ToList();

                    if (ud.CourseData.ContainsKey(courseCode))
                    {
                        ud.CourseData[courseCode] = theStudents;
                    }
                    else
                    {
                        ud.CourseData.Add(courseCode, theStudents);
                    }
                }
                catch (Exception ex)
                {
                    Logman.WriteLog(ex.Source, ex.StackTrace);
                    MessageBox.Show(
                        "An error occured while tyring to retrieve the values from the database. Make sure you are currently connected and try again.",
                        "Data Generation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            if (theStudents.Count == 0)
            {
                MessageBox.Show(
                    "It seems there are no students currently registered for this course. Please ensure you have selected the proper course and try again.",
                    "No Students Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            labelGenStatus.Text = "Data Retrieval Complete. Generating Report Template...";

            tempName = Application.UserAppDataPath + "\\tempexam.xlsx";

            if (File.Exists(tempName))
            {
                DialogResult xd =
                    MessageBox.Show(
                        "It seems this file has already been generated. Press Cancel to cancel this operation and open the exiting file? or Press OK to overwrite the file.",
                        "File Already Exists", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (xd == DialogResult.OK)
                {
                    try
                    {
                        File.Delete(tempName);
                    }
                    catch
                    {
                        foreach (Process p in Process.GetProcessesByName("excel"))
                        {
                            p.Kill();
                        }
                        File.Delete(tempName);
                    }
                }
                else
                {
                    buttonViewinExcel.PerformClick();
                    labelGenStatus.Text = "Generation Complete. You may now print or open.";
                    return;
                }
            }

            labelGenStatus.Update();

            var eag = new ExamAttendanceGen();
            eag.CreatePackage(tempName, "DEPARTMENT OF " + comboBoxDepartment.Text,
                              sessionTool.Text + "Course Registration Form/Class List", sessionTool.Text,
                              courseDetails[1] == "1" ? "FIRST" : "SECOND",
                              courseCode + ": " + courseDetails[0]);

            labelGenStatus.Text = "Template generated. Inserting Student Data. Please wait...";
            labelGenStatus.Update();

            Helper.InsertStudenData(tempName, theStudents, "Sheet1", "A", 6, 3, 0, true);

            labelGenStatus.Text = "Please choose a location to save the file.";
            labelGenStatus.Update();

            buttonSaveAs.PerformClick();

            labelGenStatus.Text = "Generation Complete. You may now print or open.";
        }

        private void buttonSaveAs_Click(object sender, EventArgs e)
        {
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                saveFile(sfd.FileName);
            }
        }

        private void saveFile(string fileName)
        {
            try
            {
                if (File.Exists(fileName))
                {
                    if (savedFileName == fileName)
                    {
                        DialogResult dd =
                            MessageBox.Show(
                                "This file has already been saved. Click Ok to open the file, or Click Cancel to abort.",
                                "File Already Exists", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                        if (dd == DialogResult.OK)
                        {
                            buttonViewinExcel.PerformClick();
                            return;
                        }
                    }
                    else
                    {
                        File.Delete(fileName);
                    }
                }
                if (savedFileName == null)
                {
                    File.Move(tempName, fileName);
                }
                else
                {
                    File.Move(savedFileName, fileName);
                }
                savedFileName = fileName;
            }
            catch (Exception ex)
            {
                DialogResult dr =
                    MessageBox.Show(
                        "It seems this file is currently open in Excel. Do you want eCampus to shutdown all open Excel Applications and attempt to save again? You can close the file manually and Click Retry or Click Ignore to automatically close all open apps and continue.",
                        "File Save Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Hand);
                if (dr == DialogResult.Retry)
                {
                    saveFile(fileName);
                }
                else if (dr == DialogResult.Ignore)
                {
                    foreach (Process p in Process.GetProcessesByName("excel"))
                    {
                        p.Kill();
                    }
                    saveFile(fileName);
                }
            }
        }

        private void buttonViewinExcel_Click(object sender, EventArgs e)
        {
            if (savedFileName == null)
            {
                MessageBox.Show(
                    "You have to save the file first before you can open or view. Press OK to choose a location to save your file.");
            }
            else
            {
                if (File.Exists(savedFileName))
                {
                    Process.Start("excel", "\"" + savedFileName + "\"");
                }
                else
                {
                    MessageBox.Show(
                        "It seems the file you are trying to open has been renamed or does not exist anymore.");
                }
            }
        }

        private void buttonPrintReport_Click(object sender, EventArgs e)
        {
            if (savedFileName == null)
            {
                MessageBox.Show("It seems the file has not been saved yet. Please save the file first and try again.",
                                "File Missing", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }
            int pages = Convert.ToInt32(Math.Ceiling(listViewStudents.Items.Count/30d));
            var excelApp = new Microsoft.Office.Interop.Excel.Application(); // Creates a new Excel Application
            excelApp.Visible = true; // Makes Excel visible to the user.

            Workbook excelWorkbook = null;
            excelApp.DefaultFilePath = savedFileName;
            // Open the Workbook:
            excelWorkbook = excelApp.Workbooks.Open(savedFileName, 0, false, 5, "", "", false, XlPlatform.xlWindows, "",
                                                    true,
                                                    false, 0, true, false, false);

            var ws = excelWorkbook.Worksheets[1] as Worksheet;

            // The following gets the Worksheets collection
            var excelSheet = excelWorkbook.Worksheets[1] as Worksheet;

            excelSheet.PrintOutEx(1, 1, 1, false, null, false, false, false, false);
            excelSheet.get_Range("B12", "B" + (12 + listViewStudents.Items.Count)).Activate();

            bool userDidntCancel =
                excelApp.Dialogs[XlBuiltInDialog.xlDialogPrint].Show(2, 1, 4, 1, false, false);

            if (userDidntCancel)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //Marshal.FinalReleaseComObject(excelSheet);

                excelWorkbook.Close(false, Type.Missing, Type.Missing);
                Marshal.FinalReleaseComObject(excelWorkbook);

                excelApp.Quit();
                Marshal.FinalReleaseComObject(excelApp);
            }
        }

        private void studentsData_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            DataGridViewRow cRow = studentsData.Rows[e.RowIndex];
            try
            {
                crowTScore = Convert.ToInt32(cRow.Cells["CAScore"].Value) +
                             Convert.ToInt32(cRow.Cells["ExamScore"].Value);
            }
            catch
            {
            }
        }

        private void comboBoxDataGen_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelExanDate.Visible = examDatePicker.Visible = comboBoxDataGen.Text == "Official Grade Report Template";
        }

        private void comboCourses_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelCourse.Text = comboCourses.Text;
            loadCourse(labelCourse.Text);
        }

        private void loadCourse(string course)
        {
            LoadCourses(false);
            string key = course + ":" + _cDepartment;
            if (!mycourses.ContainsKey(key))
            {
                buttonUpload.Enabled = false;
                return;
            }
            LecturerCourse selCourse = mycourses[course + ":" + _cDepartment];
            if (selCourse.Submitted)
            {
                if (selCourse.HodApproved == false || selCourse.DeanApproved == false)
                {
                    buttonUpload.Text = "Re&Submit";
                    buttonUpload.Enabled = true;
                }
                else
                {
                    buttonUpload.Enabled = false;
                    if (selCourse.HodApproved == null || selCourse.DeanApproved == null)
                    {
                        buttonUpload.Text = "Awaiting Approval";
                    }
                    else
                    {
                        buttonUpload.Text = "Approved";
                    }
                }
            }
            else
            {
                buttonUpload.Text = "&Submit";
                buttonUpload.Enabled = true;
            }
        }

        private void comboBoxCourseGen_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxDepartment.Items.Clear();
            comboBoxDepartment.Text = "Select Department";
            foreach (var course in mycourses)
            {
                if (course.Key.Contains(comboBoxCourseGen.Text))
                {
                    comboBoxDepartment.Items.Add(course.Value.Department);
                }
            }
        }

        private void buttonNextAnnounce_Click(object sender, EventArgs e)
        {
            announceIndex++;
            try
            {
                richTextBoxAnnouncements.Rtf = announcements[announceIndex];
            }
            catch (ArgumentException)
            {
                richTextBoxAnnouncements.Text = announcements[announceIndex].StripRTF();
            }
            if (announceIndex == announcements.Count - 1)
            {
                buttonNextAnnounce.Enabled = false;
            }
            if (announceIndex != 0)
            {
                buttonPreviousAnnounce.Enabled = true;
            }
        }

        private void buttonPreviousAnnounce_Click(object sender, EventArgs e)
        {
            announceIndex--;
            try
            {
                richTextBoxAnnouncements.Rtf = announcements[announceIndex];
            }
            catch (ArgumentException)
            {
                richTextBoxAnnouncements.Text = announcements[announceIndex].StripRTF();
            }
            if (announceIndex == 0)
            {
                buttonPreviousAnnounce.Enabled = false;
            }
            if (announceIndex != announcements.Count - 1)
            {
                buttonNextAnnounce.Enabled = true;
            }
        }

        private int timerCount;
        /// <summary>
        /// The amount of time to wait before session is locked. Since timer runs every 3 seconds, this means 180 seconds or three minutes
        /// </summary>
        private int timerTime = 60;


        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            if (_local)
            {
                return;
            }
            if (announcements.Count !=
                Helper.GetAnnouncementCount("department", _currentLecturer.Department, UserRoles.Lecturer))
            {
                LoadAnnouncements(true);
            }
            if (_acts.Count != Helper.GetActivitiesCount("department", _currentLecturer.Department))
            {
                LoadActivities(true);
            }
            timerCount++;
            if (timerCount == timerTime)
            {
                btnLock.PerformClick();
            }
            timer1.Enabled = true;
        }

        private void notifyIcon1_BalloonTipClicked(object sender, EventArgs e)
        {
            tabControl1.SelectTab(0);
            BringToFront();
            Focus();
            if (WindowState == FormWindowState.Minimized)
            {
                WindowState = FormWindowState.Normal;
            }
        }

        private void notifyIcon1_BalloonTipShown(object sender, EventArgs e)
        {
            sp = sp ?? new SoundPlayer();
            sp.Stream = Resources._2cool;
            sp.Play();
        }


        private void btnEditPassword_Click(object sender, EventArgs e)
        {
            UpdatePassword up = new UpdatePassword();
            up.ShowDialog();
        }

        private void toolBtnAbout_Click(object sender, EventArgs e)
        {
            About ab = new About();
            ab.ShowDialog();
        }
    }
}
