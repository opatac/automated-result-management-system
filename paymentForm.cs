using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace ARMS
{
    public partial class paymentForm : Form
    {
        private string sregno;
        public paymentForm()
        {
            InitializeComponent();
        }
        public paymentForm(string Regno)  
        {
            InitializeComponent();
            sregno = Regno;
            this.Load += (s, a) => { if (sregno != null)loadPayment(); };
        }
        private void loadPayment()
        {
            if (sregno == null)
            {
                MessageBox.Show("There is no student items", "Student Payment History", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            MySqlConnection conn = new MySqlConnection(Helper.LocalConnection);
            MySqlCommand cmd = new MySqlCommand();
            string sql = "SELECT * FROM payment WHERE regno=@regno";
            cmd.CommandText = sql;
            cmd.Connection = conn;
            conn.Open();
            cmd.Parameters.AddWithValue("@regno", sregno);
            List<payment> payments = new List<payment>();
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                payment pymt = new payment();
                pymt.date = Convert.ToDateTime(reader["date"].ToString());
                pymt.feetype = reader["feetype"].ToString();
                pymt.level = reader["level"].ToString();
                pymt.regno = reader["regno"].ToString();
                payments.Add(pymt);
            }
            conn.Close();
            dataGridView1.DataSource = payments;
        }

        private void txtregno_Click(object sender, EventArgs e)
        {
            if (txtregno.Text == "Enter Reg. No.")
                txtregno.Clear();
            else
                txtregno.SelectAll();
        }

        private void btnsearch_Click(object sender, EventArgs e)
        {
            loadPayment();
        }
    }
    public class payment {
        public string regno { set; get; }
        public string feetype { set; get; }
        public string level { set; get; }
        public DateTime date { set; get; }
    }
}
